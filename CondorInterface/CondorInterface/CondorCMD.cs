﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondorInterface
{
    public class CondorCMD
    {
        private ProcessRunner processRunner;
        private string submitNode;
        private string localDir;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="submitNode">hostname of condor submit node</param>
        /// <param name="localDir">local directory containing all .sub files</param>
        public CondorCMD(string submitNode, string localDir) 
        {
            this.submitNode = submitNode;
            this.localDir = localDir;
            this.processRunner = new ProcessRunner(submitNode);
        } 

        /// <summary>
        /// Writes program arguments to a file and copy to submit node
        /// </summary>
        /// <param name="args">program arguments</param>
        public void UploadProgramArguments(List<string> args)
        {
            const string input_filename = "input_args.txt";
            string path = Path.Combine(Directory.GetCurrentDirectory(), input_filename);
            string[] argsArray = new string[args.Count];
            for (int i = 0; i < args.Count; i++)
            {
                argsArray[i] = args[i];
            }
            System.IO.File.WriteAllLines(path, argsArray);

            string destPath = String.Format("\\\\{0}\\{1}", submitNode, 
                Path.Combine(localDir.Replace(':', '$'), "files", input_filename));
            
            File.Copy(path, destPath, true);
        }

        /// <summary>
        /// Writes single program argument to a file and copy to submit node
        /// </summary>
        /// <param name="args">program arguments</param>>
        public void UploadProgramArguments(String args)
        {
            UploadProgramArguments(new List<string>(new [] {args }));
            /*const string input_filename = "input_args.txt";
            string path = Path.Combine(Directory.GetCurrentDirectory(), input_filename);
            System.IO.File.WriteAllText(path, args);

            string destPath = String.Format("\\\\{0}\\{1}", submitNode, 
                Path.Combine(localDir.Replace(':', '$'), "files", input_filename));
            
            File.Copy(path, destPath, true);*/
        }

        /// <summary>
        /// Takes a template batch file and substitutes with corresponding binFolder, exeName
        /// and uploads to submit node. Does not upload executable (it assumes that the executable is
        /// already on the network drive)
        /// </summary>
        /// <param name="templateBatch">path of template batch file</param>
        /// <param name="binFolder">folder containing binary relative to network path</param>
        /// <param name="exeName">executable name</param>
        /// <param name="copyToInput">flag to indicate whether to copy to input folder</param>
        /// <param name="argsCopyMax">program arguments to copy</param>
        /// <param name="batchOutput">output batch filename</param>
        public void UploadExecutableBatch(string templateBatchPath, string binFolder, string exeName, 
            bool copyToInput=false, int argsCopyMax=1, string batchFileOutput="run.bat")
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(batchFileOutput);
            string[] lines = System.IO.File.ReadAllLines(templateBatchPath);

            foreach(string line in lines)
            {
                if (line.StartsWith("set binfolder")) {
                    file.WriteLine("set binfolder=" + binFolder); 
                } else if (line.StartsWith("set executable"))
                {
                    file.WriteLine("set executable=" + exeName);
                } else if (line.StartsWith("set copyToInput="))
                {
                    string state = copyToInput ? "1" : "0";
                    file.WriteLine("set copyToInput=" + state);
                } else if (line.StartsWith("set argCopyMax="))
                {
                    file.WriteLine("set argCopyMax=" + argsCopyMax.ToString());
                } else
                {
                    file.WriteLine(line);
                }
            }
            file.Close();
            string destPath = String.Format("\\\\{0}\\{1}", submitNode, 
                Path.Combine(localDir.Replace(':', '$'), batchFileOutput));
            File.Copy(batchFileOutput, destPath, true);
        }

        /// <summary>
        /// Takes a template condor submit file and substitutes desired executable file (.bat file)
        /// </summary>
        /// <param name="submitFilePath">path of template submit file</param>
        /// <param name="submitFileOutput">outout name for executable batch file</param>
        public void UploadSubmitFile(string templateSubmitPath, string submitBatchName="run.bat", 
            string submitFileOutput="run.sub")
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(submitFileOutput);
            string[] lines = System.IO.File.ReadAllLines(templateSubmitPath);
            foreach (string line in lines)
            {
                if (line.StartsWith("executable"))
                {
                    file.WriteLine("executable=" + Path.Combine(localDir, submitBatchName));
                } else
                {
                    file.WriteLine(line);
                }
            }
            file.Close();
            string destPath = String.Format("\\\\{0}\\{1}", submitNode,
                Path.Combine(localDir.Replace(':', '$'), submitFileOutput));
            File.Copy(submitFileOutput, destPath, true);
            //File.Copy()
        }

        /// <summary>
        /// Perform a condor submit given a .sub file already on the condor submission node 
        /// in the local directory passed into the constructor
        /// </summary>
        /// <param name="submitFileName">.sub file for condor</param>
        /// <returns>jobids of the the submitted condor job</returns>
        public List<string> SubmitJobs(string submitFileName) 
        {
            List<string> result = new List<string>();
            Tuple<string, string> output = processRunner.Run("condor_submit --terse "  
                + Path.Combine(localDir, submitFileName));

            /*process.StartInfo.Arguments = string.Format("-r:{0} condor_submit --terse {1}", 
                submitNode, Path.Combine(localDir, submitFileName));
            Tuple<string, string> output = startProcess();*/
            string stdout = output.Item1;
            string stderr = output.Item2;

            //something went wrong
            if (stdout.Equals(""))
            {
                return result;
            }

            string[] jobIds = stdout.Split('-');
            string startId = jobIds[0].Trim(' ', '\n');
            string endId = jobIds[1].Trim(' ', '\n');

            //extract clusterId and procId from jobId (in the form clusterId.procId)
            int clusterId = Int32.Parse(startId.Split('.')[0]);
            int startProcId = Int32.Parse(startId.Split('.')[1]);
            int endProcId = Int32.Parse(endId.Split('.')[1]);

            for (int i = startProcId; i <= endProcId; i++)
            {
                result.Add(clusterId.ToString() + "." + i.ToString());
            }
            //Console.WriteLine(stdout);
            //Console.WriteLine(stderr);
            return result;
        }

        /// <summary>
        /// Removes a job from the condor queue
        /// </summary>
        /// <param name="jobId">job to remove</param>
        /// <returns>true if success, false otherwise (not found) </returns>
        public bool RemoveJob(string jobId)
        {
            //process.StartInfo.Arguments = String.Format("-r:{0} condor_rm {1}", submitNode, jobId);
            //Tuple<string, string> output = startProcess();
            Tuple<string, string> output = processRunner.Run("condor_rm " + jobId);
            string stdout = output.Item1;
            string stderr = output.Item2;
            
            //jobId not found
            if (stdout == "")
            {
                return false;
            }
            return true;
        }
        
        /// <summary>
        /// Get the status of jobs which are still on the queue, essentially the output of doing 
        /// a condor_q of the list of jobIds
        /// </summary>
        /// <param name="jobIds">job statuses to retrieve</param>
        /// <returns>
        /// dictionary of jobs and their associated status, a job that is not in 
        /// the dictionary means that it has completed 
        /// </returns>
        public Dictionary<string, JobStatus> GetJobStatus(List<string> jobIds)
        {
            if (jobIds.Count == 0)
            {
                return new Dictionary<string, JobStatus>();
            }

            /*process.StartInfo.Arguments = String.Format("-r:{0} condor_q {1}", submitNode, 
                String.Join(" ", jobIds));

            Tuple<string, string> output = startProcess();*/
            Tuple<string, string> output = processRunner.Run("condor_q " + String.Join(" ", jobIds));
            string stdout = output.Item1;
            string stderr = output.Item2;

            string [] statusLines = stdout.Split('\n');
            int count = statusLines.Length;

            //first two lines contain title, last two lines contain summary + blank
            if (statusLines.Length <= 4)
            {
                return new Dictionary<string, JobStatus>();
            }

            Dictionary<string, JobStatus> jobs = new Dictionary<string, JobStatus>();
            for (int i = 2; i < statusLines.Length - 2; i++)
            {
                string [] line = statusLines[i].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (line.Length < 9)
                {
                    continue;
                }
                JobStatus jobStatus = new JobStatus(line);

                //job exists and is still on the queue
                if (jobStatus.Id != null)
                {
                    jobs.Add(jobStatus.Id, jobStatus);
                }
            }

            //JobStatus jobStatus = new JobStatus();
            return jobs;
        }

        /// <summary>
        /// Retrieves all completed jobs (no longer on the condor queue anymore
        /// </summary>
        /// <param name="jobIds">jobids to check for completion</param>
        /// <returns>all completed jobs</returns>
        public List<string> CompletedJobs(List<string> jobIds)
        {
            Dictionary<string, JobStatus> jobStatus = GetJobStatus(jobIds);
            List<string> result = new List<string>();

            foreach(string jobid in jobIds)
            {
                if (!jobStatus.ContainsKey(jobid))
                {
                    result.Add(jobid);
                }
            }
            return result;
        }


        /// <summary>
        /// Get job completion status
        /// </summary>
        /// <param name="jobIds"></param>
        /// <returns></returns>
        public bool HasJobsCompleted(List<string> jobIds)
        {
            Dictionary<string, JobStatus> jobStatus = GetJobStatus(jobIds);
            return jobStatus.Count == 0 ? true : false;
        }

        /// <summary>
        /// Get job percentage completion
        /// </summary>
        /// <param name="jobIds">jobs to check for completion</param>
        /// <returns>percentage out of 100</returns>
        public float GetPercentComplete(List<string> jobIds) 
        {
            Dictionary<string, JobStatus> jobStatus = GetJobStatus(jobIds);
            return ((jobIds.Count - jobStatus.Count)/((float) jobIds.Count)) * 100f;
        }

        /// <summary>
        /// Get the jobs which had errors (i.e. jobs in the condor 'hold' state)
        /// </summary>
        /// <param name="jobIds">jobIds to parse</param>
        /// <returns>jobsIds which encounted errors, will always be a subset of input</returns>
        public List<string> GetErrorJobs(List<string> jobIds)
        {
            Dictionary<string, JobStatus> jobs = GetJobStatus(jobIds);
            List<string> result = new List<string>();
            foreach (string id in jobIds)
            {
                if (jobs.ContainsKey(id) && jobs[id].Status.Equals("H"))
                {
                    result.Add(id);                    
                }
            }
            return result;
        }
    }
}
