﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondorInterface
{
    /*
    public enum CONDOR_JOB_STATUS {
       hold,
       running,
       idle,
       completed,
       removed,
       suspended,
       unknown
    };*/

    public class JobStatus
    {
        public string Id { get; set; }
        public string Owner { get; set; }
        public string Submitted { get; set; }
        public string Runtime { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public string Size { get; set; }
        public string Cmd { get; set; }


        public JobStatus(string [] attr)
        {
            Id = attr[0];
            Owner = attr[1];
            Submitted = attr[2] + " " + attr[3];
            Runtime = attr[4];
            Status = attr[5];
            Priority = attr[6];
            Size = attr[7];
            Cmd = attr[8];
        }

        public JobStatus() { }

        /*private CONDOR_JOB_STATUS ConvertST(string st)
        {
            switch (st)
            {
                case "C":
                    return CONDOR_JOB_STATUS.completed;
                case "X":
                    return CONDOR_JOB_STATUS.removed;
                case "S":
                    return CONDOR_JOB_STATUS.suspended;
                case "I":
                    return CONDOR_JOB_STATUS.idle;
                case "R":
                    return CONDOR_JOB_STATUS.running;
                case "H":
                    return CONDOR_JOB_STATUS.hold;
                default:
                    return CONDOR_JOB_STATUS.unknown;
            }
        }
        
        public static string ToStringJobs(List<JobStatus> jobs)
        {
            foreach(JobStatus job in jobs)
            {
                job.ToString();
            }
            return result;
        }*/

        public string ToString()
        {
            return String.Format("{0} {1} {2} {3} {4} {5} {6} {7}", Id, Owner, Submitted, 
                Runtime, Status, Priority, Size, Cmd);
        }
    }
}
