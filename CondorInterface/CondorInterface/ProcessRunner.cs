﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondorInterface
{
    /// <summary>
    /// Wrapper class for process runner for local or remote (via winrs) PCs
    /// </summary>
    public class ProcessRunner
    {
        private Process process;
        private bool runLocal = false;
        private string host;

        public ProcessRunner(string host)
        {
            process = new Process();

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            
            if (host == "localhost" || String.IsNullOrEmpty(host))
            {
                runLocal = true;
            } else
            {
                process.StartInfo.FileName = "winrs.exe";
                runLocal = false;
                this.host = host;
            }
        }

        /// <summary>
        /// Start running a process
        /// </summary>
        /// <returns>stdout, stderr of process</returns>
        private Tuple<string, string> StartProcess()
        {
            StringBuilder stdout = new StringBuilder();
            StringBuilder stderr = new StringBuilder();

            //todo: how to avoid duplication here
            process.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))
                {
                    stdout.Append(e.Data + '\n');
                }
            });

            process.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))
                {
                    stderr.Append(e.Data + '\n');
                }
            });

            //todo: add timeout
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.CancelOutputRead();
            process.CancelErrorRead();
            process.Close();

            return Tuple.Create(stdout.ToString(), stderr.ToString());
        }

        public Tuple<string, string> Run(string cmd)
        {
            if (String.IsNullOrEmpty(cmd))
            {
                return new Tuple<string, string>("","");
            }

            string[] cmds = cmd.Split(' ');

            if (!runLocal)
            {
                process.StartInfo.FileName = "winrs.exe";
                process.StartInfo.Arguments = "-r:" + host + " " + cmd;
            } else
            {
                process.StartInfo.FileName = cmds[0];
                process.StartInfo.Arguments = String.Join(" ", cmds, 1, cmds.Length - 1);
            }
            return StartProcess();
        } 

        public bool IsLocal()
        {
            return runLocal;
        }
    }
}
