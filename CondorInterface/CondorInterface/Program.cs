﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using OpenCV;

namespace CondorInterface
{
    class Program
    {
        private static StringBuilder output = new StringBuilder();
        private static string submitNode = "condor-sub-gjh9.it.csiro.au";
        private static string localDir = "E:\\CGA\\general";
        private static string batTemplatePath = "C:\\Users\\luo029\\Documents\\run_template.bat";
        private static string subTemplatePath = "C:\\Users\\luo029\\Documents\\run_template.sub";
        private static string networkPath = "\\\\con-osm-02-cdc.it.csiro.au\\OSM_CBR_EN_CGA_work";
        private static int reduceAmount = 5;

        private static int getGridCount(string fileName, int gridSize)
        {
            CondorCMD condor = new CondorCMD(submitNode, localDir);
            //condor.UploadProgramArguments(new List<string>() { "tree.jpg abc aaa", "dog.png abc aaa" });
            condor.UploadExecutableBatch(batTemplatePath, "grid_binaries", "ImgHistogramDivide.exe", false);

            condor.UploadProgramArguments(new List<string>() {
                String.Format("{0} {1} grid_out.txt", fileName, gridSize)});

            List<string> jobIds = condor.SubmitJobs("run.sub");


            bool complete = false;
            while (!complete)
            {
                //Console.WriteLine(condor.GetPercentComplete(jobIds));
                complete = condor.HasJobsCompleted(jobIds);
                Thread.Sleep(1000);
            }

            string outFile = Path.Combine(networkPath, "output", "grid_out.txt");
            string text = System.IO.File.ReadAllText(outFile).Trim(' ', '\n');

            return Int32.Parse(text);
        }

        private static Dictionary<string, int> generateHistogram(CondorCMD condor, string filename,
            int count, int gridSize)
        {
            //CondorCMD condor = new CondorCMD(submitNode, localDir);
            condor.UploadExecutableBatch(batTemplatePath, "hist_binaries", "ImgHistogram.exe", true, 1, "runHist.bat");
            condor.UploadSubmitFile(subTemplatePath, "runHist.bat", "runHist.sub");

            List<string> arguments = new List<string>();
            for (int i = 0; i < count; i++)
            {
                arguments.Add(String.Format("{0} {1} {2} {3}", filename, i, gridSize,
                    String.Format("{0}_hist{1}_out.bin", Path.GetFileNameWithoutExtension(filename), i)));
            }
            condor.UploadProgramArguments(arguments);

            List<string> jobIds = condor.SubmitJobs("runHist.sub");

            Dictionary<string, int> jobIndexMap = new Dictionary<string, int>();

            for (int i = 0; i < jobIds.Count; i++)
            {
                jobIndexMap[jobIds[i]] = i;
            }


            /*bool complete = false;
            while (!complete)
            {
                Console.WriteLine(condor.GetPercentComplete(jobIds));
                complete = condor.HasJobsCompleted(jobIds);
                //Console.WriteLine(complete);

                Thread.Sleep(1000);
            }*/
            return jobIndexMap;
        }

        private static bool reduceHistogram(string filename, CondorCMD condor,
            Dictionary<string, int> onGoingJobs, int count)
        {
            //jobs completed
            Queue<Tuple<string, int>> completeQueue = new Queue<Tuple<string, int>>();
            bool complete = false;

            while (!complete) {
                //retrive completed jobs
                List<string> completedJobs = condor.CompletedJobs(new List<string>(onGoingJobs.Keys));

                //add to completion queue
                foreach (string jobId in completedJobs)
                {
                    completeQueue.Enqueue(new Tuple<string, int>(jobId, onGoingJobs[jobId]));
                    onGoingJobs.Remove(jobId);
                }
                string hFilename = Path.GetFileNameWithoutExtension(filename);

                //queue hits threshold or few remain, perform a reduce on the histograms
                if (completeQueue.Count > reduceAmount || ((onGoingJobs.Count <= reduceAmount
                    && completeQueue.Count > 1)))
                {
                    List<string> arguments = new List<string>();

                    //todo: generalize output file
                    Console.WriteLine("####QUEUE#####");

                    //remove from queue to prepare input arguments (input histogram counts) 
                    //for collasce operation
                    int queueCount = completeQueue.Count;
                    for (int i = 0; i < queueCount; i++)
                    {
                        Tuple<string, int> job = completeQueue.Dequeue();
                        string jobId = job.Item1;
                        int index = job.Item2;
                        Console.WriteLine("job: " + jobId);
                        //int index = onGoingJobs[jobId];
                        arguments.Add(String.Format("{0}_hist{1}_out.bin", hFilename, index));
                    }
                    Console.WriteLine("##############");
                    count++;

                    //last arguement is the outputfile
                    arguments.Add(String.Format("{0}_hist{1}_out.bin", hFilename, count));
                    Console.WriteLine("Reducing: " + String.Join(" ", arguments.ToArray()));

                    //submit job to collasce histogram
                    condor.UploadProgramArguments(String.Join(" ", arguments.ToArray()));
                    condor.UploadExecutableBatch(batTemplatePath, "reduce_binaries",
                        "ImgHistogramReduce.exe", true, arguments.Count - 1, "runReduce.bat");
                    condor.UploadSubmitFile(subTemplatePath, "runReduce.bat", "runReduce.sub");
                    List<string> jobIds = condor.SubmitJobs("runReduce.sub");

                    //job did not make it on the queue a possible error
                    if (jobIds.Count != 1)
                    {
                        return false;
                    }

                    //output count
                    onGoingJobs[jobIds[0]] = count;
                }
                else if (condor.HasJobsCompleted(new List<string>(onGoingJobs.Keys)) &&
                    (completeQueue.Count <= 1))
                {
                    //rename and copy to output directory
                    /*string lastfile = String.Format("{0}_hist{1}_out.bin", hFilename, count);
                    string outfile = String.Format("{0}_hist_final.bin", hFilename);
                    string outfilepath = Path.Combine(networkPath, "input", outfile);

                    if (File.Exists(outfilepath))
                    {
                        File.Delete(outfilepath);
                    }

                    File.Move(Path.Combine(networkPath, "input", lastfile), outfilepath);*/
                    break;
                }

                Thread.Sleep(3000);
            }

            return true;
        }

        private static bool CsvHistogram(string filename, CondorCMD condor)
        {
            //submit job to convert to histogram binary to csv
            condor.UploadProgramArguments(filename + " " +
                Path.GetFileNameWithoutExtension(filename) + "_out.csv");

            condor.UploadExecutableBatch(batTemplatePath, "csv_binaries",
                "ImgHistogramCsv.exe", false, 1, "runCsv.bat");
            condor.UploadSubmitFile(subTemplatePath, "runCsv.bat", "runCsv.sub");
            List<string> jobIds = condor.SubmitJobs("runCsv.sub");

            //job did not make it on the queue a possible error
            if (jobIds.Count != 1)
            {
                return false;
            }

            bool complete = false;
            while (!complete)
            {
                Console.WriteLine(condor.GetPercentComplete(jobIds));
                complete = condor.HasJobsCompleted(jobIds);
                Thread.Sleep(1000);
            }
            return true;
        }

        private static void testHistogram(CondorCMD condor)
        {
            condor.UploadProgramArguments("tree_hist0_out.bin tree_hist1_out.bin tree_hist2_out.bin " +
                "tree_hist3_out.bin tree_hist12_out.bin");
            condor.UploadExecutableBatch(batTemplatePath, "reduce_binaries", "ImgHistogramReduce.exe", true, 4);
            List<string> jobIds = condor.SubmitJobs("run.sub");
            bool complete = false;
            while (!complete)
            {
                Console.WriteLine(condor.GetPercentComplete(jobIds));
                complete = condor.HasJobsCompleted(jobIds);
                Thread.Sleep(1000);
            }
        }


        private static void condorRun()
        {
            Console.WriteLine("Counting grids");
            int count = getGridCount("tree.jpg", 500);
            Console.WriteLine("Found: " + count);
            Console.WriteLine("finished");
            CondorCMD condor = new CondorCMD(submitNode, localDir);
            //testHistogram(condor);
            //int count = 12;
            Dictionary<string, int> jobids = generateHistogram(condor, "tree.jpg", count, 500);
            if (reduceHistogram("tree.jpg", condor, jobids, count))
            {
                Console.WriteLine("Success");
            } else
            {
                Console.WriteLine("Fail");

            }
            //CsvHistogram("tree_hist_final.bin", condor);
            
            Console.WriteLine("END");
        }

        static void Main(string[] args1)
        {
            condorRun();
        }
    }
}
