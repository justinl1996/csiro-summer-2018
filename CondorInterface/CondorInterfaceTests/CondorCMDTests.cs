﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CondorInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondorInterface.Tests
{
    [TestClass()]
    public class CondorCMDTests
    {
        private CondorCMD condorCMD;
        private ProcessRunner processRunnerLocal;
        private ProcessRunner processRunnerRemote;

        [TestInitialize]
        public void TestInitialize()
        {
            condorCMD = new CondorCMD("condor-sub-gjh9.it.csiro.au", "E:\\CGA\\luo029");
            processRunnerLocal = new ProcessRunner("localhost");
            processRunnerRemote = new ProcessRunner("condor-sub-gjh9.it.csiro.au");
        }

        [TestMethod()]
        public void CondorCMDTest()
        {
            //Assert.Success();
        }

        [TestMethod()]
        public void UploadProgramArgumentsTest()
        {
            condorCMD.UploadProgramArguments(new List<string>() { "cba.txt", "fed.txt", "ihg.txt"});

        }


        [TestMethod()]
        public void SubmitJobsTest()
        {
            //Assert.AreEqual(new List<string>(), condorCMD.SubmitJobs("doesnotexist.sub"));
            List<string> jobIds = condorCMD.SubmitJobs("partitionImage.sub");

        }

        [TestMethod()]
        public void RemoveJobTest()
        {
            Assert.AreEqual(false, condorCMD.RemoveJob("1.3"));
            Assert.AreEqual(false, condorCMD.RemoveJob("saldsj"));
            //Assert.AreEqual(true, condorCMD.RemoveJob("59.0"));
        }

        [TestMethod()]
        public void GetJobStatusTest()
        {
            //List<string> jobIds = condorCMD.SubmitJobs("partitionImage.sub");
            //condorCMD.GetJobStatus(new List<string>(){"1.0", "3.3"});
            condorCMD.GetJobStatus(new List<string>() { "69.8", "69.9", "69.10", "69.11" });

        }

        [TestMethod()]
        public void Stuff()
        {
            List<string> foo = new List<string>() { "sad", "dsadsa", "dquewi" };
            //string result = String.Join(" ", foo);

            string result = String.Format("{0,20} asds", "123");
            Console.WriteLine(result);
            //string test = foo.ToString();
        }

        [TestMethod()]
        public void GetErrorJobsTest()
        {
            Assert.AreEqual(3, condorCMD.GetErrorJobs(new List<string>() { "82.0", "82.1", "82.3"}).Count);
            Assert.AreEqual(1, condorCMD.GetErrorJobs(new List<string>() { "82.0", "12.3", "34.2"}).Count);
            
        }

        [TestMethod()]
        public void PercentCompleteTest()
        {
            Assert.AreEqual(0.0f, condorCMD.GetPercentComplete(new List<string>() { "82.0", "82.1", "82.3" }));
        }

        [TestMethod()]
        public void TestUploadExecutableBatch()
        {
            condorCMD.UploadExecutableBatch("C:\\Users\\luo029\\Documents\\run_template.bat", "bin", "a.exe");
            Console.WriteLine("DSASDSd");
        }

        [TestMethod()]
        public void TestUploadSubmitFile()
        {
            condorCMD.UploadSubmitFile("C:\\Users\\luo029\\Documents\\run_template.sub", "run2.bat", "run.sub");
            
        }

        [TestMethod()]
        public void TestProcessRunnerLocal()
        {
            Tuple<string, string> output = processRunnerLocal.Run("condor_q");
            string stdout = output.Item1;
            string stderr = output.Item2;
        }

        [TestMethod()]
        public void TestProcessRunnerRemote()
        {
            Tuple<string, string> output = processRunnerRemote.Run("condor_q");
            string stdout = output.Item1;
            string stderr = output.Item2;
        }

    }
}