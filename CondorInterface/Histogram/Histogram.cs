﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenCvSharp;

namespace OpenCV
{
    public class Histogram
    {
        //public const int histSize = 256;
        
        /// <summary>
        /// Generate histogram for each color channel BGR
        /// </summary>
        /// <param name="img">image to compute histogram</param>
        /// <returns>Matrix with histgram values 0 - 255 for 3 channels</returns>
        public static Mat [] generate(Mat img)
        {
            Mat[] outImg = Cv2.Split(img);

            //uniform range 0-255
            Rangef range = new Rangef();
            range.Start = 0;
            range.End = 256;
            Rangef[] ranges = { range };

            bool uniform = true; bool accumulate = false;
            Mat[] hist = new Mat[3];

            int[] histSizes = { 256 };

            //3 - channels (B, G, R)
            for (int i = 0; i < 3; i++)
            {
                hist[i] = new Mat();
                Cv2.CalcHist(outImg, new[] { i }, new Mat(), hist[i], 1, histSizes, ranges, 
                    uniform, accumulate);
            }

            return hist;
        }

        /// <summary>
        /// Generate histogram for each color channel BGR (same as above but my 
        /// version without normalization issue)
        /// </summary>
        /// <param name="img">image to compute histogram</param>
        /// <returns>Matrix with histgram values 0 - 255 for 3 channel</returns>
        public static Mat [] generate2(Mat img)
        {
            Mat[] outImg = Cv2.Split(img );
            Mat[] hist = new Mat[3];
            byte intensity = 0;

            for (int c = 0; c < outImg.Length; c++) {
                //hist[c] = new Mat(255, 1, MatType.CV_32S);
                hist[c] = Mat.Zeros(255, 1, (MatType) MatType.CV_32S);
                for (int x = 0; x < outImg[c].Cols; x++)
                {
                    for (int y = 0; y < outImg[c].Rows; y++)
                    {
                        intensity = outImg[c].Get<byte>(y, x);
                        int count = hist[c].Get<int>(intensity, 0);
                        hist[c].Set<int>(intensity, 0, count + 1);
                    }
                }
            }

            return hist;
        }

        /// <summary>
        /// Generate a histogram for the grid region
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="x">topleft x coordinate</param>
        /// <param name="y">topleft y coordinate</param>
        /// <param name="gridsize">size of grid (width, height) </param>
        /// <returns>B,G,R matrix corresponding to histogram values</returns>
        public static Mat [] generate(string filename, int x, int y, int gridsize)
        {
            
            Mat img = Cv2.ImRead(filename, ImreadModes.Color);
            //split into 3 seperate channels 

            Mat region =  new Mat(img, new Rect(x, y, gridsize, gridsize));
            return generate(region);
        }


        /// <summary>
        /// Combine histograms inplace
        /// </summary>
        /// <param name="hist1">first histogram (will be modified) </param>
        /// <param name="hist2">second histogram </param>
        /// <returns>true if successful</returns>
        public static bool CombineHistogram(Mat[] hist1, Mat[] hist2)
        {
            if (hist1.Length != hist2.Length || hist1.Length != 3)
            {
                return false;
            }

            for (int i = 0; i < 3; i++)
            {
                if (hist1[i].Rows != hist2[i].Rows || hist1[i].Cols != hist2[i].Cols)
                {
                    return false;
                }

                for (int r = 0; r < hist1[i].Rows; r++)
                {
                    int l1 = hist1[i].At<int>(r, 0);
                    int l2 = hist2[i].At<int>(r, 0);
                    int val = hist1[i].At<int>(r, 0) + hist2[i].At<int>(r, 0);
                   
                    hist1[i].Set(r, 0, val);
                }
            }
            /*hist1[0] = hist1[0] + hist2[0];
            hist1[1] = hist1[1] + hist2[1];
            hist1[2] = hist1[2] + hist2[2];*/

            return true;
        }

        /// <summary>
        /// Combine histograms inplace
        /// </summary>
        /// <param name="hists">histograms to combine (first histogram will be modified) </param>
        /// <returns>true if successful</returns>
        public static bool CombineHistogram(List<Mat []> hists)
        {
            for (int i = 0; i < hists.Count-1; i++)
            {
                if (!CombineHistogram(hists[0], hists[i+1]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Output B,G,R histogram to stdout
        /// </summary>
        /// <param name="hist">histogram to output</param>
        public static void OutputHistogram(Mat [] hist)
        {
            Console.WriteLine("########## RGB HISTOGRAM ###########");
            for (int r = 0; r < hist[0].Rows; r++)
            {
                string[] line = new string[3]; 
                for (int i = 0; i < 3; ++i)
                {
                    line[i] = hist[i].At<int>(r, 0).ToString();
                }
                Console.WriteLine(string.Join(",", line));
            }
            Console.WriteLine("####################################");
        }

        /// <summary>
        /// Write B,G,R histogram to filename
        /// </summary>
        /// <param name="hist">histogram to output</param>
        /// <param name="filename">file to write to</param>
        public static void OutputHistogramCSV(Mat[] hist, string filename)
        {
            using (StreamWriter stream = new StreamWriter(filename))
            {
                for (int r = 0; r < hist[0].Rows; r++)
                {
                    string[] line = new string[3];
                    for (int i = 0; i < 3; ++i)
                    {
                        line[i] = hist[i].At<int>(r, 0).ToString();
                    }
                    stream.WriteLine(String.Join(",", line));
                }
            }
        }

        public static void example()
        {
            Mat src = new Mat("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.GrayScale);
            // Mat src = Cv2.ImRead("lenna.png", ImreadModes.GrayScale);
            Mat dst = new Mat();

            Cv2.Canny(src, dst, 50, 200);
            using (new Window("src image", src))
            using (new Window("dst image", dst))
            {
                Cv2.WaitKey();
            }
        }


    }
}
