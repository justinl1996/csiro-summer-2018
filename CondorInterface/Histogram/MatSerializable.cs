﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenCvSharp;

namespace OpenCV
{
    /// <summary>
    /// Wrapper for Mat class to enable serialization 
    /// (Mat not serializable by default)
    /// </summary>
    [System.Serializable]
    public class MatSerializable {
        private int rows;
        private int cols;
        private int[,] array;
        private int matType;

        public MatSerializable(Mat mat)
        {
            rows = mat.Rows;
            cols = mat.Cols;
            matType = mat.Type();

            array = new int[rows,cols];
            for (int r = 0; r < mat.Rows; r++)
            {
                for (int c = 0; c < mat.Cols; c++)
                {
                    //todo: generalise type
                    array[r,c] = mat.At<int>(r, c);
                } 
            }
        }

        public Mat getMat()
        {
            Mat mat = new Mat(rows, cols, matType);
            for (int r = 0; r < mat.Rows; r++)
            {
                for (int c = 0; c < mat.Cols; c++)
                {
                    mat.Set<int>(r, c, array[r,c]);
                }
            }
            return mat;
        }

    }
}
