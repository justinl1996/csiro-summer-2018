﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenCV;
using OpenCvSharp;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

namespace OpenCV
{
    public class Util
    {
        public static void Serialize(string outfile, List<Mat> hist)
        {
            Stream stream = File.Open(outfile, FileMode.Create, FileAccess.Write);
            BinaryFormatter formatter = new BinaryFormatter();
            List<MatSerializable> matSerializables = new List<MatSerializable>(hist.Count);
            for (int i = 0; i < hist.Count; i++)
            {
                matSerializables.Add(new MatSerializable(hist[i]));
            }
            formatter.Serialize(stream, matSerializables);
            stream.Close();
        }

        public static Mat[] Deserialize(string infile)
        {
            Stream stream = File.Open(infile, FileMode.Open, FileAccess.Read);
            BinaryFormatter formatter = new BinaryFormatter();
            List<MatSerializable> matSerializables = (List<MatSerializable>)formatter.Deserialize(stream);

            Mat[] result = new Mat[matSerializables.Count];
            for (int i = 0; i < matSerializables.Count; i++)
            {
                result[i] = matSerializables[i].getMat();
            }

            stream.Close();
            return result;
        }


        public static Tuple<int, int> GetGridCoordinate(int index, int gridSize, int width)
        {
            //int rows = (int) Math.Ceiling(height / ((float)gridSize));
            int cols = (int)Math.Ceiling(width / ((float)gridSize));

            return new Tuple<int, int>((index % cols) * gridSize, (index / cols) * gridSize);
        }

        public static int GetGridCount(int gridSize, int width, int height)
        {
            int rows = (int) Math.Ceiling(height / ((float)gridSize));
            int cols = (int)Math.Ceiling(width / ((float)gridSize));

            return rows * cols;
        }
    }
}
