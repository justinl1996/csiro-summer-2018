﻿using System;
using OpenCvSharp;
using OpenCV;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

namespace ImgHistogram
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("imghistogram imgfile index gridsize outfile");
                Environment.Exit(1);
            }

            int index = Int32.Parse(args[1]);
            int gridSize = Int32.Parse(args[2]);
            string outfile = args[3];


            Mat img = Cv2.ImRead(args[0], ImreadModes.Color);
            int width = img.Cols;
            int height = img.Rows;

            Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(index, gridSize, img.Cols);
            if (coord.Item1 > width || coord.Item2 > height)
            {
                Console.WriteLine("index exceeds no. of grids possible");
                Environment.Exit(1);
            }

            int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
            int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;

            img = new Mat(img, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));

            Mat [] hist = Histogram.generate2(img);

            //string outName = Path.GetFileNameWithoutExtension(args[0]) + 
            //    String.Format("_hist{0}_out.bin", index);

            OpenCV.Util.Serialize(outfile, new List<Mat>(hist));
            //int count = OpenCV.Util.GetGridCount(gridSize, width, height);
        }
    }
}
