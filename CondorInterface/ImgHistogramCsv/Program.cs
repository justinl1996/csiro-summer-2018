﻿using System;
using OpenCV;
using OpenCvSharp;

namespace ImgHistogramCsv
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2 )
            {
                Console.WriteLine("ImgHistogramCsv input output");
                Environment.Exit(1);
            }

            string infile = args[0];
            string outfile = args[1];

            Mat [] hist = OpenCV.Util.Deserialize(infile);
            OpenCV.Histogram.OutputHistogramCSV(hist, outfile); 
        }
    }
}
