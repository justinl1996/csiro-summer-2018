﻿using System;
using OpenCvSharp;

namespace ImgHistogramDivide
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("ImgHistogramDivide imgfile gridsize outfile\n");
                Environment.Exit(1);
            }
            string infile = args[0];
            int gridSize = Int32.Parse(args[1]);
            string outfile = args[2];

            Mat img = Cv2.ImRead(infile, ImreadModes.AnyColor);
            int count = OpenCV.Util.GetGridCount(gridSize, img.Cols, img.Rows);

            System.IO.File.WriteAllText(outfile, count.ToString());
        }
    }
}
