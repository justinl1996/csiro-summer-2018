﻿using System;
using System.Collections.Generic;
using OpenCV;
using OpenCvSharp;

namespace ImgHistogramReduce
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("args: " + String.Format(" ", args));
            if (args.Length == 2)
            {
                System.IO.File.Copy(args[0], args[1], true);
                Environment.Exit(0);
            }
            else if (args.Length < 3)
            {
                Console.WriteLine("ImgHistogramReduce hist0.bin hist1.bin .... outfile");
                Environment.Exit(1);
            }
            string outfile = args[args.Length - 1];
            List<Mat[]> imgHists = new List<Mat[]>(args.Length);

            for (int i = 0; i < args.Length - 1; i++)
            {
                Mat[] hist = OpenCV.Util.Deserialize(args[i]);
                imgHists.Add(hist);
            }

            OpenCV.Histogram.CombineHistogram(imgHists);
            OpenCV.Util.Serialize(outfile, new List<Mat>(imgHists[0]));
        }
    }
}
