using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenCvSharp;
using OpenCV;
using ImgHistogram;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

namespace ImgHistogram.Tests
{
    [TestClass]
    public class ProgramTests
    {
        private bool compareMat(Mat hist1, Mat hist2)
        {
            if (hist1.Rows != hist2.Rows || hist1.Cols != hist2.Cols)
            {
                return false;
            }
            for (int r = 0; r < hist1.Rows; r++)
            {
                for (int c = 0; c < hist1.Cols; c++)
                {
                    if (hist1.At<float>(r, c) != hist2.At<float>(r, c))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool compareHist(Mat[] hist1, Mat[] hist2)
        {
            if (hist1.Length != hist2.Length)
            {
                return false;
            }
            for (int i = 0; i < hist1.Length; i++)
            {
                if (!compareMat(hist1[i], hist2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        [TestMethod]
        public void testHistogram()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat [] hist = Histogram.generate2(image);
            Histogram.OutputHistogram(hist);
        }


        [TestMethod]
        public void TestSerialization()
        {
            Mat[] outHist = OpenCV.Histogram.generate("C:\\Users\\luo029\\Pictures\\tree.jpg", 0, 0, 100);
            OpenCV.Util.Serialize("test.bin", new List<Mat>(outHist));
            Mat[] outHist2 = OpenCV.Util.Deserialize("test.bin");

            bool flag = true;
            for (int i = 0; i < outHist.Length; i++)
            {
                if (!compareMat(outHist[i], outHist2[i]))
                {
                    flag = false;
                }
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void TestGridCoordinate()
        {
            Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(0, 100, 1000);
            Assert.AreEqual(0, coord.Item1);
            Assert.AreEqual(0, coord.Item2);

            coord = OpenCV.Util.GetGridCoordinate(3, 100, 950);
            Assert.AreEqual(300, coord.Item1);
            Assert.AreEqual(0, coord.Item2);

            coord = OpenCV.Util.GetGridCoordinate(3, 100, 310);
            Assert.AreEqual(300, coord.Item1);
            Assert.AreEqual(0, coord.Item2);

            coord = OpenCV.Util.GetGridCoordinate(4, 100, 310);
            Assert.AreEqual(0, coord.Item1);
            Assert.AreEqual(100, coord.Item2);

            coord = OpenCV.Util.GetGridCoordinate(6, 100, 320);
            Assert.AreEqual(200, coord.Item1);
            Assert.AreEqual(100, coord.Item2);

            coord = OpenCV.Util.GetGridCoordinate(7, 100, 320);
            Assert.AreEqual(300, coord.Item1);
            Assert.AreEqual(100, coord.Item2);
        }

        [TestMethod()]
        public void TestCombineHist()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree2_hist.jpg", ImreadModes.Color);
            int height = image.Rows;
            int width = image.Cols;

            Mat imageLeft = new Mat(image, new Rect(0, 0, width/2, height));
            Mat imageRight = new Mat(image, new Rect(width / 2, 0, width / 2, height));

            Mat[] imageLeftHist = Histogram.generate2(imageLeft);
            Mat[] imageRightHist = Histogram.generate2(imageRight);
            Mat[] imageHist = Histogram.generate2(image);

            Histogram.CombineHistogram(imageLeftHist, imageRightHist);

            bool same = true;
            for (int i = 0; i < imageLeftHist.Length; i++)
            {
                if (!compareMat(imageLeftHist[i], imageHist[i]))
                {
                    same = false;
                }
            }
            Assert.IsTrue(same);

            same = true;
            for (int i = 0; i < imageRightHist.Length; i++)
            {
                if (!compareMat(imageRightHist[i], imageHist[i]))
                {
                    same = false;
                }
            }
            Assert.IsFalse(same);
        }
        
        [TestMethod()]
        public void TestCombineHistAndSerialization()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree2_hist.jpg", ImreadModes.Color);
            int height = image.Rows;
            int width = image.Cols;

            Mat imageLeft = new Mat(image, new Rect(0, 0, width / 2, height));
            Mat imageRight = new Mat(image, new Rect(width / 2, 0, width / 2, height));

            Mat[] imageLeftHist = Histogram.generate(imageLeft);
            Mat[] imageRightHist = Histogram.generate(imageRight);

            OpenCV.Util.Serialize("imgLeft.bin", new List<Mat>(imageLeftHist));
            OpenCV.Util.Serialize("imgRight.bin", new List<Mat>(imageRightHist));

            Mat[] imageLeftHist2 = OpenCV.Util.Deserialize("imgLeft.bin");
            Mat[] imageRightHist2 = OpenCV.Util.Deserialize("imgRight.bin");

            Mat[] imageHist = Histogram.generate(image);

            Histogram.CombineHistogram(imageLeftHist, imageRightHist);
            bool same = true;
            for (int i = 0; i < imageLeftHist.Length; i++)
            {
                if (!compareMat(imageLeftHist[i], imageHist[i]))
                {
                    same = false;
                }
            }
            Assert.IsTrue(same);
        }

        [TestMethod()]
        public void TestCombineHistAll()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat [] imageHist = Histogram.generate(image);

            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 500;

            int gridCount = OpenCV.Util.GetGridCount(gridSize, width, height);
            Mat firstImage = new Mat(image, new Rect(0, 0, gridSize, gridSize));
            Mat[] firstImageHist = OpenCV.Histogram.generate(firstImage);

            for (int i = 1; i < gridCount; i++)
            {
                Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(i, gridSize, width);
                int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
                int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;
                Mat gridImg = new Mat(image, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));
                Mat [] gridImgHist = OpenCV.Histogram.generate(gridImg);
                OpenCV.Histogram.CombineHistogram(firstImageHist, gridImgHist);
            }

            bool same = true;
            for (int i = 0; i < imageHist.Length; i++)
            {
                if (!compareMat(imageHist[i], firstImageHist[i]))
                {
                    same = false;
                }
            }
            Assert.IsTrue(same);
        }
        [TestMethod()]
        public void testDeserializeHistCombine()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree1.jpg", ImreadModes.Color);
            Mat[] imageHist = Histogram.generate(image);

            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 300;

            Mat[] hist0 = OpenCV.Util.Deserialize("hist0_out.bin");
            Mat[] hist1 = OpenCV.Util.Deserialize("hist1_out.bin");
            Mat[] hist2 = OpenCV.Util.Deserialize("hist2_out.bin");
            Mat[] hist3 = OpenCV.Util.Deserialize("hist3_out.bin");

            Histogram.CombineHistogram(hist0, hist1);
            Histogram.CombineHistogram(hist0, hist2);
            Histogram.CombineHistogram(hist0, hist3);

            bool same = true;
            for (int i = 0; i < imageHist.Length; i++)
            {
                if (!compareMat(hist0[i], imageHist[i]))
                {
                    same = false;
                }
            }
            Assert.IsTrue(same);
            Histogram.OutputHistogram(hist0);
        }

        [TestMethod()]
        public void testIndividualHistogram()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat[] imageHist = Histogram.generate(image);

            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 500;

            int gridCount = OpenCV.Util.GetGridCount(gridSize, width, height);
            //Mat firstImage = new Mat(image, new Rect(0, 0, gridSize, gridSize));
            //Mat[] firstImageHist = OpenCV.Histogram.generate(firstImage);
            bool same = true;
            List<Mat[]> histograms = new List<Mat[]>();
            List<Mat[]> histograms2 = new List<Mat[]>();

            for (int i = 0; i < gridCount; i++)
            {
                Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(i, gridSize, width);
                int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
                int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;
                Mat gridImg = new Mat(image, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));
                Mat[] gridImgHist = OpenCV.Histogram.generate(gridImg);

                Mat[] otherImgHist = OpenCV.Util.Deserialize(String.Format("tree_hist{0}_out.bin", i));

                histograms.Add(gridImgHist);
                histograms2.Add(otherImgHist);
            }
            Assert.IsTrue(same);

            OpenCV.Histogram.CombineHistogram(histograms);
            OpenCV.Histogram.CombineHistogram(histograms2);

            same = OpenCV.Histogram.CombineHistogram(histograms[0], histograms2[0]);
            Assert.IsTrue(same);

            same = OpenCV.Histogram.CombineHistogram(histograms[0], imageHist);
            Assert.IsTrue(same);

            OpenCV.Histogram.OutputHistogram(imageHist);
        }


        [TestMethod()]
        public void testDeserializeAllCombine()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat[] imageHist = Histogram.generate(image);
            Mat[] hist1 = OpenCV.Util.Deserialize("tree_hist15_out.bin");

            bool same = true;
            if (imageHist.Length != hist1.Length)
            {
                same = false;
            }

            for (int i = 0; i < imageHist.Length; i++)
            {
                if (!compareMat(imageHist[i], hist1[i]))
                {
                    same = false;
                }
            }
            Assert.IsTrue(same);
            
        }


        [TestMethod()]
        public void testFirstReduction()
        {
            string[] input = {"tree_hist0_out.bin", "tree_hist1_out.bin", "tree_hist3_out.bin", "tree_hist7_out.bin",
                "tree_hist10_out.bin", "tree_hist2_out.bin", "tree_hist4_out.bin", "tree_hist5_out.bin",
                    "tree_hist6_out.bin", "tree_hist9_out.bin" };
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            //Mat[] imageHist = Histogram.generate(image);

            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 500;
            List<Mat[]> histograms = new List<Mat[]>();
            List<Mat[]> histograms2 = new List<Mat[]>();
            bool same = true;

            foreach (string filename in input)
            {
                Mat[] h1 = OpenCV.Util.Deserialize(filename);

                histograms.Add(h1);
                int index = int.Parse(Regex.Match(filename, @"\d+").Value);

                Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(index, gridSize, width);
                int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
                int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;
                Mat gridImg = new Mat(image, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));
                //histograms2.Add(OpenCV.Histogram.generate(gridImg));
                Mat[] h2 = OpenCV.Histogram.generate(gridImg);
                histograms2.Add(h2);
                same = compareHist(h1, h2);
                if (!same)
                {
                    break;
                }
            }

            OpenCV.Histogram.CombineHistogram(histograms);
            OpenCV.Histogram.CombineHistogram(histograms2);

            same = compareHist(histograms[0], histograms2[0]);
            Assert.IsTrue(same);

            Mat[] otherHist = OpenCV.Util.Deserialize("tree_hist13_out.bin");
            same = compareHist(histograms2[0], otherHist);

            OpenCV.Histogram.OutputHistogram(histograms2[0]);
            //OpenCV.Histogram.OutputHistogram(otherHist);
            /*OpenCV.Histogram.CombineHistogram(histograms);

            Mat[] otherHist = OpenCV.Util.Deserialize("tree_hist13_out.bin");

            for (int i = 0; i < histograms.Count; i++)
            {
                same = compareHist(histograms[i], otherHist);
            }*/


            /*for (int i = 0; i < otherHist.Length; i++)
            {
                if (!compareMat(otherHist[i], histograms[0][i]))
                {
                    same = false;
                }
            }*/

            Assert.IsTrue(same);
        }

        [TestMethod()]
        public void testAllReduction()
        {
            string[] input = {"tree_hist0_out.bin", "tree_hist1_out.bin", "tree_hist3_out.bin", "tree_hist7_out.bin",
                "tree_hist10_out.bin", "tree_hist2_out.bin", "tree_hist4_out.bin", "tree_hist5_out.bin",
                    "tree_hist6_out.bin", "tree_hist9_out.bin", "tree_hist8_out.bin", "tree_hist11_out.bin"};
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat[] imageHist = Histogram.generate(image);

            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 500;
            List<Mat[]> histograms = new List<Mat[]>();
            List<Mat[]> histograms2 = new List<Mat[]>();
            bool same = true;

            foreach (string filename in input)
            {
                Mat[] h1 = OpenCV.Util.Deserialize(filename);

                histograms.Add(h1);
                int index = int.Parse(Regex.Match(filename, @"\d+").Value);

                Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(index, gridSize, width);
                int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
                int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;
                Mat gridImg = new Mat(image, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));
                //histograms2.Add(OpenCV.Histogram.generate(gridImg));
                Mat[] h2 = OpenCV.Histogram.generate(gridImg);
                histograms2.Add(h2);
                same = compareHist(h1, h2);
                if (!same)
                {
                    break;
                }
            }

            OpenCV.Histogram.CombineHistogram(histograms);
            OpenCV.Histogram.CombineHistogram(histograms2);

            same = compareHist(histograms[0], histograms2[0]);
            Assert.IsTrue(same);

            OpenCV.Histogram.OutputHistogram(histograms[0]);

            same = compareHist(histograms[0], imageHist);
            Assert.IsTrue(same);
        }

        [TestMethod()]
        public void testSecondReduction()
        {
            string[] input = {"tree_hist8_out.bin", "tree_hist11_out.bin"  };
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            List<Mat[]> histograms = new List<Mat[]>();
            List<Mat[]> histograms2 = new List<Mat[]>();
            int width = image.Cols;
            int height = image.Rows;
            int gridSize = 500;
            bool same = true;

            foreach (string filename in input)
            {
                Mat[] h1 = OpenCV.Util.Deserialize(filename);
                histograms.Add(h1);
                int index = int.Parse(Regex.Match(filename, @"\d+").Value);

                Tuple<int, int> coord = OpenCV.Util.GetGridCoordinate(index, gridSize, width);
                int gridWidth = (coord.Item1 + gridSize) < width ? gridSize : width - coord.Item1;
                int gridHeight = (coord.Item2 + gridSize) < height ? gridSize : height - coord.Item2;
                Mat gridImg = new Mat(image, new Rect(coord.Item1, coord.Item2, gridWidth, gridHeight));
                Mat[] h2 = OpenCV.Histogram.generate(gridImg);
                histograms2.Add(h2);

                same = compareHist(h1, h2);
                if (!same)
                {
                    break;
                }
            }

            Assert.IsTrue(same);
            OpenCV.Histogram.CombineHistogram(histograms);
            OpenCV.Histogram.CombineHistogram(histograms2);

            same = compareHist(histograms[0], histograms2[0]);
            Assert.IsTrue(same);

            Mat[] otherHist = OpenCV.Util.Deserialize("tree_hist14_out.bin");
            same = true;
            OpenCV.Histogram.OutputHistogram(otherHist);

            same = compareHist(histograms[0], otherHist);
            Assert.IsTrue(same);
        }

        [TestMethod()]
        public void testThirdReduction()
        {
            Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            Mat[] imageHist = Histogram.generate(image);

            string[] input = { "tree_hist17_out.bin", "tree_hist18_out.bin" };

            List<Mat[]> histograms = new List<Mat[]>();
            foreach (string filename in input)
            {
                histograms.Add(OpenCV.Util.Deserialize(filename));
            }

            //OpenCV.Histogram.OutputHistogram(histograms[0]);
            //OpenCV.Histogram.OutputHistogram(histograms[1]);
            OpenCV.Histogram.CombineHistogram(histograms);

            //OpenCV.Histogram.OutputHistogram(histograms[0]);
            Mat[] otherHist = OpenCV.Util.Deserialize("tree_hist19_out.bin");
            bool same = true;
            same = compareHist(otherHist, histograms[0]);
            Assert.IsTrue(same);

            OpenCV.Histogram.OutputHistogram(otherHist);
            //OpenCV.Histogram.OutputHistogram(imageHist);

            /*Cv2.Normalize(otherHist[0], otherHist[0], 0, 1, NormTypes.MinMax, -1, null);
            Cv2.Normalize(otherHist[1], otherHist[1], 0, 1, NormTypes.MinMax, -1, null);
            Cv2.Normalize(otherHist[2], otherHist[2], 0, 1, NormTypes.MinMax, -1, null);

            Cv2.Normalize(imageHist[0], imageHist[0], 0, 1, NormTypes.MinMax, -1, null);
            Cv2.Normalize(imageHist[1], imageHist[1], 0, 1, NormTypes.MinMax, -1, null);
            Cv2.Normalize(imageHist[2], imageHist[2], 0, 1, NormTypes.MinMax, -1, null);*/

            //same = compareHist(otherHist, imageHist);

            //same = compareHist(otherHist, imageHist);
            //OpenCV.Histogram.OutputHistogram(otherHist);
            //OpenCV.Histogram.OutputHistogram(imageHist);
            //OpenCV.Histogram.OutputHistogram(histograms[0]);
            //OpenCV.Histogram.OutputHistogram(histograms[1]);
            Assert.IsTrue(same);

        }

        [TestMethod]
        public void stuff()
        {
            for (int i = 18; i <= 18; i++)
            {
                Mat[] otherHist = OpenCV.Util.Deserialize(
                    String.Format("tree_hist{0}_out.bin", i));
                //OpenCV.Histogram.OutputHistogram(otherHist);
                Console.WriteLine(i);
                OpenCV.Histogram.OutputHistogram(otherHist);
            }
            //Mat image = Cv2.ImRead("C:\\Users\\luo029\\Pictures\\tree.jpg", ImreadModes.Color);
            //Mat[] imageHist = Histogram.generate2(image);
        }
    }
}
