namespace IntelligentScissors
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.openButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.snapRadiusNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.cursorSnapCheckBox = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.SeachDistanaceNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.gradientLearningCheckBox = new System.Windows.Forms.CheckBox();
            this.pixelLearningCheckBox = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.noFilterRadioBtn = new System.Windows.Forms.RadioButton();
            this.bilateralBtn = new System.Windows.Forms.RadioButton();
            this.gaussianRadioBtn = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cursoryLabel = new System.Windows.Forms.Label();
            this.cursorxLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.undoBtn = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.snapRadiusNumericUpDown)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeachDistanaceNumericUpDown)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.undoBtn, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(915, 529);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.pictureBox1, 2);
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(909, 443);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.openButton);
            this.flowLayoutPanel1.Controls.Add(this.refreshButton);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(140, 452);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(772, 74);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(694, 3);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(75, 23);
            this.openButton.TabIndex = 0;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(613, 3);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 1;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.snapRadiusNumericUpDown);
            this.panel1.Controls.Add(this.cursorSnapCheckBox);
            this.panel1.Location = new System.Drawing.Point(515, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(92, 71);
            this.panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cursor Control";
            // 
            // snapRadiusNumericUpDown
            // 
            this.snapRadiusNumericUpDown.Dock = System.Windows.Forms.DockStyle.Left;
            this.snapRadiusNumericUpDown.Location = new System.Drawing.Point(0, 0);
            this.snapRadiusNumericUpDown.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.snapRadiusNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.snapRadiusNumericUpDown.Name = "snapRadiusNumericUpDown";
            this.snapRadiusNumericUpDown.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.snapRadiusNumericUpDown.Size = new System.Drawing.Size(40, 20);
            this.snapRadiusNumericUpDown.TabIndex = 3;
            this.snapRadiusNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cursorSnapCheckBox
            // 
            this.cursorSnapCheckBox.AutoSize = true;
            this.cursorSnapCheckBox.Location = new System.Drawing.Point(3, 26);
            this.cursorSnapCheckBox.Name = "cursorSnapCheckBox";
            this.cursorSnapCheckBox.Size = new System.Drawing.Size(81, 17);
            this.cursorSnapCheckBox.TabIndex = 4;
            this.cursorSnapCheckBox.Text = "cursor snap";
            this.cursorSnapCheckBox.UseVisualStyleBackColor = true;
            this.cursorSnapCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.SeachDistanaceNumericUpDown);
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(427, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(82, 71);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // SeachDistanaceNumericUpDown
            // 
            this.SeachDistanaceNumericUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.SeachDistanaceNumericUpDown.Location = new System.Drawing.Point(3, 3);
            this.SeachDistanaceNumericUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.SeachDistanaceNumericUpDown.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.SeachDistanaceNumericUpDown.Name = "SeachDistanaceNumericUpDown";
            this.SeachDistanaceNumericUpDown.Size = new System.Drawing.Size(60, 20);
            this.SeachDistanaceNumericUpDown.TabIndex = 0;
            this.SeachDistanaceNumericUpDown.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.SeachDistanaceNumericUpDown.ValueChanged += new System.EventHandler(this.SeachDistanaceNumericUpDown_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Search Distance";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.gradientLearningCheckBox);
            this.panel2.Controls.Add(this.pixelLearningCheckBox);
            this.panel2.Location = new System.Drawing.Point(303, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(118, 71);
            this.panel2.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Learning";
            // 
            // gradientLearningCheckBox
            // 
            this.gradientLearningCheckBox.AutoSize = true;
            this.gradientLearningCheckBox.Location = new System.Drawing.Point(9, 25);
            this.gradientLearningCheckBox.Name = "gradientLearningCheckBox";
            this.gradientLearningCheckBox.Size = new System.Drawing.Size(66, 17);
            this.gradientLearningCheckBox.TabIndex = 1;
            this.gradientLearningCheckBox.Text = "Gradient";
            this.gradientLearningCheckBox.UseVisualStyleBackColor = true;
            this.gradientLearningCheckBox.CheckedChanged += new System.EventHandler(this.gradientLearningCheckBox_CheckedChanged);
            // 
            // pixelLearningCheckBox
            // 
            this.pixelLearningCheckBox.AutoSize = true;
            this.pixelLearningCheckBox.Location = new System.Drawing.Point(9, 5);
            this.pixelLearningCheckBox.Name = "pixelLearningCheckBox";
            this.pixelLearningCheckBox.Size = new System.Drawing.Size(53, 17);
            this.pixelLearningCheckBox.TabIndex = 0;
            this.pixelLearningCheckBox.Text = "Pixels";
            this.pixelLearningCheckBox.UseVisualStyleBackColor = true;
            this.pixelLearningCheckBox.CheckedChanged += new System.EventHandler(this.pixelLearningCheckBox_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.noFilterRadioBtn);
            this.panel3.Controls.Add(this.bilateralBtn);
            this.panel3.Controls.Add(this.gaussianRadioBtn);
            this.panel3.Location = new System.Drawing.Point(191, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(106, 71);
            this.panel3.TabIndex = 8;
            // 
            // noFilterRadioBtn
            // 
            this.noFilterRadioBtn.AutoSize = true;
            this.noFilterRadioBtn.Checked = true;
            this.noFilterRadioBtn.Location = new System.Drawing.Point(7, 46);
            this.noFilterRadioBtn.Name = "noFilterRadioBtn";
            this.noFilterRadioBtn.Size = new System.Drawing.Size(61, 17);
            this.noFilterRadioBtn.TabIndex = 5;
            this.noFilterRadioBtn.TabStop = true;
            this.noFilterRadioBtn.Text = "No filter";
            this.noFilterRadioBtn.UseVisualStyleBackColor = true;
            this.noFilterRadioBtn.CheckedChanged += new System.EventHandler(this.noFilterRadioBtn_CheckedChanged);
            // 
            // bilateralBtn
            // 
            this.bilateralBtn.AutoSize = true;
            this.bilateralBtn.Location = new System.Drawing.Point(7, 25);
            this.bilateralBtn.Name = "bilateralBtn";
            this.bilateralBtn.Size = new System.Drawing.Size(62, 17);
            this.bilateralBtn.TabIndex = 4;
            this.bilateralBtn.Text = "Bilateral";
            this.bilateralBtn.UseVisualStyleBackColor = true;
            this.bilateralBtn.CheckedChanged += new System.EventHandler(this.bilateralBtn_CheckedChanged);
            // 
            // gaussianRadioBtn
            // 
            this.gaussianRadioBtn.AutoSize = true;
            this.gaussianRadioBtn.Location = new System.Drawing.Point(7, 4);
            this.gaussianRadioBtn.Name = "gaussianRadioBtn";
            this.gaussianRadioBtn.Size = new System.Drawing.Size(69, 17);
            this.gaussianRadioBtn.TabIndex = 3;
            this.gaussianRadioBtn.Text = "Gaussian";
            this.gaussianRadioBtn.UseVisualStyleBackColor = true;
            this.gaussianRadioBtn.CheckedChanged += new System.EventHandler(this.gaussianRadioBtn_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cursoryLabel);
            this.panel5.Controls.Add(this.cursorxLabel);
            this.panel5.Location = new System.Drawing.Point(86, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(99, 71);
            this.panel5.TabIndex = 10;
            // 
            // cursoryLabel
            // 
            this.cursoryLabel.AutoSize = true;
            this.cursoryLabel.Location = new System.Drawing.Point(3, 29);
            this.cursoryLabel.Name = "cursoryLabel";
            this.cursoryLabel.Size = new System.Drawing.Size(26, 13);
            this.cursoryLabel.TabIndex = 1;
            this.cursoryLabel.Text = "Y: 0";
            // 
            // cursorxLabel
            // 
            this.cursorxLabel.AutoSize = true;
            this.cursorxLabel.Location = new System.Drawing.Point(3, 6);
            this.cursorxLabel.Name = "cursorxLabel";
            this.cursorxLabel.Size = new System.Drawing.Size(26, 13);
            this.cursorxLabel.TabIndex = 0;
            this.cursorxLabel.Text = "X: 0";
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(569, 80);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 100);
            this.panel4.TabIndex = 9;
            // 
            // undoBtn
            // 
            this.undoBtn.Location = new System.Drawing.Point(3, 452);
            this.undoBtn.Name = "undoBtn";
            this.undoBtn.Size = new System.Drawing.Size(75, 23);
            this.undoBtn.TabIndex = 9;
            this.undoBtn.Text = "Undo";
            this.undoBtn.UseVisualStyleBackColor = true;
            this.undoBtn.Click += new System.EventHandler(this.undoBtn_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|BMP Files (*.bmp)|*.bmp|All file" +
    "s (*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 529);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "CGA Lasso Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.snapRadiusNumericUpDown)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeachDistanaceNumericUpDown)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox cursorSnapCheckBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown snapRadiusNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.NumericUpDown SeachDistanaceNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox gradientLearningCheckBox;
        private System.Windows.Forms.CheckBox pixelLearningCheckBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton noFilterRadioBtn;
        private System.Windows.Forms.RadioButton bilateralBtn;
        private System.Windows.Forms.RadioButton gaussianRadioBtn;
        private System.Windows.Forms.Button undoBtn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label cursoryLabel;
        private System.Windows.Forms.Label cursorxLabel;
        private System.Windows.Forms.Panel panel4;
    }
}

