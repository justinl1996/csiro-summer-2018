using ScissorTool;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Numerics;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using OpenCvSharp.Extensions;


// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace IntelligentScissors
{
    public partial class Form1 : Form
    {
        private ScissorTool.Cost cost;
        private ScissorTool.PixelGraph pixelGraph;

        private Bitmap originalImage;
        private Bitmap editImage;
        private Bitmap permanentImage;
        private List<Vector2> currentPath;
        private Segment segment;
        private SegmentContainer segmentContainer;

        //is it the first click
        private bool firstClick = false;

        //how far should the cursor snap work
        private int cursorSnapDistance = 0;

        //how far should we perform Dykstra from seed point
        private int searchDistance = 0;
            
        //for learning
        private bool includePixelHistory = false;
        private bool includeGradientHistory = false;

        //indicate if tracing a segment
        private bool activeSegment = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Console.WriteLine("Form1_Load");
            cost = new ScissorTool.Cost();
            updateCursorSnapState();
            updateSearchDistanceValue();

        }

        private void openButton_Click(object sender, EventArgs e)
        {
            /*if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {*/
            //string filename = openFileDialog1.FileName;
            //string filename = "C:\\Users\\luo029\\Pictures\\lasso\\small4.png";
            //string filename = "C:\\Users\\luo029\\Pictures\\lasso\\noisy4.png";
            string filename = "C:\\Users\\luo029\\Pictures\\lasso\\noisy1.png";

            cost.LoadImage(filename);
            pixelGraph = new PixelGraph(cost, cost.img.Rows, cost.img.Cols);
            //pixelGraph.Search(new Vector2(cost.img.Rows/2, cost.img.Cols/2), 
            //    searchDistance, includePixelHistory, includeGradientHistory);
            reset();
        }
        
        /// <summary>
        /// Reset image and start fresh
        /// </summary>
        private void reset()
        {
            currentPath = new List<Vector2>();
            segment = new Segment();
            segmentContainer = new SegmentContainer();

            //convert to bitmap to be used in picturebox
            originalImage = new Bitmap(BitmapConverter.ToBitmap(cost.img));
            permanentImage = new Bitmap(BitmapConverter.ToBitmap(cost.img));
            editImage = new Bitmap(BitmapConverter.ToBitmap(cost.img));
            
            pictureBox1.Image = editImage;
            pictureBox1.Refresh();

            firstClick = true;

            //resize form to accomodate for image
            Width = Math.Max(Width, editImage.Width + 20);
            Height = editImage.Height + 200;
            Refresh();
        }

        /// <summary>
        /// Draw a point on a bitmap image
        /// </summary>
        /// <param name="point">x, y position</param>
        /// <param name="image">image to draw point to</param>
        /// <param name="penColor">colour of point</param>
        /// <param name="size">size of point</param>
        private void DrawPoint(Vector2 point, Image image, Color penColor, int size)
        {
            Graphics graphics = Graphics.FromImage(image);
            Brush brush = new SolidBrush(penColor);
            graphics.FillEllipse(brush, point.X - size, point.Y - size, size * 2, size * 2);
        }

        /// <summary>
        /// Draw a path on a bitmap image
        /// </summary>
        /// <param name="path">positions to draw path </param>
        /// <param name="image">image to draw point to</param>
        /// <param name="penColor">colour of path</param>
        private void DrawPath(IEnumerable<Vector2> path, Image image, 
            Color penColor)
        {
            Graphics graphics = Graphics.FromImage(image);
            Pen pen = new Pen(penColor, 3);
            Vector2 previous = new Vector2(-1);

            foreach (Vector2 position in path) 
            {
                if (previous.X != -1)
                {
                    graphics.DrawLine(pen, previous.X, previous.Y, position.X, position.Y);
                }
                previous = position;
            }
        }

        /// <summary>
        /// Get the closest seed point within the given radius
        /// </summary>
        /// <param name="pos">starting position</param>
        /// <param name="radius">radius from current position</param>
        /// <returns>Ois</returns>
        private Vector2 GetNearSeed(Vector2 pos, float radius)
        {
            float minDist = float.MaxValue;
            Vector2 minPos = new Vector2(-1);

            foreach (Vector2 seed in segment.GetSeedPoints())
            {
                Vector2 dist = new Vector2(pos.X - seed.X, pos.Y - seed.Y);
                if (dist.Length() < minDist && dist.Length() < radius)
                {
                    minDist = dist.Length();
                    minPos = seed;
                }
            };
            return minPos;
        }

        /// <summary>
        /// Check if position is near the start position within a given radius
        /// </summary>
        /// <param name="x">x position</param>
        /// <param name="y">y position</param>
        /// <param name="radius">radius from current position</param>
        /// <returns>True if near, otherwise False</returns>
        private bool IsNearStartPoint(int x, int y, float radius)
        {
            Vector2 dist = new Vector2(x - segment.GetFirstPoint().X, 
                y - segment.GetFirstPoint().Y);

            return dist.Length() < radius;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("Mouse Click: " + e.X + ", " + e.Y);
            if (e.X < cost.img.Cols && e.Y < cost.img.Rows)
            {
                //firstClick = true;
                //Console.WriteLine(segment.IsEmpty());
                if (activeSegment == true || e.Button == MouseButtons.Right)
                {
                    startNewPath();
                    activeSegment = true;
                }
                //completed path -> draw green path
                if (e.Button == MouseButtons.Left)
                {
                    DrawPath(currentPath, permanentImage, Color.Green);
                    
                    //add to history
                    cost.AddVisitedPositions(segment.GetLastVisited(Cost.HistoryCount));
                    segment.Add(currentPath);
                    
                    activeSegment = true;

                    if (segment.isClosed())
                    {
                        closePath();
                        activeSegment = false;
                    } 
                }
            }
        }


        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("Mouse Move: " + e.X + ", " + e.Y);
            if (firstClick == true)
            {
                //copy fresh image without (previous trace)
                editImage = (Bitmap) permanentImage.Clone(); 
                if (e.X < cost.img.Cols && e.Y < cost.img.Rows)
                {
                    Vector2 snapPos;
                    if (IsNearStartPoint(e.X, e.Y, 15))
                    {
                        //stick to start point
                        snapPos = segment.GetFirstPoint();
                    }
                    else if (cursorSnapDistance != 0)
                    {
                        //stick to gradient
                        snapPos = pixelGraph.GetMaxGradientPoint(new Vector2(e.X, e.Y), 
                            cursorSnapDistance);
                    }
                    else
                    {
                        //don't stick to anything
                        snapPos = new Vector2(e.X, e.Y);
                    }

                    //Vector2 maxPos = new Vector2(e.X, e.Y);
                    if (snapPos.X >= 0 && snapPos.Y >= 0)
                    {
                        //trace path back to seed point
                        currentPath = pixelGraph.TracePath(snapPos);

                        //draw the white path back to seed point
                        DrawPath(currentPath, editImage, Color.AntiqueWhite); 
                    }
                }

                //set to picturebox
                pictureBox1.Image = editImage;
                pictureBox1.Refresh();
            }
            cursoryLabel.Text = "Y: " + e.Y;
            cursorxLabel.Text = "X: " + e.X;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            updateCursorSnapState();
        }

        private void numericUpDown_ValueChanged(object sender, EventArgs e)
        {

        }

        private void updateCursorSnapState()
        {
            if (cursorSnapCheckBox.CheckState == CheckState.Checked)
            {
                cursorSnapDistance = Decimal.ToInt32(snapRadiusNumericUpDown.Value);
            } else
            {
                cursorSnapDistance = 0;
            }
        }

        private void updateSearchDistanceValue()
        {
            searchDistance = Decimal.ToInt32(SeachDistanaceNumericUpDown.Value);
        }

        private void SeachDistanaceNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            updateSearchDistanceValue();
        }

        private void pixelLearningCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            includePixelHistory = pixelLearningCheckBox.CheckState == CheckState.Checked;
        }

        private void gradientLearningCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            includeGradientHistory = gradientLearningCheckBox.CheckState == CheckState.Checked;
        }


        private void bilateralBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (cost.Loaded && bilateralBtn.Checked == true)
            {
                cost.BilateralFilter();
                reset();
            }
        }

        private void noFilterRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (cost.Loaded && noFilterRadioBtn.Checked == true)
            {
                cost.NoFilter();
                reset();
            }
        }

        private void gaussianRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (cost.Loaded && gaussianRadioBtn.Checked == true)
            {
                cost.Gaussianfilter();
                reset();
            }
        }

        private void closePath()
        {
            //outline completed segment with blue
            foreach(List<Vector2> section in segment.GetPaths())
            {
                DrawPath(section, permanentImage, Color.Blue);
            }
            //add segment to container and start a new one
            segmentContainer.Add(segment);
            segment = new Segment();

            pixelGraph.Reset();

            //store and display image
            editImage = (Bitmap)permanentImage.Clone();
            pictureBox1.Image = permanentImage;
            pictureBox1.Refresh();

            //new current path
            currentPath = new List<Vector2>();
        }

        private void startNewPath()
        {
            //draw red point at start of path
            Vector2 startPos = currentPath.First();
            DrawPoint(currentPath.First(), permanentImage, Color.Red, 3);

            //perform a fresh search
            pixelGraph.Reset();
            pixelGraph.Search(startPos, searchDistance,
                includePixelHistory, includeGradientHistory);
        }

        private void redraw(Bitmap image)
        {
            void redraw(Segment segment)
            {
                Color color = segment.isClosed() ? Color.Blue : Color.Green;
                //Color color = Color.Green;
                foreach (List<Vector2> section in segment.GetPaths())
                {
                    DrawPath(section, image, color);
                }
                foreach (Vector2 pos in segment.GetSeedPoints())
                {
                    DrawPoint(pos, image, Color.Red, 3);
                }
            }

            //draw the completed segments
            foreach (Segment s in segmentContainer.GetSegents())
            {
                redraw(s);
            }
            //draw incomplete segments
            redraw(segment);
        }

        private void undoBtn_Click(object sender, EventArgs e)
        {
            if (!segment.Undo()) //no more paths in segment to undo
            {
                //get segment from previous completed segment 
                segment = segmentContainer.RemoveLast();

                if (segment == null)
                {
                    //no more segments start afresh
                    segment = new Segment();
                }
                else
                {
                    //otherwise remove a path
                    segment.Undo();
                }
            } 
            //start from clean image
            permanentImage = (Bitmap)originalImage.Clone();
            editImage = (Bitmap) originalImage.Clone();
            pixelGraph.Reset();

            //segment is not empty so do a search from last start position
            if (!segment.IsEmpty())
            {
                Vector2 startPos = segment.GetLastPoint();
                if (startPos.X > 0 && startPos.Y > 0)
                {
                    //note we halve the search distance for performance 
                    pixelGraph.Search(startPos, searchDistance/2,
                        includePixelHistory, includeGradientHistory);
                }
            } else
            {
                firstClick = true;
            }

            //redraw everything
            redraw(permanentImage);
            redraw(editImage);

            pictureBox1.Image = editImage;
            pictureBox1.Refresh();
        }

        override
        protected bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Console.WriteLine(msg);
            return true;
        }
    }
}
