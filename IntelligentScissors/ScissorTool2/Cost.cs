﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using OpenCvSharp;


namespace ScissorTool
{
    public class Cost
    {
        //before filter
        public Mat prefilterImg;
        //after filter
        public Mat img;

        //store in array (avoid using OpenCV.Mat as access is slow)
        private float[,] gradMagX;
        private float[,] gradMagY;
        private byte[,] laplace;
        public byte[,] original;
        private CircularList<Vector2> visitHistory;

        //gradient direction
        //public Mat gradDir;

        //histogram for learning pixel intensities
        private Histogram insidePixelHist;
        private Histogram outsidePixelHist;
        private Histogram centerPixelHist;
        private Histogram gradientHist;
        public bool Loaded { get; private set; }

        public const int HistoryCount = 32;

        /// <summary>
        /// Cost Class: Class for modelling of the 
        /// </summary>
        public Cost()
        {
            img = new Mat();
            prefilterImg = new Mat();
            visitHistory = new CircularList<Vector2>(HistoryCount);
        }

        /// <summary>
        /// Loads images, intializes data structures and image pre-processing
        /// </summary>
        /// <param name="filename"></param>
        public void LoadImage(String filename)
        {
            Loaded = true;
   
            prefilterImg = Cv2.ImRead(filename, ImreadModes.Color);
            Cv2.CvtColor(prefilterImg, prefilterImg, ColorConversionCodes.BGR2GRAY);
            img = prefilterImg.Clone();

            insidePixelHist = new Histogram(255);
            outsidePixelHist = new Histogram(255);
            centerPixelHist = new Histogram(255);

            //maximum possible gradient 361 = sqrt(255^2 + 255^2) 
            gradientHist = new Histogram(361);

            gradMagY = new float[img.Rows, img.Cols];
            gradMagX = new float[img.Rows, img.Cols];
            laplace = new byte[img.Rows, img.Cols];
            original = new byte[img.Rows, img.Cols];

            PreCompute();
        }

        /// <summary>
        /// Apply bilateral filter and perform pre-processing
        /// </summary>
        public void BilateralFilter()
        {
            Cv2.BilateralFilter(prefilterImg, img, 9, 100, 100);
            PreCompute();
        }

        /// <summary>
        /// Apply bilateral filter and perform pre-processing
        /// </summary>
        public void Gaussianfilter()
        {
            Cv2.GaussianBlur(prefilterImg, img, new Size(3, 3), 0);
            PreCompute();
        }

        /// <summary>
        /// Apply original image (remove filter) and perform pre-processing
        /// </summary>
        public void NoFilter()
        {
            img = prefilterImg.Clone();
            PreCompute();
        }

        /// <summary>
        /// Get 1st order gradient vector 
        /// </summary>
        /// <param name="p">pixel position to get gradient</param>
        /// <returns>gradient vector (dx, dy) </returns>
        public Vector2 GetGradient(Vector2 p)
        {
            int row = (int)p.Y;
            int col = (int)p.X;

            return new Vector2(gradMagX[row, col], gradMagY[row, col]);
        }

        /// <summary>
        /// Return a position that is contained inside an image
        /// </summary>
        /// <param name="pos">pixel position (row, col) to fix</param>
        /// <returns>same position if already contained, otherwise fixed to edge</returns>
        private Vector2 fixInsideImg(Vector2 pos)
        {
            if (pos.X < 0 )
            {
                pos.X = 0;
            } else if (pos.X >= img.Cols)
            {
                pos.X = img.Cols - 1;
            }

            if (pos.Y < 0)
            {
                pos.Y = 0;
            }
            else if (pos.Y >= img.Rows)
            {
                pos.Y = img.Rows - 1;
            }
            return pos;
        }

        /// <summary>
        /// Get the pixel position on either side of the edge (opposite 
        /// direction of gradient)
        /// </summary>
        /// <param name="pos">center position</param>
        /// <returns>Inside position, Outside position</returns>
        private Tuple<Vector2, Vector2> GetInsideOutsidePixel(Vector2 pos)
        {
            Vector2 gradient = GetGradient(pos);
            if (gradient.Length() != 0)
            {
                gradient /= gradient.Length();
            }

            //makesure pos is inside image
            Vector2 insideP = fixInsideImg(pos + 8 * gradient);
            Vector2 outsideP = fixInsideImg(pos - 8 * gradient);

            return new Tuple<Vector2, Vector2>(insideP, outsideP);
        }

        /// <summary>
        /// Precompute laplacian + gradient and copy to array
        /// </summary>
        public void PreCompute()
        {
            Mat laplaceMat = new Mat();

            Cv2.Laplacian(img, laplaceMat, MatType.CV_8U);
            Cv2.Threshold(laplaceMat, laplaceMat, 0, 1, ThresholdTypes.Binary);

            CopyToArray(laplaceMat, laplace);
            CopyToArray(img, original);

            //faster to compute gradient this way
            for (int row = 0; row < original.GetLength(0) - 1; row++)
            {
                for (int col = 0; col < original.GetLength(1) - 1; col++)
                {
                    //pixel to right
                    int x = original[row, col] - original[row, col + 1] ;
                    //pixel below
                    int y = original[row, col] - original[row + 1, col];

                    //magnitude + direction
                    double mag = Math.Sqrt(x * x + y * y);
                    double edgeAngle = Math.Atan2(y, x);

                    double rotatedEdgeAngle = edgeAngle + Math.PI / 2.0;
                    gradMagX[row, col] = (float) Math.Abs(Math.Cos(rotatedEdgeAngle) * mag);
                    gradMagY[row, col] = (float) Math.Abs(Math.Sin(rotatedEdgeAngle) * mag);
                }
            }
        }

        /// <summary>
        /// Compute total cost to go from p to q (should be neighbouring)
        /// </summary>
        /// <param name="p">start pixel position</param>
        /// <param name="q">end pixel position</param>
        /// <param name="includePixelHistory">True to include visited pixels in cost</param>
        /// <param name="includeGradHistory">True to include past gradient in cost</param>
        /// <returns>value corresponding to cost</returns>
        public double GetTotalCost(Vector2 p, Vector2 q, bool includePixelHistory, 
            bool includeGradHistory)
        {
            int pRow = (int)p.Y;
            int pCol = (int)p.X;
            int qRow = (int)q.Y;
            int qCol = (int)q.X;
            
            //so it snaps to image edge
            if (qRow == 0 || qRow == img.Rows - 2 || qCol == img.Cols - 2 || qCol == 0)
            {
                return 0.0f;
            }

            //can only be above, below, right or left
            float magnitude;
            if (p.X < q.X)
            {
                magnitude = gradMagX[pRow, pCol];
            } else if (q.X < p.X)
            {
                magnitude = gradMagX[qRow, qCol];
            } else if (p.Y < q.Y)
            {
                magnitude = gradMagY[pRow, pCol];
            } else
            {
                magnitude = gradMagY[qRow, qCol];
            }
            if (magnitude == 0)
            {
                return 1E30f;
            }

            double a = 0, b = 0, c = 0, d = 0;
            if (includePixelHistory)
            {
                //get inside, center, outside pixel and compute cost, 
                // relative to past pixels
                Tuple<Vector2, Vector2> inOutPixel = GetInsideOutsidePixel(q);

                byte insideQ = original[(int)inOutPixel.Item1.Y, (int)inOutPixel.Item1.X];
                byte outsideQ = original[(int)inOutPixel.Item2.Y, (int)inOutPixel.Item2.X];
                byte centerQ = original[qRow, qCol];

                a = insidePixelHist.GetCost(insideQ);
                b = outsidePixelHist.GetCost(outsideQ);
                c = centerPixelHist.GetCost(centerQ);

            } 
            if (includeGradHistory)
            {
                //get gradient and compute cost, 
                // relative to past gradients
                double gradX = gradMagX[qRow, qCol];
                double gradY = gradMagY[qRow, qCol];
                d = gradientHist.GetCost((int) 
                    Math.Sqrt(gradX * gradX + gradY * gradY));
            }

            //note: weights were empirically determined 
            return 0.2f * laplace[pRow, pCol] + (0.4f / magnitude) + 
                0.05f * a + 0.05f * b + 0.05f * c + 0.25f * d; 
        }

        /// <summary>
        /// Updates histograms with previous positions
        /// </summary>
        /// <param name="positions"></param>
        public void AddVisitedPositions(List<Vector2> positions)
        {
            visitHistory.Add(positions);

            insidePixelHist.Clear();
            outsidePixelHist.Clear();
            centerPixelHist.Clear();
            gradientHist.Clear();

            //iterate starting from most recent first
            for (int i = 0; i < visitHistory.Count; i++)
            {
                Vector2 pos = visitHistory[i];
                Tuple<Vector2, Vector2> inOutPixel = GetInsideOutsidePixel(pos);
                Vector2 insideP = inOutPixel.Item1;
                Vector2 outsideP = inOutPixel.Item2;

                //4x more for most recent, decreasing linearly
                int weight = (visitHistory.Count - i) / (visitHistory.Count / 4);

                //add to pixel intensity + gradient to histogram
                insidePixelHist.Increment(original[(int)insideP.Y, (int)insideP.X], weight);
                outsidePixelHist.Increment(original[(int)outsideP.Y, (int)outsideP.X], weight);
                centerPixelHist.Increment(original[(int)pos.Y, (int)pos.X], weight);

                double gradX = gradMagX[(int)pos.Y, (int)pos.X];
                double gradY = gradMagY[(int)pos.Y, (int)pos.X];
                //Console.WriteLine((int)Math.Sqrt(gradX * gradX + gradY * gradY));
                gradientHist.Increment((int)Math.Sqrt(gradX * gradX + gradY * gradY), weight);
            }
            
            insidePixelHist.SmoothAndInvertHistogram(1);
            outsidePixelHist.SmoothAndInvertHistogram(1);
            centerPixelHist.SmoothAndInvertHistogram(1);
            gradientHist.SmoothAndInvertHistogram(1);
        }

        /// <summary>
        /// Copy from mat to array for faster access
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src">source Mat</param>
        /// <param name="dst">destination array</param>
        private void CopyToArray<T>(Mat src, T [,] dst) where T: struct
        {
            for (int r = 0; r < src.Rows; r++)
            {
                for (int c = 0; c < src.Cols; c++)
                {
                    dst[r, c] = src.At<T>(r, c);
                }
            }
        }
    }
}
