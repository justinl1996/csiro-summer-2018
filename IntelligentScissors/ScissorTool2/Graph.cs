﻿using System;
using System.Numerics;
using System.Collections.Generic;

namespace ScissorTool
{
    /// <summary>
    /// PixelNode class: Represents a node (pixel) in the graph
    /// </summary>
    public class PixelNode :IComparable
    {
        int row, col;

        public double Cost { get; set; } = Double.MaxValue;
        public PixelNode Parent { get; set; } = null;

        public PixelNode(Vector2 pos)
        {
            this.row = (int) pos.Y;
            this.col = (int) pos.X;
        }

        public PixelNode(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        /// <summary>
        /// CompareTo for Pixel node based of cost to get to adjacent nodes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>-1 if less, 0 if same, 1 if greater</returns>
        public int CompareTo(object obj)
        {
            double travelCost = Cost - ((PixelNode)obj).Cost;
            if (travelCost > 0)
            {
                return 1;
            } else if (travelCost < 0)
            {
                return -1;
            }
            return 0;
        }

        /// <summary>
        /// Generate neighbouring positions given current row, column
        /// </summary>
        /// <param name="row">start row</param>
        /// <param name="col">start column</param>
        /// <param name="maxRow">maximum row possible</param>
        /// <param name="maxCol">maximum column possible</param>
        /// <returns>all neighbouring positions</returns>
        public static List<Vector2> GetNeighbours(int row, int col, int maxRow, int maxCol)
        {
            int[,] dirs = new int[,] { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
            List<Vector2> result = new List<Vector2>();

            for (int x = 0; x < dirs.GetLength(0); x++)
            {
                int neighbourRow = row + dirs[x, 0];
                int neighbourCol = col + dirs[x, 1];
                if (neighbourRow < maxRow - 1 && neighbourCol < maxCol - 1 &&
                    neighbourRow >= 0 && neighbourCol >= 0)
                {
                    result.Add(new Vector2(neighbourCol, neighbourRow)); //note X => col, Y => row
                }
            }
            return result;
        }

        /// <summary>
        /// Get neighbouring position positions
        /// </summary>
        /// <param name="maxRow">maximum row possible</param>
        /// <param name="maxCol">maximum column possible</param>
        /// <returns>all neighbouring positions</returns>
        public List<Vector2> GetNeighbours(int maxRow, int maxCol)
        {
            return GetNeighbours(row, col, maxRow, maxCol);
        }

        /// <summary>
        /// Get pixel position
        /// </summary>
        /// <returns>x, y position</returns>
        public Vector2 GetPos()
        {
            return new Vector2(col, row);
        }

        /// <summary>
        /// Get unique key
        /// </summary>
        /// <returns>string representing pixel position</returns>
        public string GetKey()
        {
            return row.ToString() + " " + col.ToString(); 
        }
    }

    /// <summary>
    /// PixelGraph class: Graph consisting of pixels as node
    /// </summary>
    public class PixelGraph
    {
        //model for the computing the cost
        Cost costModel;

        PixelNode SeedNode { get; set; }
        private PixelNode [,] pixels;
        
        //max row + column of image
        int maxRow, maxCol;

        /// <summary>
        /// Constructor for PixelGraph
        /// </summary>
        /// <param name="costModel">cost object</param>
        /// <param name="maxRow">maximum row of image</param>
        /// <param name="maxCol">maximum column of image</param>
        public PixelGraph(Cost costModel, int maxRow, int maxCol)
        {
            pixels = new PixelNode[maxRow, maxCol];
            for (int i = 0; i < maxRow; i++)
            {
                for (int j = 0; j < maxCol; j++)
                {
                    pixels[i, j] = new PixelNode(i, j);
                    pixels[i, j].Parent = null;
                }
            }
            this.maxRow = maxRow;
            this.maxCol = maxCol;
            this.costModel = costModel;
        }

        /// <summary>
        /// Reset graph, call this before performing a search
        /// </summary>
        public void Reset()
        {
            for (int i = 0; i < maxRow; i++)
            {
                for (int j = 0; j < maxCol; j++)
                {
                    pixels[i, j].Cost = Double.MaxValue;
                    pixels[i, j].Parent = null;
                }
            }
        }

        /// <summary>
        /// Trace path back to seed node, following parent pointers back
        /// </summary>
        /// <param name="startPosition">pixel position to start</param>
        /// <returns>Path to seed node</returns>
        public List<Vector2> TracePath(Vector2 startPosition)
        {
            List<Vector2> result = new List<Vector2>();
            PixelNode currentNode = pixels[(int)startPosition.Y, (int)startPosition.X];
            do
            {
                result.Add(currentNode.GetPos());
                currentNode = currentNode.Parent;
            } while (currentNode != null);
            return result;
        }

        /// <summary>
        /// BFS to find a point with maximum gradient
        /// </summary>
        /// <param name="startPosition">location to start position</param>
        /// <param name="distance">distance to search from start (no. of search levels)</param>
        /// <returns>position with maximum gradient</returns>
        public Vector2 GetMaxGradientPoint(Vector2 startPosition, int distance)
        {
            //queue containing position + level
            Queue<Tuple<Vector2, int>> queue = new Queue<Tuple<Vector2, int>>();
            //HashSet<string> visited = new HashSet<string>();
            bool[,] visited = new bool[maxRow, maxCol];
            float maxGradient = float.MinValue;
            Vector2 maxGradientPos = startPosition;
            queue.Enqueue(new Tuple<Vector2, int>(startPosition, 0));

            while (queue.Count != 0)
            {
                Tuple<Vector2, int> pixel = queue.Dequeue();
                Vector2 pos = pixel.Item1;
                int level = pixel.Item2;
                //visited.Add(pos.ToString());
                visited[(int)pos.Y, (int)pos.X] = true;

                Vector2 gradient = costModel.GetGradient(pos);

                //back to the seed node 
                if (SeedNode == null || pos == SeedNode.GetPos())
                {
                    return pos;
                }

                //we find a larger gradient
                if (gradient.Length() > maxGradient) {
                    maxGradient = gradient.Length();
                    maxGradientPos = pos;
                }

                //add neighbours to queue
                List<Vector2> neighbours = PixelNode.GetNeighbours((int)pos.Y, 
                    (int)pos.X, maxRow, maxCol);
                foreach (Vector2 neighbour in neighbours)
                {
                    if (!visited[(int) neighbour.Y, (int) neighbour.X] && level + 1 < distance)
                    {
                        queue.Enqueue(new Tuple<Vector2, int>(neighbour, level + 1));
                    }
                }
            }

            return maxGradientPos;
        }

        /// <summary>
        /// Dijkstra search starting from seedPos
        /// </summary>
        /// <param name="seedPos">source pixel position</param>
        /// <param name="radius">distance to go (large=>slower)</param>
        /// <param name="includePixelHistory">include pixel history in path cost calculation</param>
        /// <param name="includeGradHistory">include gradient history in path cost calculation</param>
        public void Search(Vector2 seedPos, int radius, 
            bool includePixelHistory, bool includeGradHistory)
        {
            SeedNode = pixels[(int)seedPos.Y, (int)seedPos.X]; ;
            SeedNode.Cost = 0;

            VCSKicksCollection.PriorityQueue<PixelNode> pq = 
                new VCSKicksCollection.PriorityQueue<PixelNode>();
            //HashSet<String> visited = new HashSet<String>();
            bool [,] visited = new bool[maxRow, maxCol];

            pq.Enqueue(SeedNode);

            while (pq.IsEmpty() == false) 
            {
                //expanded node -> we know this is the shortest path
                PixelNode node = pq.Dequeue();
                //visited.Add(node.GetKey());
                visited[(int)node.GetPos().Y, (int)node.GetPos().X] = true;

                //get left, right, up, down neighbours
                foreach (Vector2 vec in node.GetNeighbours(maxRow, maxCol))
                {
                    PixelNode neighbour = pixels[(int)vec.Y, (int)vec.X];

                    //node has already been previous expanded
                    //if (visited.Contains(neighbour.GetKey()) || 
                    if (visited[(int)neighbour.GetPos().Y, (int)neighbour.GetPos().X] == true ||
                        Vector2.Distance(vec, seedPos) > radius)
                    {
                        continue;
                    }
                    double cost = node.Cost + costModel.GetTotalCost(node.GetPos(), 
                        neighbour.GetPos(), includePixelHistory, includeGradHistory);
                    //we find a better cost        
                    if (cost < neighbour.Cost)
                    {
                        neighbour.Cost = cost;
                        neighbour.Parent = node;
                        pq.Enqueue(neighbour);
                    }
                }
            }
        }
    }
}
