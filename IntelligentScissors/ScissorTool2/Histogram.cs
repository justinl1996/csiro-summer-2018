﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Statistics;

namespace ScissorTool
{
    /// <summary>
    /// Histogram for recording past gradient + pixel values
    /// </summary>
    public class Histogram
    {
        //data values
        private List<double> frequency;

        //for smoothed histogram
        private List<double> smoothHist;

        //max value of histogram
        private int range;

        /// <summary>
        /// Constructor for Histogram
        /// </summary>
        /// <param name="range">max value of histogram</param>
        public Histogram(int range)
        {
            this.range = range;
            frequency = new List<double>(range);
            smoothHist = new List<double>(range);
            for (int i = 0; i < range; i++)
            {
                frequency.Add(0);
                smoothHist.Add(0);
            }
        }

        /// <summary>
        /// Add a data value
        /// </summary>
        /// <param name="x">data value to add</param>
        /// <param name="weight">weight to apply to data</param>
        public void Increment(int x, int weight)
        {
            //higher weight => add more 
            for (int i = 0; i < weight; i++)
            {
                frequency.Add(x);
            }
        }

        /// <summary>
        /// Clear data values in histogram
        /// </summary>
        public void Clear()
        {
            frequency.Clear();
        }

        /// <summary>
        /// Smooth and invert the histogram (run this before calling getCost)
        /// </summary>
        /// <param name="bandwidth">bandwidth of gaussian</param>
        public void SmoothAndInvertHistogram(double bandwidth = 1)
        {
            smoothHist.Clear();
            for (int i = 0; i < range; i++)
            {
                double value = KernelDensity.EstimateGaussian(i, bandwidth, frequency);
                if (Double.IsNaN(value))
                {
                    smoothHist.Add(0);
                } else
                {
                    smoothHist.Add(value);
                }
            }
            
            //invert: more frequent -> lower cost
            for (int i = 0; i < smoothHist.Count; i++)
            {
                smoothHist[i] = 1 - smoothHist[i];
            }
        }

        /// <summary>
        /// Return the cost
        /// </summary>
        /// <param name="x">data value to get cost</param>
        /// <returns></returns>
        public double GetCost(int x)
        {
            return smoothHist[x];
        }
    }
}
