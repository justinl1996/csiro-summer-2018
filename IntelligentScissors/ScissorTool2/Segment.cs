﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ScissorTool
{
    /// <summary>
    /// SegmentContainer class: Responsible for holding segments
    /// </summary>
    public class SegmentContainer
    {
        private List<Segment> segments;

        public SegmentContainer()
        {
            segments = new List<Segment>();
        }

        /// <summary>
        /// Add a segment
        /// </summary>
        /// <param name="segment">segment to add</param>
        public void Add(Segment segment)
        {
            segments.Add(segment);
        }

        /// <summary>
        /// Remove last added segment
        /// </summary>
        /// <returns>the last added segment</returns>
        public Segment RemoveLast()
        {
            if (segments.Count == 0)
            {
                return null;
            }
            Segment segment = segments[segments.Count - 1];
            segments.RemoveAt(segments.Count - 1);
            return segment;
        }

        /// <summary>
        /// Get segments
        /// </summary>
        /// <returns>iterator over segments</returns>
        public IEnumerable<Segment> GetSegents()
        {
            foreach(Segment segment in segments)
            {
                yield return segment;
            } 
        }

        /// <summary>
        /// Get no. of segments
        /// </summary>
        /// <returns>no. of segments</returns>
        public int Count()
        {
            return segments.Count;
        }
    }

    /// <summary>
    /// Segment class: represents an outlined particle
    /// </summary>
    public class Segment
    {
        //list of path sections
        private LinkedList<List<Vector2>> path;
            
        public Segment()
        {
            path = new LinkedList<List<Vector2>>();
        }

        /// <summary>
        /// Check if path is empty
        /// </summary>
        /// <returns>True if there are any paths, otherwise false</returns>
        public bool IsEmpty()
        {
            return path.Count == 0;
        }

        /// <summary>
        /// Add a section to paths
        /// </summary>
        /// <param name="section">section to add</param>
        public void Add(List<Vector2> section)
        {
            path.AddLast(section);
        }

        /// <summary>
        /// Retrive the seed points of the complete path
        /// </summary>
        /// <returns>seed points</returns>
        public List<Vector2> GetSeedPoints()
        {
            List<Vector2> seedpoints = new List<Vector2>();
            foreach (List<Vector2> section in path)
            {
                if (section.Count != 0)
                {
                    seedpoints.Add(section.First());
                }
            }
            if (path.Count != 0)
            {
                seedpoints.Add(path.First().Last());
            }

            return seedpoints;
        }

        /// <summary>
        /// Remove the last path added
        /// </summary>
        /// <returns>true if this is possible (no. of paths > 0), otherwise false</returns>        
        public bool Undo()
        {
            if (path.Count != 0)
            {
                path.RemoveLast();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get the sections making up the path
        /// </summary>
        /// <returns></returns>
        public IEnumerable<List<Vector2>> GetPaths()
        {
            foreach (List<Vector2> p in path)
            {
                yield return p;
            }
        }

        /// <summary>
        /// Get the last added points from the lasted added path
        /// </summary>
        /// <param name="count">no. of positions to retrieve</param>
        /// <returns>list of positions previous visited (most recent first)</returns>
        public List<Vector2> GetLastVisited(int count)
        {
            List<Vector2> result = new List<Vector2>();
            if (path.Count == 0)
            {
                return result;
            }

            int amount = Math.Min(count, path.Last().Count);
            List<Vector2> section = path.Last();
            for (int i = 0; i < amount; i++)
            {
                result.Add(section[i]);
            }
            return result;
        }

        /// <summary>
        /// Get very last position added
        /// </summary>
        /// <returns>last position, otherwise (-1, -1) if no such position exists</returns>
        public Vector2 GetLastPoint()
        {
            if (path.Count == 0)
            {
                return new Vector2(-1);
            }
            return path.Last().First();
        }

        /// <summary>
        /// Get very first position added
        /// </summary>
        /// <returns>last first, otherwise (-1, -1) if no such position exists</returns>
        public Vector2 GetFirstPoint()
        {
            if (path.Count == 0)
            {
                return new Vector2(-1);
            }
            return path.First().Last();
        }

        /// <summary>
        /// Check if the the segment is closed. i.e. first & last point are same
        /// </summary>
        /// <returns></returns>
        public bool isClosed()
        {
            return GetFirstPoint() == GetLastPoint();
        }

        /*
        public bool isConnected()
        {
            /*Vector2 prev = path.First.Value;
            foreach (Vector2 vec in path)
            {
                if (Math.Abs(vec.X - prev.X) > 1 || Math.Abs(vec.Y - prev.Y) > 1)
                {
                    return false;
                }
                prev = vec;
            }
            return true;
        }
        */
    }
}
