﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ScissorTool;

namespace ScissorToolTest
{
    [TestClass]
    public class CircularListTest
    {
       
        private CircularList<int> circularList;

        [TestInitialize]
        public void TestInitialize()
        {
            circularList = new CircularList<int>(3); 
        }

        [TestMethod]
        public void TestWrap()
        {
            circularList.Value = 1;
            circularList.Next();
            circularList.Value = 2;
            circularList.Next();

            circularList.Value = 3;
            circularList.Next();
            circularList.Value = 4;
            circularList.Next();

            circularList.Value = 9;
            circularList.Next();
            circularList.Value = 10;
            circularList.Next();

            Assert.AreEqual(3, circularList.Count);
            List<int> list = new List<int>(circularList);
            Assert.AreEqual(10, list[0]);
            Assert.AreEqual(9, list[1]);
            Assert.AreEqual(4, list[2]);

            /*foreach(int cl in circularList)
            {
                Console.WriteLine(cl);
            }*/

        }
    }
}
