﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScissorTool;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ScissorToolTest
{
    [TestClass]
    public class GraphTest
    {
        private ScissorTool.Cost cost;

        [TestInitialize]
        public void TestInitialize()
        {
            //cost = new ScissorTool.Cost();
            //cost.LoadImage("C:\\Users\\luo029\\Pictures\\lasso\\small1.png");
            //cost.img.Size();
        }

        [TestMethod]
        public void TestSearch()
        {
            //PixelGraph pixelGraph = new PixelGraph(cost, cost.img.Rows, cost.img.Cols);
            //pixelGraph.Search(new Vector2(0, 0));
        }

        [TestMethod]
        public void TestVector()
        {
            Vector2 vec1 = new Vector2(1, 1);
            Vector2 vec2 = vec1;
            Assert.AreEqual(vec1, vec2);
            vec2.X = 2;
            vec2.Y = 2;

            Assert.AreNotEqual(vec1, vec2);
            Assert.AreEqual(vec1.X, 1);
            Assert.AreEqual(vec1.Y, 1);

        }
    }
}
