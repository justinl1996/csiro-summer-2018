﻿using MathNet.Numerics.Statistics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
//using MathNet.Numerics.Statistics; 
using System.Collections.Generic;
using System.Text;

namespace ScissorToolTest
{
    [TestClass]
    public class KDensityTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
        }

        [TestMethod]
        public void TestSmoothHistogram()
        {
            IList<double> list = new List<double>();
            list.Add(1.0f);
            list.Add(2.0f);
            list.Add(3.0f);

            list.Add(8.0f);
            list.Add(8.0f);

            for (int i = -10; i < 10; i++)
            {
                double x = KernelDensity.EstimateGaussian(i, 1, list);
                Console.WriteLine(x);
            }
        }

        [TestMethod]
        public void TestHistogram()
        {
            ScissorTool.Histogram hist = new ScissorTool.Histogram(10);
            hist.Increment(2);
            hist.Increment(2);
            hist.Increment(3);
            hist.Increment(2);

            hist.SmoothAndInvertHistogram(1);
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(hist.GetCost(i));
            }
        }

        [TestMethod]
        public void TestHistogramMax()
        {
            ScissorTool.Histogram hist = new ScissorTool.Histogram(10);
            for (int i = 0; i < 100000; i++)
            {
                hist.Increment(5);
            }
            /*List<double> result = hist.SmoothAndInvertHistogram(1);
            foreach (double e in result)
            {
                Console.WriteLine(e);
            }*/

        }

    }
}
