﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;


namespace ScissorToolTest
{
    public class PQNode : IComparable
    {
        public int val;
        public PQNode(int v)
        {
            val = v;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            return val - ((PQNode)obj).val;
        }
    }


    [TestClass]
    public class PriorityQueueTest
    {
        private VCSKicksCollection.PriorityQueue<PQNode> pq;

        [TestInitialize]
        public void TestInitialize()
        {
            pq = new VCSKicksCollection.PriorityQueue<PQNode>();
        }

        [TestMethod]
        public void TestEnqueue()
        {
            pq.Enqueue(new PQNode(3));
            pq.Enqueue(new PQNode(1));
            pq.Enqueue(new PQNode(5));
            pq.Enqueue(new PQNode(9));

            Assert.AreEqual(1, pq.Dequeue().val);
            Assert.AreEqual(3, pq.Dequeue().val);
            Assert.AreEqual(5, pq.Dequeue().val);
            Assert.AreEqual(9, pq.Dequeue().val);

        }

    }
}
