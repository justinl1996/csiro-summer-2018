﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScissorTool;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ScissorToolTest
{
    [TestClass]
    public class SegmentTest
    {
        private Segment segment;
        [TestInitialize]
        public void TestInitialize()
        {
            segment = new Segment();
        }

        [TestMethod]
        public void TestAddPath()
        {
            LinkedList<Vector2> list= new LinkedList<Vector2>();
            list.AddLast(new Vector2(1, 2));
            list.AddLast(new Vector2(1, 4));
            list.AddLast(new Vector2(3, 1));

            segment.Add(list);
            //segment.path.RemoveLast();

            Assert.AreEqual(3, segment.Path.Count);
            LinkedList<Vector2> list2 = new LinkedList<Vector2>();
            list2.AddLast(new Vector2(3, 3));

            segment.Add(list2);
            list2.RemoveLast();
            Assert.AreEqual(4, segment.Path.Count);
        }

        [TestMethod]
        public void TestConnected()
        {
            LinkedList<Vector2> list = new LinkedList<Vector2>();
            list.AddLast(new Vector2(1, 2));

            segment.Add(list);
        }

        [TestMethod]
        public void TestGetLast()
        {
            segment.Add(new Vector2(1, 0));
            segment.Add(new Vector2(3, 2));
            
            //segment.Add(new Vector2(4, 5));
            //segment.Add(new Vector2(2, 2));

            List<Vector2> result = segment.GetLastVisited(4);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(new Vector2(3, 2), result[0]);
            Assert.AreEqual(new Vector2(1, 0), result[1]);
            

            result = segment.GetLastVisited(0);
            Assert.AreEqual(0, result.Count);
            
        }
    }
}

