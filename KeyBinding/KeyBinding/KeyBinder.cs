﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;

namespace KeyBinding
{
    public struct KeyMap
    {
        public Keys currentKey;
        public Keys defaultKey;
        public string id;
        public string description;

        public KeyMap(Keys key, string id, string description)
        {
            this.defaultKey = key;
            this.currentKey = key;
            this.id = id;
            this.description = description;
        }
    }

    [DataContract]
    public class KeyBinder
    {
        //private Dictionary<string, Dictionary<Keys, KeyMap>> defaultKeyMapTable;
        [DataMember]
        private Dictionary<string, Dictionary<string, Keys>> keyId;

        [DataMember]
        private Dictionary<string, Dictionary<Keys, KeyMap>> keyMapTable;

        #region Singleton implementation

        public static KeyBinder Current { get { return LazyInitializer.Instance; } }

        private static class LazyInitializer
        {
            static LazyInitializer() { }
            public static readonly KeyBinder Instance = new KeyBinder();
        }

        #endregion

        public KeyBinder()
        {
            Reset();
        }

        /// <summary>
        /// Register a key to be added to lookup
        /// </summary>
        /// <param name="key">key to add</param>
        /// <param name="context">context of where the key is used</param>
        /// <param name="id">some unique id</param>
        /// <param name="description">some description of the key's action</param>
        public void Register(Keys key, string context, string id, string description="")
        {
            //new context so start new dictionaries
            if (!keyMapTable.ContainsKey(context))
            {
                keyMapTable[context] = new Dictionary<Keys, KeyMap>();
                //defaultKeyMapTable[context] = new Dictionary<Keys, KeyMap>();
                keyId[context] = new Dictionary<string, Keys>();
            }

            keyMapTable[context][key] = new KeyMap(key, id, description);
            //defaultKeyMapTable[context][key] = new KeyMap(key, id, description);
            keyId[context][id] = key;
        }

        private void checkContextExists(string context)
        {
            if (!keyMapTable.ContainsKey(context))
            {
                throw new KeyNotFoundException(context + " context not found");
            }
        }

        private void checkIdExists(string context, string id)
        {
            if (!keyId[context].ContainsKey(id))
            {
                throw new KeyNotFoundException(id + " id has not been previously registered");
            }
        }

        private void checkKeyExists(string context, Keys key)
        {
            if (!keyMapTable[context].ContainsKey(key))
            {
                throw new KeyNotFoundException("Key " + GetString(key) + " has not " +
                    "been previously registered");
            }
        }

        /// <summary>
        /// Updates a key with a new binding
        /// </summary>
        /// <param name="key">new key to bind</param>
        /// <param name="context">context of where the key is used</param>
        /// <param name="id">id of the key to be changed</param>
        public void Update(Keys key, string context, string id)
        {
            checkContextExists(context);
            checkIdExists(context, id);

            Keys oldKey = keyId[context][id];
            Update(oldKey, key, context);
        }

        /// <summary>
        /// Updates a key with a new binding 
        /// </summary>
        /// <param name="oldKey">key to change</param>
        /// <param name="newKey">new key to bind</param>
        /// <param name="context">context of where the key is used</param>
        public void Update(Keys oldKey, Keys newKey, string context)
        {
            checkContextExists(context);
            checkKeyExists(context, oldKey);
            if (keyMapTable[context].ContainsKey(newKey))
            {
                throw new ArgumentException("Key " + GetString(newKey) + " has been previously registered");
            }
            checkContextExists(context);

            KeyMap keyMap = keyMapTable[context][oldKey];
            string id = keyMap.id;

            //update with new key
            keyMap.currentKey = newKey;
            keyMapTable[context][newKey] = keyMap;

            keyId[context][id] = newKey;
            keyMapTable[context].Remove(oldKey);
        }

        /// <summary>
        /// Restore the default key binding for a given key
        /// </summary>
        /// <param name="id">id of key to restore</param>
        /// <param name="context">context of where the key is used</param>
        public void RestoreDefault(string id, string context)
        {
            checkContextExists(context);
            checkIdExists(context, id);

            RestoreDefault(keyId[context][id], context);
        }

        /// <summary>
        /// Restore the default key binding for a given key
        /// </summary>
        /// <param name="key">key to restore</param>
        /// <param name="context">context of where the key is used</param>
        public void RestoreDefault(Keys key, string context)
        {
            checkContextExists(context);
            checkKeyExists(context, key);

            KeyMap keyMap = keyMapTable[context][key];
            keyMap.currentKey = keyMap.defaultKey;
            keyMapTable[context].Remove(key);

            
            if (keyMapTable[context].ContainsKey(keyMap.currentKey))
            {
                RestoreDefault(keyMap.currentKey, context);
            }

            keyMapTable[context][keyMap.currentKey] = keyMap;
            keyId[context][keyMap.id] = keyMap.currentKey; 
        }

        /// <summary>
        /// Restore the default keys in the context
        /// </summary>
        /// <param name="context">context to restore keys</param>
        public void RestoreDefaults(string context)
        {
            checkContextExists(context);
            //take a copy
            foreach (Keys key in keyMapTable[context].Keys.ToList())
            {
                RestoreDefault(key, context);
            }
        }

        /// <summary>
        /// Reset all keys in every context to their defaults
        /// </summary>
        public void RestoreDefaults()
        {
            foreach (string context in keyMapTable.Keys.ToList())
            {
                RestoreDefaults(context);
            }
        }

        /// <summary>
        /// Get all the keys for a given context
        /// </summary>
        /// <param name="context">context of where the key is used</param>
        /// <returns>keymaps in the context</returns>
        public List<KeyMap> GetAll(string context)
        {
            checkContextExists(context);
            List<KeyMap> result = new List<KeyMap>(keyMapTable[context].Values);

            //for neatness, probably not necessary
            //result.Sort((x, y) => (x.currentKey).CompareTo(y.currentKey));

            return result;
        }

        /// <summary>
        /// Get all registered contexts
        /// </summary>
        /// <returns>strings of the contexts</returns>
        public List<string> GetContexts()
        {
            return new List<string>(keyMapTable.Keys);
        }

        /// <summary>
        /// get the keymap for a given key from some context
        /// </summary>
        /// <param name="key">key to get keymap</param>
        /// <param name="context">context of where the key is used</param>
        /// <returns>the keymap for the given key in the context</returns>
        public KeyMap Get(Keys key, string context)
        {
            checkContextExists(context);

            if (!keyMapTable[context].ContainsKey(key)) 
            {
                return new KeyMap(Keys.None, "", "");
            }
            return keyMapTable[context][key];
        }

        /// <summary>
        /// get the keymap for a given key from a list of contexts
        /// </summary>
        /// <param name="key">key to get keymap</param>
        /// <param name="contexts">contexts to search for key binding</param>
        /// <returns>the found keymap in the contexts</returns>
        /*public KeyMap Get(Keys key, string [] contexts)
        {
            foreach (string context in contexts)
            {
                KeyMap keyMap = Get(key, context);
                if (keyMap.currentKey != Keys.None)
                {
                    return keyMap;
                }
            }
            return new KeyMap(Keys.None, "", "");
        }*/

        /// <summary>
        /// Get the keymap for a given id
        /// </summary>
        /// <param name="id">id to get keymap of</param>
        /// <param name="context">context of where the key is used</param>
        /// <returns>the keymap for the given key in the context</returns>
        public KeyMap Get(string id, string context)
        {
            checkContextExists(context);
            checkIdExists(context, id);
            Keys key = keyId[context][id];
            return Get(key, context);
        }

        /// <summary>
        /// Get the keymap for a given id
        /// </summary>
        /// <param name="id">id to get keymap of</param>
        /// <param name="contexts">contexts to search for key binding</param>
        /// <returns>the keymap for the given key in the context</returns>
        /*public KeyMap Get(string id, string [] contexts)
        {
            foreach(string context in contexts)
            {
                KeyMap keyMap = Get(id, context);
                if (keyMap.currentKey != Keys.None)
                {
                    return keyMap;
                }
            }
            return new KeyMap(Keys.None, "", "");
        }*/


        /// <summary>
        /// Get a nice string representation of key that have a modifier 
        /// (swaps the modifier so it comes before the key)
        /// </summary>
        /// <param name="key">key to get string of</param>
        /// <returns>a 'nicer' string representation</returns>
        public static string GetString(Keys key)
        {
            string result = key.ToString();
            if (result.Contains(","))
            {
                string [] temp = result.Split(',');
                result = temp[1].Substring(1) + "+" + temp[0];
            }
            return result;
        }

        /// <summary>
        /// Save the keybindings to a file
        /// </summary>
        /// <param name="filename">filename to save to</param>
        public void Save(string filename)
        {
            var stream = File.Create(filename);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(KeyBinder));
            ser.WriteObject(stream, this);
            stream.Close();
        }

        /// <summary>
        /// Load the keybindings to a file
        /// </summary>
        /// <param name="filename">filename to load from</param>
        public void Load(string filename)
        {
            var stream = File.OpenRead(filename);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(KeyBinder));
            KeyBinder loadedKeyBinder = ((KeyBinder) ser.ReadObject(stream));
            this.keyId = loadedKeyBinder.keyId;
            this.keyMapTable = loadedKeyBinder.keyMapTable;
            stream.Close();
        }

        /// <summary>
        /// start a clean slate
        /// </summary>
        public void Reset()
        {
            keyMapTable = new Dictionary<string, Dictionary<Keys, KeyMap>>();
            keyId = new Dictionary<string, Dictionary<string, Keys>>();
        }
    }
}
