using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KeyBinding;


namespace KeyBindingGen
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyBinder.Current.Register(Keys.A, "DrawPanel", "DrawCircle", "Draw a Circle");
            KeyBinder.Current.Register(Keys.B, "DrawPanel", "DrawSquare", "Draw a Square");
            KeyBinder.Current.Register(Keys.C, "DrawPanel", "DrawTriangle", "Draw a Triangle");
            KeyBinder.Current.Register(Keys.D, "DrawPanel", "DrawRectangle", "Draw a Rectangle");
            KeyBinder.Current.Register(Keys.E, "DrawPanel", "DrawOval", "Draw a Oval");
            KeyBinder.Current.Register(Keys.F, "DrawPanel", "DrawLine", "Draw a Line");

            KeyBinder.Current.Register(Keys.G, "DrawPanel", "DrawStar", "Draw a Star");
            KeyBinder.Current.Register(Keys.H, "DrawPanel", "Pen", "Pen tool");
            KeyBinder.Current.Register(Keys.I, "DrawPanel", "Bucket", "Bucket");
            KeyBinder.Current.Register(Keys.J, "DrawPanel", "Brush", "Brush");
            KeyBinder.Current.Register(Keys.K, "DrawPanel", "Highlight", "Highlighter");
            KeyBinder.Current.Register(Keys.L, "DrawPanel", "Blur", "Gaussian Blue");

            KeyBinder.Current.Register(Keys.Control | Keys.C, "EditPanel", "Copy", "Copy");
            KeyBinder.Current.Register(Keys.Control | Keys.V, "EditPanel", "Paste", "Paste");
            KeyBinder.Current.Register(Keys.Control | Keys.Z, "EditPanel", "Undo", "Undo");
            KeyBinder.Current.Register(Keys.Control | Keys.Y, "EditPanel", "Redo", "Redo");

            KeyBinder.Current.Register(Keys.Alt | Keys.A, "EditPanel", "Play", "Play");
            KeyBinder.Current.Register(Keys.Alt | Keys.S, "EditPanel", "Stop", "Stop");

            KeyBinder.Current.Save("C:\\Users\\luo029\\Documents\\keybindings.json");
        }
    }
}
