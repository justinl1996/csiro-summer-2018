using KeyBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace KeyBindingGuiTest
{
    public partial class Form1 : Form
    {
        private string keyBindingFile = "..\\..\\..\\keybindings.json";
        public Form1()
        {
            InitializeComponent();
            KeyBinding.KeyBinder.Current.Load(keyBindingFile);
            //this.KeyDown += new KeyEventHandler(this.keyDown);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form keyBinding = new KeyBindingWindow.KeyBindingForm(keyBindingFile);
            keyBinding.Show();
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            //no way to focus on pictureBox, so form captures all keystrokes

            Keys key = e.KeyCode;
            Keys modifier = ModifierKeys;

            drawBox.Image = null;
            editBox.Image = null;
            drawBox.Refresh();
            editBox.Refresh();

            Console.WriteLine(key | modifier);
            KeyMap keyMap = KeyBinder.Current.Get(key | modifier,  "Draw Panel");
            switch (keyMap.id)
            {
                case "DrawSquare":
                    //label1.Text = "Draw Square";
                    drawText(0, 0, "Draw Square", drawBox.Handle);
                    break;
                case "DrawCircle":
                    //label1.Text = "Draw Circle";
                    drawText(0, 0, "Draw Circle", drawBox.Handle);
                    break;
                case "DrawTriangle":
                    //label1.Text = "Draw Triangle";
                    drawText(0, 0, "Draw Triangle", drawBox.Handle);
                    break;
                default:
                    drawText(0, 0, "Unknown", drawBox.Handle);
                    break;
            }

            keyMap = KeyBinder.Current.Get(key | modifier, "Edit Panel");
            switch (keyMap.id)
            { 
                case "Undo":
                    drawText(0, 0, "Undo", editBox.Handle);
                    break;
                case "Redo":
                    drawText(0, 0, "Redo", editBox.Handle);
                    break;
                case "Play":
                    drawText(0, 0, "Play", editBox.Handle);
                    break;
                case "Stop":
                    drawText(0, 0, "Stop", editBox.Handle);
                    break;
                default:
                    drawText(0, 0, "Unknown", editBox.Handle);
                    break;
            }

        }

        public void drawText(int x, int y, string text, IntPtr handle)
        {
            
            using (Graphics g = Graphics.FromHwnd(handle))
            {
                using (Font myFont = new Font("Calibri", 14))
                {
                    g.DrawString(text, myFont, Brushes.Black, new PointF(x, y));
                }
            }
        }

        private void loadKeysBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = openFileDialog.FileName;
                    keyBindingFile = filePath;
                    KeyBinding.KeyBinder.Current.Load(keyBindingFile);
                    Console.WriteLine("HERE");
                }
            }
        }
    }
}
