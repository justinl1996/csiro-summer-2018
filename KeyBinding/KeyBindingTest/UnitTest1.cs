﻿using System;
using KeyBinding;
using System.Windows.Forms;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace KeyBindingTest
{
    [TestClass]
    public class KeyBinderUnitTest
    {

        [TestInitialize]
        public void Init()
        {
            KeyBinder.Current.Reset();
            KeyBinder.Current.Register(Keys.A, "PictureBox1", "a", "apple");
            KeyBinder.Current.Register(Keys.B, "PictureBox1", "b", "banana");
            KeyBinder.Current.Register(Keys.C, "PictureBox1", "c", "cranberry");
        }

        [TestMethod]
        public void TestGetMethod()
        {
            KeyMap keymap = KeyBinder.Current.Get(Keys.A, "PictureBox1");
            Assert.AreEqual("a", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.C, "PictureBox1");
            Assert.AreEqual("c", keymap.id);

            KeyMap keymap2 = KeyBinder.Current.Get("c", "PictureBox1");
            Assert.ReferenceEquals(keymap, keymap2);
        }
       
        [TestMethod]
        public void TestGetMethod2()
        {
            KeyBinder.Current.Register(Keys.A, "EditBox1", "d", "darth");
            KeyBinder.Current.Register(Keys.B, "DrawBox1", "e", "eggplant");
            KeyBinder.Current.Register(Keys.C, "SettingsBox1", "f", "flute");

            KeyMap keymap = KeyBinder.Current.Get(Keys.A, "PictureBox1");
            Assert.AreEqual("a", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.A, "EditBox1");
            Assert.AreEqual("d", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.B, "PictureBox1");
            Assert.AreEqual("b", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.B, "DrawBox1");
            Assert.AreEqual("e", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.C, "PictureBox1");
            Assert.AreEqual("c", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.C, "SettingsBox1");
            Assert.AreEqual("f", keymap.id);
        }

        [TestMethod]
        public void TestWrongContext()
        {
            Assert.ThrowsException<KeyNotFoundException>(() => KeyBinder.Current.Get(Keys.A, "ASDAS"));
            Assert.ThrowsException<KeyNotFoundException>(() => KeyBinder.Current.Update(Keys.A, "ASDAS" , "a"));
        }

        [TestMethod]
        public void TestUpdateMethod()
        {
            KeyBinder.Current.Update(Keys.Z, "PictureBox1", "a");
            KeyMap keymap = KeyBinder.Current.Get(Keys.Z, "PictureBox1");
            Assert.AreEqual("a", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.A, "PictureBox1");
            Assert.AreEqual("", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.B, "PictureBox1");
            Assert.AreEqual("b", keymap.id);

            Assert.ThrowsException<KeyNotFoundException>(() => 
                    KeyBinder.Current.Update(Keys.A, "ASDD", "1"));
            Assert.ThrowsException<KeyNotFoundException>(() => 
                    KeyBinder.Current.Update(Keys.A, "PictureBox1", "nothing"));
        }

        [TestMethod]
        public void TestUpdateMethod2()
        {
            KeyBinder.Current.Update(Keys.A, Keys.S, "PictureBox1");
            KeyMap keymap = KeyBinder.Current.Get(Keys.S, "PictureBox1");
            Assert.AreEqual("a", keymap.id);

            KeyBinder.Current.Update(Keys.B, Keys.R, "PictureBox1");
            keymap = KeyBinder.Current.Get(Keys.R, "PictureBox1");
            Assert.AreEqual("b", keymap.id);
        }


        [TestMethod]
        public void TestRestoreDefaultMethod()
        {
            //KeyBinder.Current.Update(Keys.Z, "PictureBox1", "a");
            //KeyBinder.Current.Update(Keys.Y, "PictureBox1", "b");
            //KeyBinder.Current.Update(Keys.X, "PictureBox1", "c");
            
            KeyBinder.Current.RestoreDefaults();

            KeyMap keymap = KeyBinder.Current.Get(Keys.A, "PictureBox1");
            Assert.AreEqual("a", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.B, "PictureBox1");
            Assert.AreEqual("b", keymap.id);

            keymap = KeyBinder.Current.Get(Keys.C, "PictureBox1");
            Assert.AreEqual("c", keymap.id);
        }
        
        [TestMethod]
        public void TestResetMethod()
        {
            KeyBinder.Current.Register(Keys.U, "DELETE", "U");
            KeyBinder.Current.Register(Keys.Z, "DELETE2", "Z");
            KeyBinder.Current.Reset();

            Assert.AreEqual(0, KeyBinder.Current.GetContexts().Count);
        }
    }
}
