namespace KeyBindingWindow
{
    partial class KeyBindingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.infoLabel = new System.Windows.Forms.Label();
            this.keyListView = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.contextComboBox = new System.Windows.Forms.ComboBox();
            this.defaultBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.infoLabel);
            this.panel1.Controls.Add(this.keyListView);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.contextComboBox);
            this.panel1.Controls.Add(this.defaultBtn);
            this.panel1.Controls.Add(this.okBtn);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 321);
            this.panel1.TabIndex = 0;
            // 
            // infoLabel
            // 
            this.infoLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.infoLabel.Location = new System.Drawing.Point(63, 253);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.infoLabel.Size = new System.Drawing.Size(156, 33);
            this.infoLabel.TabIndex = 14;
            this.infoLabel.Text = "Double click to customise";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // keyListView
            // 
            this.keyListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.keyListView.FullRowSelect = true;
            this.keyListView.Location = new System.Drawing.Point(12, 72);
            this.keyListView.Name = "keyListView";
            this.keyListView.Size = new System.Drawing.Size(207, 178);
            this.keyListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.keyListView.TabIndex = 13;
            this.keyListView.UseCompatibleStateImageBehavior = false;
            this.keyListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.keyListView_ColumnClick);
            this.keyListView.ItemActivate += new System.EventHandler(this.keyListView_ItemActivate);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Context";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Key Bindings";
            // 
            // contextComboBox
            // 
            this.contextComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.contextComboBox.FormattingEnabled = true;
            this.contextComboBox.Location = new System.Drawing.Point(12, 25);
            this.contextComboBox.Name = "contextComboBox";
            this.contextComboBox.Size = new System.Drawing.Size(207, 21);
            this.contextComboBox.TabIndex = 10;
            this.contextComboBox.SelectedIndexChanged += new System.EventHandler(this.contextComboBox_SelectedIndexChanged);
            // 
            // defaultBtn
            // 
            this.defaultBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.defaultBtn.Location = new System.Drawing.Point(63, 289);
            this.defaultBtn.Name = "defaultBtn";
            this.defaultBtn.Size = new System.Drawing.Size(75, 23);
            this.defaultBtn.TabIndex = 9;
            this.defaultBtn.Text = "Defaults";
            this.defaultBtn.UseVisualStyleBackColor = true;
            this.defaultBtn.Click += new System.EventHandler(this.defaultBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(144, 289);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 8;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // KeyBindingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 321);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "KeyBindingForm";
            this.Text = "Key Shortcuts";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox contextComboBox;
        private System.Windows.Forms.Button defaultBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.ListView keyListView;
        private System.Windows.Forms.Label infoLabel;
    }
}

