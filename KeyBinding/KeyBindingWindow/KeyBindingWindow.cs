using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KeyBinding;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace KeyBindingWindow
{
    public partial class KeyBindingForm : Form
    {
        private KeyEventHandler keyEventHandler;

        //to indicate if we are in the state of awaiting key press
        private bool listenState = false;

        //used for mapping items in the listview to the actual keymap
        private Dictionary<string, KeyMap> keyNames;

        //selected list item and key that is to be changed
        private ListViewItem selectedItem;
        private Keys selectedKey;

        //current selected context
        private string selectedContext;

        //filename of key binding
        private string keyBindingFile;

        public KeyBindingForm(string filename)
        {
            InitializeComponent();
            this.keyListView.ListViewItemSorter = new ListViewItemComparer(1);
            /*KeyBinder.Current.Register(Keys.A, "Draw Panel", "DrawSquare", "Draw Square");
            KeyBinder.Current.Register(Keys.B, "Draw Panel", "DrawCircle", "Draw Circle");
            KeyBinder.Current.Register(Keys.C, "Draw Panel", "DrawTriangle", "Draw Triangle");
            KeyBinder.Current.Register(Keys.Control | Keys.Z, "Edit Panel", "Undo", "Undo");
            KeyBinder.Current.Register(Keys.Control | Keys.Y, "Edit Panel", "Redo", "Redo");
            KeyBinder.Current.Register(Keys.Shift | Keys.A, "Edit Panel", "Play", "Play");
            KeyBinder.Current.Register(Keys.Alt | Keys.A, "Edit Panel", "Stop", "Stop");*/

            keyEventHandler = new KeyEventHandler(this.keyListView_KeyDown);
            keyNames = new Dictionary<string, KeyMap>();

            keyBindingFile = filename;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (string context in KeyBinder.Current.GetContexts())
            {
                contextComboBox.Items.Add(context);
            }            
            
            contextComboBox.SelectedIndex = 0;
            selectedContext = contextComboBox.Items[0].ToString();
        }

        /// <summary>
        /// Stop listening for key down event
        /// </summary>
        private void resetKeyHandler()
        {
            infoLabel.Text = "Double click to customise";
            keyListView.KeyDown -= keyEventHandler;
            listenState = false;
        }

        //prevent resizing column width
        private void keyListView_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = keyListView.Columns[e.ColumnIndex].Width;
        }

        /// <summary>
        /// Key listener handler
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>
        private void keyListView_KeyDown(object sender, KeyEventArgs e)
        {
            Keys key = e.KeyCode;
            Keys modifier = ModifierKeys;
            if (key == Keys.Escape)
            {
                resetKeyHandler();
                keyListView_Redraw(KeyBinder.Current.GetAll(selectedContext));
                return;
            }
            if (Control.ModifierKeys != Keys.None && (key == Keys.ControlKey || key == Keys.Menu || key == Keys.ShiftKey))
            {
                // we only pressed a modifier, so wait till user press an actual key as well
                return;
            }
            if (key == Keys.Back)
            {
                //restore previously binded key
                KeyBinder.Current.RestoreDefault(selectedKey, selectedContext);
            } else
            {
                //attempt to bind the key (of course this can fail if the key has been previously binded)
                try
                {
                    KeyBinder.Current.Update(selectedKey, modifier | key, selectedContext);
                }
                catch (ArgumentException ae)
                {
                    KeyMap keyMap = KeyBinder.Current.Get(modifier | key, selectedContext);

                    ListViewItem item = keyListView.FindItemWithText(KeyBinder.GetString(keyMap.currentKey));
                    if (item != null)
                    {
                        item.BackColor = Color.Red;
                    }
                    MessageBox.Show(ae.Message, "Error Binding Key",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
                //selectedItem.Text = (modifier | key).ToString();
            }

            //redraw list with new key binding
            resetKeyHandler();
            keyListView_Redraw(KeyBinder.Current.GetAll(selectedContext));
        }

        /// <summary>
        /// When the user selects of key to bind
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>
        private void keyListView_ItemActivate(object sender, EventArgs e)
        {
            if (listenState) //check if waiting for key press
            {
                Console.WriteLine("RESET KEY");
                resetKeyHandler();
                keyListView_Redraw(KeyBinder.Current.GetAll(selectedContext));
                return;
            }
            if (keyListView.SelectedIndices.Count == 0) //should never be true, just to be safe
            {
                return;
            }
            Console.WriteLine("ITEM ACTIVATED");

            infoLabel.Text = "ESC to cancel\nBackspace for default";
            //keyListView.SelectedIndices;

            ListViewItem item = keyListView.SelectedItems[0];
            selectedItem = item;

            //grab key to bind
            selectedKey = keyNames[selectedItem.Text].currentKey;
      
            selectedItem.SubItems[0].Text = "...";

            //start waiting for key press      
            this.keyListView.KeyDown += keyEventHandler;
            listenState = true;
        }

        /// <summary>
        /// User changed context, so redraw list view
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>
        private void contextComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            resetKeyHandler();

            selectedContext = contextComboBox.Text;
            List<KeyMap> keyMaps = KeyBinder.Current.GetAll(selectedContext);

            keyListView_Redraw(keyMaps);
        }

        /// <summary>
        /// Redraw the list view with key bindings
        /// </summary>
        /// <param name="keyMaps">key bindings to display</param>
        private void keyListView_Redraw(List<KeyMap> keyMaps)
        {
            keyListView.View = View.Details;
            keyListView.TabIndex = 0;
            keyListView.Clear();

            keyListView.Columns.Add("Key", 1 * keyListView.Width / 3, HorizontalAlignment.Left);
            keyListView.Columns.Add("Action", 2 * keyListView.Width / 3, HorizontalAlignment.Left);

            keyNames.Clear();

            //create rows
            for (int i = 0; i < keyMaps.Count; i++)
            {
                KeyMap keyMap = keyMaps[i];
                            
                ListViewItem item = new ListViewItem(new string[] {
                    KeyBinding.KeyBinder.GetString(keyMap.currentKey),
                    keyMap.description});
                keyListView.Items.Insert(i, item);
                keyNames[item.Text] = keyMap;
            }
        }

        /// <summary>
        /// 'Default' Button is pressed, display warning and restore default key bindings if user accepts
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>
        private void defaultBtn_Click(object sender, EventArgs e)
        {
            resetKeyHandler();
            DialogResult result = MessageBox.Show("Are you sure you want to restore default key bindings?", 
                "Key bindings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes) 
            {
                KeyBinder.Current.RestoreDefaults(selectedContext);
                keyListView_Redraw(KeyBinder.Current.GetAll(selectedContext));
            }
        }

        /// <summary>
        /// 'OK' Button is pressed, so save bindings and exit
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            resetKeyHandler();
            KeyBinder.Current.Save(keyBindingFile);
            this.Close();
        }

        /// <summary>
        /// Sort columns when user clicks column
        /// </summary>
        /// <param name="sender">reference to control that raise event</param>
        /// <param name="e">containing event data</param>>
        private void keyListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            keyListView.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }
    }

    // Implements the manual sorting of items by columns.
    class ListViewItemComparer : IComparer
    {
        private int col;
        public ListViewItemComparer()
        {
            col = 0;
        }
        public ListViewItemComparer(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
        }
    }
}
