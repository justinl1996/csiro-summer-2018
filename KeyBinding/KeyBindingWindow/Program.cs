using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyBindingWindow
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string keyBindingFile = "C:\\Users\\luo029\\Documents\\keybindings.json";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Console.WriteLine("HERE");
            Application.Run(new KeyBindingForm(keyBindingFile));
        }
    }
}
