#include "pch.h"
#include <fstream>
#include <algorithm>

#include "Motion.h"
#include "Stat.h"
#include "Util.h"


/* Detect if there is motion (uses simple backgraound thresholding) */
bool TrainMotion::motion_detect(Mat &previous, Mat &current)
{
	//blur to remove noise
	GaussianBlur(previous, previous, Size(21, 21), 0);
	GaussianBlur(current, current, Size(21, 21), 0);

	Mat frame_diff;

	//take difference and threshold
	absdiff(current, previous, frame_diff);
	threshold(frame_diff, frame_diff, 25, 255, THRESH_BINARY);

	//enlarge substantial regions with great differences and remove
	dilate(frame_diff, frame_diff, Mat());

	std::vector<std::vector<Point> > contours;
	std::vector<Vec4i> hierarchy;

	//find contours in region, if found -> motion detected
	findContours(frame_diff, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	return contours.size() != 0;
}

/**Fixes barrel distortion, based of python implementation found here:
https://stackoverflow.com/questions/26602981/correct-barrel-distortion-in-opencv-manually-without-chessboard-image
*/
void TrainMotion::barrel_correct(const Mat &img, Mat &out)
{
	Mat cam = Mat::eye(3, 3, CV_32F);
	Mat coeff = Mat::zeros(4, 1, CV_32F);

	//magic number 
	coeff.at<float>(0, 0) = -2.2e-5f;

	//define center
	cam.at<float>(0, 2) = img.cols / 2.0f;
	cam.at<float>(1, 2) = img.rows / 2.0f;

	//define focal length
	cam.at<float>(0, 0) = 9.8f;
	cam.at<float>(1, 1) = 18.0f;

	cv::undistort(img, out, cam, coeff);

	//rotate the image so the tracks run horizontally
	Mat rot = getRotationMatrix2D(Point2f(img.cols / 2.0f, img.rows / 2.0f), 1.35f, 1.0f);
	warpAffine(out, out, rot, Size(img.cols, img.rows));
}

/**Given an approximate start and end position (xmin and xmax), identifies most likely 
position where the middle of train carriage gap is*/
int TrainMotion::get_train_median(Mat orig, int start, int end)
{
	//emipirically determined thresholds, seem to work very well
	int mid_thresh = 40;
	const int top_bot_thresh = 60;

	//on leftside -> so start from left edge
	if (start < 0.2f * orig.cols) {
		start = 0;
	}
	// on rightside -> so start from right edge
	else if (end > 0.8f * orig.cols) {
		end = orig.cols;
		mid_thresh = 90;
	}

	//grab regions to look for 'dark regions'
	Mat img_mid(orig, Rect(start, orig.rows / 2 - 40, end - start, 20));
	Mat img_top(orig, Rect(start, TRACK_TOP_Y + 20, end - start, 20));
	Mat img_bot(orig, Rect(start, TRACK_BOT_Y - 40, end - start, 20));

	Mat intensity_mid, intensity_top, intensity_bot;

	//Average pixel intensities columnwise
	reduce(img_mid, intensity_mid, 0, CV_REDUCE_AVG);
	reduce(img_top, intensity_top, 0, CV_REDUCE_AVG);
	reduce(img_bot, intensity_bot, 0, CV_REDUCE_AVG);

	//threshold accordingly
	threshold(intensity_mid, intensity_mid, mid_thresh, 1, THRESH_BINARY_INV);
	threshold(intensity_top, intensity_top, top_bot_thresh, 1, THRESH_BINARY_INV);
	threshold(intensity_bot, intensity_bot, top_bot_thresh, 1, THRESH_BINARY_INV);

	std::vector<int> values_top, values_bot, values_mid;

	//obtain positions which are below threshold (darkest regions)
	for (int i = 0; i < intensity_mid.cols; i++) {
		if (intensity_mid.at<uchar>(0, i) == 1) {
			values_mid.push_back(i);
		}
		if (intensity_top.at<uchar>(0, i) == 1) {
			values_top.push_back(i);
		}
		if (intensity_bot.at<uchar>(0, i) == 1) {
			values_bot.push_back(i);
		}
	}

	int mean;
	//not enough values so use the top and bottom regions
	if (values_mid.size() < 10) {
		//still not enough -> not likely to be a gap
		//should process the top and bottom regions here rather than above
		if (values_bot.size() < 10 || values_top.size() < 10) {
			return -1;
		}
		//enough vakyes use the mean of the two
		mean = int((Stat::get_mean<int>(values_bot) + Stat::get_mean<int>(values_top)) / 2) + start;
	}
	else {
		//enough values use the mean of the mid values
		mean = int(Stat::get_mean<int>(values_mid) + start);  //+ get_mean(values_bot))/2 + start;
	}
	return mean;
}

/**Get the estimated start + end (xmin and xmax) of carriage gap*/
std::pair<int, int> TrainMotion::get_train_gap(const Mat &top_track, const Mat &bot_track)
{
	const int thresh = 60;
	const float z_thresh = 1.5;
	const int std_thresh = 120;

	Mat top_intensity, bot_intensity;
	//Mat top_sobelx, bot_sobelx;
	cv::Sobel(top_track, top_intensity, CV_8U, 1, 0, 3);
	cv::Sobel(bot_track, bot_intensity, CV_8U, 1, 0, 3);

	//imshow("top track", top_intensity);
	//imshow("bot track", bot_intensity);
	//Mat bot_intensity;

	reduce(top_intensity, top_intensity, 0, CV_REDUCE_AVG);
	reduce(bot_intensity, bot_intensity, 0, CV_REDUCE_AVG);

	//threshold to obtain 'bright' regions
	threshold(top_intensity, top_intensity, thresh, 1, THRESH_BINARY);
	threshold(bot_intensity, bot_intensity, thresh, 1, THRESH_BINARY);

	int top_mean = 0, bot_mean = 0;
	int top_total = 0, bot_total = 0;

	//collect x-coordinates with the bright regions 
	std::vector<int> top_thresh, bot_thresh;
	for (int i = 0; i < top_intensity.cols; i++) {
		if (top_intensity.at<uchar>(0, i) == 1) {
			top_total++;
			top_mean += i;
			top_thresh.push_back(i);
		}
		if (bot_intensity.at<uchar>(0, i) == 1) {
			bot_total++;
			bot_mean += i;
			bot_thresh.push_back(i);
		}
	}

	//compute means -> center of 'grates'
	if (top_total && bot_total) {
		top_mean /= top_total;
		bot_mean /= bot_total;
	}
	else {
		//nothing found 
		return std::pair(-1, -1);
	}

	//compute std (used for discarding outliers)
	float top_std = Stat::get_std<int>(top_thresh, float(top_mean));
	float bot_std = Stat::get_std<int>(bot_thresh, float(bot_mean));

	//get min + max values which are not outliers
	int minTop = Stat::get_min_after_thresh(top_thresh, top_mean, top_std, z_thresh);
	int minBot = Stat::get_min_after_thresh(bot_thresh, bot_mean, bot_std, z_thresh);

	int maxTop = Stat::get_max_after_thresh(top_thresh, top_mean, top_std, z_thresh);
	int maxBot = Stat::get_max_after_thresh(bot_thresh, bot_mean, bot_std, z_thresh);

	int start, end;

	//find the region with the smallest std and use that
	if (top_std - bot_std > std_thresh) {
		start = minBot;
		end = maxBot;
	}
	else if (bot_std - top_std > std_thresh) {
		start = minTop;
		end = maxTop;
	}
	else {
		//otherwise choose the min and max one out of the two
		start = MIN(minTop, minBot);
		end = MAX(maxTop, maxBot);
	}

	//std too large, difficult to say where the gap is
	if (bot_std > 280 && top_std > 280) {
		return std::make_pair(-1, -1);
	}

	return std::make_pair(start, end);
}

/**Dump pixel intensities for debugging purposes*/
void TrainMotion::dump_intensity(std::string filename, Mat img1, Mat img2)
{
	std::ofstream ofs;
	ofs.open(filename + ".txt", std::ofstream::out);

	for (int i = 0; i < img1.cols; i++) {
		ofs << int(img1.at<uchar>(0, i)) << ", " << int(img2.at<uchar>(0, i)) << std::endl;
	}
	ofs.close();
}

/**Calculate speed of train based of past gap positions and speed in last frame*/
float TrainMotion::calc_speed(const std::vector<int> &gap_positions, int current_index,
	float prev_speed)
{
	int idx = current_index;
	int current_pos = gap_positions[idx % BUFFER_LEN];

	//didn't detect a gap, so use speed obtained in previous frame
	if (current_pos == -1) {
		return prev_speed;
	}

	//look back a frame (recall the positioned are placed in a circular array)
	idx--;
	if (idx == -1) {
		idx = BUFFER_LEN - 1;
	}

	//see if there was a gap detected
	if (gap_positions[idx % BUFFER_LEN] == -1) {
		return prev_speed;
	}
	float time = 1.0f / FPS;

	//actual speed calculation
	return ((current_pos - gap_positions[idx % BUFFER_LEN]) / (CAMERA_LENGTH / 100.0f)) / time;
}

/**Show the last added frame including the speed and estimated gap location
only works when the debug flag is set
*/
void TrainMotion::showFrame()
{
	if (!is_debug) {
		return;
	}
	imshow("full img", disp_frame);
	waitKey(0);
}

void TrainMotion::finish()
{
	video.release();
}

/**Add a new frame and perform speed calculation*/
float TrainMotion::addFrame(const std::string &filename) 
{
	Mat correct;
	Mat img = imread(filename, IMREAD_COLOR);

	barrel_correct(img, correct);
	cvtColor(correct, correct, CV_BGR2GRAY);

	//extract region containing train edge for motion detection (don't bother with anything else)
	Mat train_region(correct, Rect(0, TRACK_TOP_Y, correct.cols, TRACK_HEIGHT * 3));
	
	//not the first frame check for motion using frame difference
	if (!prev_frame.empty()) {
		train_region.copyTo(current_frame);
		is_moving = motion_detect(prev_frame, current_frame);
	}
	else {
		video = VideoWriter("outcpp.avi", CV_FOURCC('M', 'J', 'P', 'G'), 2,
			Size(img.cols, img.rows));
	}

	//store current frame for later motion detect 
	train_region.copyTo(prev_frame);

	//extract top + bottom train edge
	Mat top_rail_region(correct, Rect(int(correct.cols * 0.02f), TRACK_TOP_Y, int(correct.cols * 0.98f), TRACK_HEIGHT));
	Mat bot_rail_region(correct, Rect(int(correct.cols * 0.02f), TRACK_BOT_Y, int(correct.cols * 0.98f), TRACK_HEIGHT));

	//std::string newfilename = Util::remove_extension(filename);
	//dump_intensity(newfilename, top_intensity, bot_intensity);
	//std::cout << filename << std::endl;
	
	//endpoints -> xrange likely to be gap between carriages 
	std::pair<int, int> endpoints = get_train_gap(top_rail_region, bot_rail_region);
	//std::cout << endpoints.first << " : " << endpoints.second << std::endl;

	if (is_debug) {
		correct.copyTo(disp_frame);
	}
	cvtColor(disp_frame, disp_frame, COLOR_GRAY2BGR);

	//could determine endpoints
	if (endpoints.first != -1 && endpoints.second != -1) {
		if (is_debug) {
			line(disp_frame, Point(endpoints.first, 165), Point(endpoints.second, 165), Scalar(0, 255, 0), 5);
			line(disp_frame, Point(endpoints.first, 550), Point(endpoints.second, 550), Scalar(0, 255, 0), 5);
		}
		//try to estimate the middle part
		int mean = get_train_median(correct, endpoints.first, endpoints.second);
		
		//got a good mean
		if (mean != -1 && is_debug) {
			line(disp_frame, Point(mean, 0), Point(mean, correct.rows), Scalar(0, 255, 0), 5);
		}
		gap_positions[frame_count % BUFFER_LEN] = mean;
	}
	else {
		gap_positions[frame_count % BUFFER_LEN] = -1;
	}

	current_speed = 0;

	//speed text for debug
	std::string motion_text = "SPEED: ";
	if (is_moving) {
		current_speed = calc_speed(gap_positions, frame_count, prev_speed);
	}
	
	motion_text += std::to_string(current_speed) + " m/s";
	prev_speed = current_speed;
	
	if (is_debug) {
		putText(disp_frame, motion_text, Point(20, int(disp_frame.rows * 0.95f)), FONT_HERSHEY_SIMPLEX, 1.0f, Scalar(0, 0, 255), 3);
	}
	video << disp_frame;
	frame_count++;
	return current_speed;
}