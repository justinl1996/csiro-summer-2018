#pragma once
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/core/utility.hpp"

//location of top + bottom train edge (after barrel correction)
#define TRACK_TOP_Y 165
#define TRACK_BOT_Y 550
#define TRACK_HEIGHT 20

//buffer length of previous gap positions
#define BUFFER_LEN 10

//length of camera field of view (in mm)
#define CAMERA_LENGTH 12800
#define FPS 1

using namespace cv;
using namespace cv::detail;


class TrainMotion {
public:
	TrainMotion(bool debug = false) : is_debug(debug) {
		gap_positions.resize(BUFFER_LEN, 0);
	}
	float addFrame(const std::string &filename);
	void showFrame();
	void finish();

private:
	bool motion_detect(Mat &previous, Mat &current);
	void barrel_correct(const Mat &img, Mat &out);
	int get_train_median(Mat orig, int start, int end);
	std::pair<int, int> get_train_gap(const Mat &top_track, const Mat &bot_track);
	void dump_intensity(std::string filename, Mat img1, Mat img2);
	float calc_speed(const std::vector<int> &gap_positions, int current_frame,
		float prev_speed);

	//frames for computing motion detect
	Mat prev_frame, current_frame, disp_frame;
	VideoWriter video;

	//previous position of carriage gap
	std::vector<int> gap_positions; 
	bool is_moving = false;
	bool is_debug = false;
	float prev_speed = 0.0f;
	float current_speed = 0.0f;
	int frame_count = 0;
};

