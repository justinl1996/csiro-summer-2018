// MovingTrains.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>

#include "opencv2/stitching/detail/matchers.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include <fstream>
#include <algorithm>
#include "Motion.h"
#include "Util.h"
#include "Stat.h"


void template_match(Mat img, Mat tmp1, Mat tmp2, Mat tmp3) {
	Mat resultF(img.rows - tmp1.rows + 1, img.cols - tmp1.cols + 1, CV_32FC1);
	Mat resultB(img.rows - tmp2.rows + 1, img.cols - tmp2.cols + 1, CV_32FC1);
	Mat resultR(img.rows - tmp3.rows + 1, img.cols - tmp3.cols + 1, CV_32FC1);

	matchTemplate(img, tmp1, resultF, TM_CCORR_NORMED);
	matchTemplate(img, tmp2, resultB, TM_CCORR_NORMED);
	matchTemplate(img, tmp3, resultR, TM_CCORR_NORMED);

	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;
	/*minMaxLoc(resultF, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	matchLoc = maxLoc;
	rectangle(img, matchLoc, Point(matchLoc.x + tmp1.cols, matchLoc.y + tmp1.rows), Scalar(0, 255, 0), 2, 8, 0);

	std::cout << "max middle: " << maxVal << std::endl;*/

	/*minMaxLoc(resultB, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	matchLoc = maxLoc;
	rectangle(img, matchLoc, Point(matchLoc.x + tmp2.cols, matchLoc.y + tmp2.rows), Scalar(0, 255, 0), 2, 8, 0);
	std::cout << "max left: " << maxVal << std::endl;*/

	minMaxLoc(resultR, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	matchLoc = maxLoc;
	rectangle(img, matchLoc, Point(matchLoc.x + tmp3.cols, matchLoc.y + tmp3.rows), Scalar(0, 255, 0), 2, 8, 0);

	std::cout << "max right: " << maxVal << std::endl;

	imshow("template", img);
	waitKey(0);
}


int main(int argc, char **argv)
{
	if (argc != 2) {
		std::cerr << "usage: MovingTrains.exe train_directory" << std::endl;
		return 1;
	}
	std::vector<std::string> inputfiles = 
		Util::get_files_in_directory(argv[1]);

	Mat tmp1 = imread("C:\\Users\\luo029\\Pictures\\template\\carriage_front_middle0.png");
	Mat tmp2 = imread("C:\\Users\\luo029\\Pictures\\template\\carriage_back_left.png");
	Mat tmp3 = imread("C:\\Users\\luo029\\Pictures\\template\\carriage_front_right.png");


	TrainMotion trainMotion = TrainMotion(true);
	

	for (std::string filename : inputfiles) {
	//for (int i = 20; i < 70; i++) {
		//std::string filename = inputfiles[i];
		if (filename.find(".jpg") == std::string::npos) {
			continue;
		}
		float speed = trainMotion.addFrame(filename);
		std::cout << "speed: " << speed << std::endl;
		//trainMotion.showFrame();
		//std::cout << i << std::endl;
		Mat img = imread(filename);
		template_match(img, tmp1, tmp2, tmp3);
	}
	trainMotion.finish();
	return 0;
}

