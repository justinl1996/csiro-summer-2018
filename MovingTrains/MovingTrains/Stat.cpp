#include "pch.h"
#include "Stat.h"

/**Get the maximum value in an array such that it is within the z threshold */
int Stat::get_max_after_thresh(const std::vector<int> values, int mean, float std,
	float zthreshold) {
	for (size_t i = 0; i < values.size(); i++) {
		if ((abs(values[values.size() - i - 1] - mean) / std) < zthreshold) {
			return values[values.size() - i - 1];
		}
	}
	return -1;
}

/**Get the minimum value in an array such that it is within the z threshold */
int Stat::get_min_after_thresh(const std::vector<int> &values, int mean, float std,
	float zthreshold) {
	for (int i = 0; i < values.size(); i++) {
		if ((abs(values[i] - mean) / std) < zthreshold) {
			return values[i];
		}
	}
	return -1;
}