#pragma once
#include <iostream>
#include <vector>

class Stat {
public:
	static int get_max_after_thresh(const std::vector<int> values, int mean, float std,
		float threshold);
	static int get_min_after_thresh(const std::vector<int> &values, int mean, float std,
		float threshold);
	template<typename T> static float get_std(const std::vector<T> &values, float mean);
	template<typename T> static float get_mean(const std::vector<T> &values);
};

/**Get the standard deviation*/
template<typename T> float Stat::get_std(const std::vector<T> &values, float mean) {
	float std = 0.0f;
	for (int i = 0; i < values.size(); i++) {
		std += (values[i] - mean) * (values[i] - mean);
	}
	return sqrt(std / values.size());
}

/**Get the mean of integers*/
template<typename T> float Stat::get_mean(const std::vector<T> &values) {
	float mean = 0.0f;
	for (int i = 0; i < values.size(); i++) {
		mean += values[i];
	}
	return mean / values.size();
}