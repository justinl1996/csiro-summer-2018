#include "pch.h"
#include "Util.h"

/** Remove the extension of a filename */
std::string Util::remove_extension(const std::string &filename)
{
	std::size_t found = filename.rfind(".");
	return filename.substr(0, found);
}

/** Return all files in a directory */
std::vector<std::string> Util::get_files_in_directory(const std::string &directory)
{
	std::vector<std::string> result;

	//only works for c++17....
	for (const auto & entry : std::filesystem::directory_iterator(directory))
		//std::cout << entry.path() << std::endl;
		result.push_back(entry.path().string());

	return result;
}