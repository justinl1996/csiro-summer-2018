#pragma once
#include <iostream>
#include <vector>
#include <filesystem>

class Util {
public:
	static std::string remove_extension(const std::string &filename);
	static std::vector<std::string> get_files_in_directory(const std::string &directory);
};