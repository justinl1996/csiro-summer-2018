// partition_image.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <time.h>

using namespace cv;
using namespace std;

//const int GRID_SIZE = 500;
string remove_extension(string filename)
{
	size_t pos = filename.rfind(".");
	if (pos == string::npos) {
		return filename;
	}
	return filename.substr(0, pos);
}


bool crop_stripe(std::string filename, const Mat &image, int size, const std::string &orientation)
{
	int end = orientation == "h" ? image.rows : image.cols;
	Mat *region = NULL;
	int count = 0, length;

	for (int offset = 0; offset < end; offset += size) {
		if (orientation == "h") {
			length = offset + size < image.rows ? size : image.rows - offset;
			region = new Mat(image, Rect(0, offset, image.cols, length));
		} else {
			length = offset + size < image.cols ? size : image.cols - offset;
			region = new Mat(image, Rect(offset, 0, length, image.rows));
		}
		string outname = remove_extension(filename) + to_string(count) + ".jpg";
		bool result = imwrite(outname, *region);
		if (!result) {
			return false;
		}

		count++;
	}
	delete region;
	
}


bool crop_grid(std::string filename, const Mat &image, int gridsize, int overlap)
{
	int row = 0, col = 0;
	srand(time(NULL));
	for (int x = 0; x < image.cols; x += gridsize - overlap) {
		row = 0;
		for (int y = 0; y < image.rows; y += gridsize - overlap) {
			
			int y_noise = (y + rand() % int(overlap * 0.8f)) - (overlap * 0.8f)/2.0f;
			if (y_noise < 0 && y == 0) {
				y_noise = 0;
			}
			int x_noise = (x + rand() % int(overlap * 0.8f)) - (overlap * 0.8f)/2.0f;
			if (x_noise < 0 && x == 0) {
				x_noise = 0;
			}


			std::cout << "x:" << x_noise << "," << "y:" << y_noise << std::endl;
			std::cout << "x%:" << (abs(x_noise - x)/float(overlap)) * 100.0f << "," << 
				"y%:" << (abs(y_noise - y)/float(overlap)) * 100.0f << std::endl;

			int width = x + gridsize < image.cols ? gridsize : image.cols - x;
			int height = y_noise + gridsize < image.rows ? gridsize : image.rows - y_noise;

			Mat region(image, Rect(x, y_noise, width, height));

			string outname = remove_extension(filename) + "_r" + to_string(row) + "_c" + to_string(col) + ".jpg";
			bool result = imwrite(outname, region);
			if (!result) {
				return false;
			}
			row++;
		}
		col++;
	}
	return true;
}

int main(int argc, char** argv)
{
	if (argc < 4)
	{
		//cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		printf("splits an image into grid cells\n");
		printf("%s %s %s %s [overlap]\n", "partition_image", "infile", "gridsize", "h/v/g");
		return -1;
	}

	Mat image;
	image = imread(argv[1], IMREAD_COLOR); // Read the file

	int gridsize = atoi(argv[2]);
	std::string option = argv[3];

	if (!image.data) // Check for invalid input
	{
		cout << "Could not open or find the image: " << argv[1] << std::endl;
		return -1;
	}
	if (option == "g") {
		int overlap = 0;
		if (argc == 5) {
			overlap = atoi(argv[4]);
		}
		printf("overlap: %d\n", overlap);
		if (!crop_grid(String(argv[1]), image, gridsize, overlap)) {
			printf("Error slicing image\n");
			return -1;
		}
	}
	else if (option == "h" || option == "v") {
		if (!crop_stripe(String(argv[1]), image, gridsize, option)) {
			printf("Error slicing image\n");
			return -1;
		}
	}



	//namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	//imshow("Display window", region); // Show our image inside it.
	printf("success\n");
	//waitKey(0); // Wait for a keystroke in the window
	return 0;
}