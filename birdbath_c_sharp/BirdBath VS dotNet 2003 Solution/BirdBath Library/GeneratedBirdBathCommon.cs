namespace GeneratedBirdBath
{
	/// <remarks/>
    [System.Xml.Serialization.XmlType(Namespace = "urn:condor")]
    public class StringAndStatus
	{

        /// <remarks/>
        public Status status;

        /// <remarks/>
        [System.Xml.Serialization.XmlElement(IsNullable=true)]
        public string message;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlType(Namespace = "urn:condor")]
    public class Status
	{

        /// <remarks/>
        public StatusCode code;

        /// <remarks/>
        [System.Xml.Serialization.XmlElement(IsNullable=true)]
        public string message;

        /// <remarks/>
        [System.Xml.Serialization.XmlElement(IsNullable=true)]
        public Status next;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:condor")]
    public enum StatusCode
	{

        /// <remarks/>
        SUCCESS,

        /// <remarks/>
        FAIL,

        /// <remarks/>
        INVALIDTRANSACTION,

        /// <remarks/>
        UNKNOWNCLUSTER,

        /// <remarks/>
        UNKNOWNJOB,

        /// <remarks/>
        UNKNOWNFILE,

        /// <remarks/>
        INCOMPLETE,

        /// <remarks/>
        INVALIDOFFSET,

        /// <remarks/>
        ALREADYEXISTS,
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlType(Namespace = "urn:condor")]
    public class ClassAdStructAttr
	{

        /// <remarks/>
        public string name;

        /// <remarks/>
        public ClassAdAttrType type;

        /// <remarks/>
        public string value;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:condor")]
    public enum ClassAdAttrType
	{

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="INTEGER-ATTR")]
        INTEGERATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="FLOAT-ATTR")]
        FLOATATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="STRING-ATTR")]
        STRINGATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="EXPRESSION-ATTR")]
        EXPRESSIONATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="BOOLEAN-ATTR")]
        BOOLEANATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="UNDEFINED-ATTR")]
        UNDEFINEDATTR,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnum(Name="ERROR-ATTR")]
        ERRORATTR,
    }
}
