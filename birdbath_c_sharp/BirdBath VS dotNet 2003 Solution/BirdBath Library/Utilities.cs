using System;
using GeneratedBirdBath;

namespace BirdBath
{
	public class Utilities
	{
		public static bool isSuccess(Status status)
		{
			return StatusCode.SUCCESS.Equals(status.code);
		}

		public static ClassAdStructAttr getAttribute(string name,
													 ClassAdStructAttr[] ad)
		{
			foreach (ClassAdStructAttr attribute in ad)
			{
				if (attribute.name == name)
				{
					return attribute;
				}
			}

			return null;
		}

		public static ClassAdStructAttr[] setAttribute(ClassAdStructAttr newAttribute,
													   ClassAdStructAttr[] ad)
		{
			/* NOTES:
			 *  - This code is faster for setting attributes that already exist.
			 *  - This code does not make deep copies of attributes, so be careful!
			 */

			foreach (ClassAdStructAttr attribute in ad)
			{
				if (null != newAttribute.name &&
					null != attribute.name)
				{
					if (0 == newAttribute.name.ToUpper().CompareTo(attribute.name.ToUpper()))
					{
						attribute.type = newAttribute.type;
						attribute.value = newAttribute.value;

						return ad;
					}
				}

			}

			ClassAdStructAttr[] extendedAd = new ClassAdStructAttr[ad.Length + 1];
			for (int index = 0; index < ad.Length; index++)
			{
				extendedAd[index] = ad[index];
			}
			extendedAd[extendedAd.Length - 1] = newAttribute;

			return extendedAd;
		}

		public static ClassAdStructAttr[] setAttributes(ClassAdStructAttr[] newAttributes,
														ClassAdStructAttr[] ad)
		{
			foreach (ClassAdStructAttr attribute in newAttributes)
			{
				ad = setAttribute(attribute, ad);
			}

			return ad;
		}
	}
}
