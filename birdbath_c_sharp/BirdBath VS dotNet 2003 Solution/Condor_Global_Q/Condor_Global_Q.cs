
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;

namespace condor_global_q
{
	class Condor_Global_Q
	{
		static void Main(string[] args)
		{
			Condor condor = new Condor(args[0]); // arg[0] = Collector URL
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.
			ClassAd[] ads;
			Status status;
			if (condor.getJobs(args[1], out ads, out status))
			{
				if (ads != null)
				{
					foreach (ClassAd ad in ads)
					{
						Console.WriteLine(ad);
					}
				}
				else
				{
					Console.WriteLine("No results.");
				}
			}
			else
			{
				Console.WriteLine("Error: {0}; Reason: {1}",
								  status.code,
								  status.message);
			}
		}
	}
}
