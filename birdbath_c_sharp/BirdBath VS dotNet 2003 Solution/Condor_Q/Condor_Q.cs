
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;

namespace condor_q
{
	class Condor_Q
	{
		static void Main(string[] args)
		{
			Schedd schedd = new Schedd(args[0]);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.
			ClassAdStructAttr[][] ads;
			Status status;
			if (schedd.getJobAds(args[1], out ads, out status))
			{
				if (ads != null)
				{
					foreach (ClassAdStructAttr[] ad in ads)
					{
						Console.WriteLine(new ClassAd(ad));
					}
				}
				else
				{
					Console.WriteLine("No results.");
				}
			}
			else
			{
				Console.WriteLine("Error: {0}; Reason: {1}",
								  status.code,
								  status.message);
			}
		}
	}
}
