
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;

namespace condor_rm
{
	class Condor_RM
	{
		static void Main(string[] args)
		{
			int clusterId = int.Parse(args[1]);
			int jobId = int.Parse(args[2]);
			string reason = args[3];

			Schedd schedd = new Schedd(args[0]);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.
			Status result = schedd.closeSpool(clusterId, jobId);
			//System.FormatException (RESULT WAS NULL)
			Console.WriteLine("CloseSpool Result: {0}", result.code);
			if (result.code != StatusCode.SUCCESS)
			{
				Console.WriteLine("Reason: {0}", result.message);
			}
			else
			{
				result = schedd.removeJob(clusterId, jobId, reason);
				Console.WriteLine("RemoveJob Result: {0}", result.code);
				if (result.code != StatusCode.SUCCESS)
				{
					Console.WriteLine("Reason: {0}", result.message);
				}
			}
		}
	}
}
