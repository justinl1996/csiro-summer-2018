
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;
using Transaction = BirdBath.Transaction;
using FileInfo = System.IO.FileInfo;

namespace condor_retrieve
{
	class Condor_Retrieve
	{
		static void Main(string[] args)
		{
			string scheddEndpoint = args[0];
			int cluster = int.Parse(args[1]);
			int job = int.Parse(args[2]);

			Console.WriteLine("Retrieving {0}.{1}'s files...", cluster, job);

			Schedd schedd = new Schedd(scheddEndpoint);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.

			Status status;
			Transaction transaction = schedd.createTransaction();
			try
			{
				if (!transaction.begin(60, out status)) // timeout after 60 seconds between calls
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				GeneratedBirdBath.FileInfo[] files;
				if (!transaction.listSpool(cluster, job, out files, out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				foreach (GeneratedBirdBath.FileInfo file in files)
				{
					Console.WriteLine("Retrieving: {0}", file.name);
					if (!transaction.getFile(cluster,
											 job,
											 file.name,
											 (int) file.size,
											 new FileInfo(file.name),
											 out status))
					{
						Console.WriteLine("Status: {0}; Message: {1}",
										  status.code, status.message);
					}
				}

				if (!transaction.commit(out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}
			}
			catch (Exception exception)
			{
				if (!transaction.abort(out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				throw exception;
			}
		}
	}
}
