
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;

namespace condor_status
{
	class Condor_Status
	{
		static void Main(string[] args)
		{
//			string[] components = "<127.0.0.1:1980>".Split('<', '>');
//			Console.WriteLine(components.Length);
//			Console.WriteLine(components[1]);
			Collector collector = new Collector(args[0]);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.
			ClassAdStructAttr[][] result = collector.queryAnyAds(args[1]);
			if (result == null)
			{
				Console.WriteLine("result is null!");
			}
			else
			{
				//System.FormatException (RESULT WAS NULL)
//			Console.WriteLine("Result: {0}", result);
				foreach (ClassAdStructAttr[] ad in result)
				{
					Console.WriteLine(new ClassAd(ad));
				}
			}
		}
	}
}
