using System;
using System.Text;
using System.Collections;

using GeneratedBirdBath;

namespace BirdBath
{
	public class ClassAd
	{
		private Hashtable attributes;

		public ClassAd()
		{
			attributes = new Hashtable();
		}

		public ClassAd(ClassAdStructAttr[] classAdStruct): this()
		{
			foreach (ClassAdStructAttr attribute in classAdStruct)
			{
				attributes[attribute.name] = attribute.value;
			}
		}

		public string this[string key]
		{
			get {
				return (string) attributes[key];
			}
			set {
				attributes[key] = value;
			}
		}

		public override string ToString()
		{
			string output = "[\n";

			foreach (string name in attributes.Keys)
			{
				output += "\t" + name + "=" + attributes[name] + ";\n";
			}

			output += "]";

			return output;
		}
	}
}
