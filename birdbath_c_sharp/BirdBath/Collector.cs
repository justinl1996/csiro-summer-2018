using GeneratedBirdBath;

namespace BirdBath
{
	public class Collector
	{
		private condorCollector service;

		public Collector(string endpoint)
		{
			service = new condorCollector();
			service.Url = endpoint;
		}

		public condorCollector getService()
		{
			return service;
		}

		public ClassAdStructAttr[][] queryAnyAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryAnyAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] queryLicenseAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryLicenseAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] queryMasterAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryMasterAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] queryScheddAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryScheddAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] queryStartdAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryStartdAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] queryStorageAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.queryStorageAds(constraint);

			return result;
		}

		public ClassAdStructAttr[][] querySubmittorAds(string constraint)
		{
			ClassAdStructAttr[][] result = service.querySubmittorAds(constraint);

			return result;
		}
	}
}
