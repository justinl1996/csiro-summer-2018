using System;
using System.Collections;

using GeneratedBirdBath;

namespace BirdBath
{
	public class Condor
	{
		private Collector collector;

		/// <summary>
		/// Create a "Condor" -- really just a place to keep methods that apply
		/// to Schedds and Collectors at the same time. The endpoint should
		/// be one of a Collector.
		/// </summary>
		/// <param name="collectorEndpoint">A  string</param>
		public Condor(string collectorEndpoint)
		{
			collector = new Collector(collectorEndpoint);
		}

		/// <summary>
		/// Get all the jobs, ALL THE JOBS IN ALL THE SCHEDDS, matching the
		/// given constraintb.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="constraint">A  string</param>
		/// <param name="jobAds">A  ClassAd[]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getJobs(string constraint,
							out ClassAd[] jobAds,
							out Status status)
		{
			ArrayList ads = new ArrayList();

			status = null;

			ClassAd[] schedds = getSchedds("HasSOAPInterface=?=TRUE");
			foreach (ClassAd ad in schedds)
			{
				// MyAddress should always be "<ip:port>"
				string address = ad["MyAddress"];
				if (address != null)
				{
					string[] components = address.Split('<', '>');
					if (components.Length == 3)
					{
						Schedd schedd = new Schedd("http://" + components[1]);
						ClassAdStructAttr[][] jobs;
						if (!schedd.getJobAds(constraint, out jobs, out status))
						{
							jobAds = null;

							return false;
						}
						foreach (ClassAdStructAttr[] job in jobs)
						{
							ads.Add(new ClassAd(job));
						}
					}
					else
					{
						// XXX: It might be better to just ignore this...
						throw new UriFormatException("MyAddress attribute not '" +
													 "<ip:port>' but: " + address);
					}
				}
			}

			jobAds = (ClassAd[]) ads.ToArray(typeof(ClassAd));

			return true;
		}

		/// <summary>
		/// Find all the Schedds in the system that match the constraint.
		/// </summary>
		/// <returns>A ClassAd[]</returns>
		/// <param name="constraint">A  string</param>
		public ClassAd[] getSchedds(string constraint)
		{
			return query(constraint,
						 new CollectorQuery(collector.queryScheddAds));
		}

		/// <summary>
		/// Find all the Startds (resources) in the system that match the
		/// given constraint.
		/// </summary>
		/// <returns>A ClassAd[]</returns>
		/// <param name="constraint">A  string</param>
		public ClassAd[] getStartds(string constraint)
		{
			return query(constraint,
						 new CollectorQuery(collector.queryStartdAds));
		}

		/// <summary>
		/// Find all the submitters (users) in the system that match the
		/// given constraint.
		/// </summary>
		/// <returns>A ClassAd[]</returns>
		/// <param name="constraint">A  string</param>
		public ClassAd[] getSubmitters(string constraint)
		{
			return query(constraint,
						 new CollectorQuery(collector.querySubmittorAds));
		}

		private delegate ClassAdStructAttr[][] CollectorQuery(string constraint);

		private ClassAd[] query(string constraint, CollectorQuery queryDelegate)
		{
			ArrayList ads = new ArrayList();

			ClassAdStructAttr[][] schedds = queryDelegate(constraint);
			foreach (ClassAdStructAttr[] ad in schedds)
			{
				ads.Add(new ClassAd(ad));
			}

			return (ClassAd[]) ads.ToArray(typeof(ClassAd));
		}
	}
}
