using GeneratedBirdBath;

namespace BirdBath
{
	public class Schedd
	{
		private condorSchedd service;

		/// <summary>
		/// Create a Schedd object that can talk to the remote Schedd given in
		/// the endpoint argument, which must be a URL such as
		/// http://localhost:1234
		/// </summary>
		/// <param name="endpoint">A  string</param>
		public Schedd(string endpoint)
		{
			service = new condorSchedd();
			service.Url = endpoint;
		}

		/// <summary>
		/// Get at the actual remote Schedd service that is being used.
		/// </summary>
		/// <returns>A condorSchedd</returns>
		public condorSchedd getService()
		{
			return service;
		}

		/// <summary>
		/// Create a transaction on the Schedd. This is the proper way to
		/// create a transaction, don't bother using Transaction's constructor.
		/// Once the transaction is created call begin() on it to get things
		/// going.
		/// </summary>
		/// <returns>A Transaction</returns>
		public Transaction createTransaction()
		{
			return new Transaction(service);
		}

		/// <summary>
		/// Request the Schedd enter a negotiation cycle. This should be called
		/// after committing a transaction where a job was submitted.
		/// </summary>
		/// <returns>A Status</returns>
		public Status requestReschedule()
		{
			return service.requestReschedule();
		}

		/// <summary>
		/// Get a given job's ad.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="ad">A  ClassAdStructAttr[]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getJobAd(int cluster,
							 int job,
							 out ClassAdStructAttr[] ad,
							 out Status status)
		{
			ClassAdStructAndStatus result = service.getJobAd(null, cluster, job);
			ad = result.classAd;
			status = result.status;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Get all job ads matching the given constraint.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="constraint">A  string</param>
		/// <param name="ads">A  ClassAdStructAttr[][]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getJobAds(string constraint,
							  out ClassAdStructAttr[][] ads,
							  out Status status)
		{
			ClassAdStructArrayAndStatus result = service.getJobAds(null, constraint);
			ads = result.classAdArray;
			status = result.status;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Put then given job on hold.
		/// </summary>
		/// <returns>A Status</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="reason">A  string</param>
		public Status holdJob(int cluster,
							  int job,
							  string reason)
		{
			return service.holdJob(null, // work outside a transaction
								   cluster, job, reason,
								   false, // notify owner via email
								   false, // notify admin via email
								   false); // hold was initiated inside Condor
		}


		/// <summary>
		/// Release the given job, which was previously on hold.
		/// </summary>
		/// <returns>A Status</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="reason">A  string</param>
		public Status releaseJob(int cluster,
								 int job,
								 string reason)
		{
			Status status =
				service.releaseJob(null, // work outside a transaction
								   cluster, job, reason,
								   false, // notify owner via email
								   false); // notify admin via email

			return status;
		}

		/// <summary>
		/// Remove a cluster. This will remove and kill all jobs in said
		/// cluster. Be warned that CloseSpool() may need to be called
		/// to really get rid of the jobs.
		/// </summary>
		/// <returns>A Status</returns>
		/// <param name="cluster">An int</param>
		/// <param name="reason">A  string</param>
		public Status removeCluster(int cluster,
									string reason)
		{
			Status status =
				service.removeCluster(null, // work outside a transaction
									  cluster, reason);

			return status;
		}

		/// <summary>
		/// Remove a job. Be sure to close its spool (CloseSpool call) too.
		/// </summary>
		/// <returns>A Status</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="reason">A  string</param>
		public Status removeJob(int cluster,
								int job,
								string reason)
		{
			Status status =
				service.removeJob(null, // work outside a transaction
								  cluster, job, reason,
								  false); // force removal (condor_rm -forcex)

			return status;
		}

		/// <summary>
		/// All jobs are submitted (by default) in a state that will make them
		/// stay in the queue until their spool is closed, with this function.
		/// For more information have a look at documentation for a job ad's
		/// LeaveInQueue attribute.
		/// </summary>
		/// <returns>A Status</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		public Status closeSpool(int cluster,
								 int job)
		{
			return service.closeSpool(null, // work outside a transaction
									  cluster, job);
		}

		/// <summary>
		/// This method creates a template job ad that can be passed to submit.
		/// This should save clients from having to know about all the important
		/// attributes on job ads.
		/// </summary>
		/// <returns>A ClassAd</returns>
		/// <param name="clusterId">An int</param>
		/// <param name="jobId">An int</param>
		/// <param name="owner">A  string</param>
		/// <param name="universe">An UniverseType</param>
		/// <param name="command">A  string</param>
		/// <param name="args">A  string</param>
		/// <param name="requirements">A  string</param>
		public ClassAd createTemplate(int clusterId,
									  int jobId,
									  string owner,
									  UniverseType universe,
									  string command,
									  string args,
									  string requirements)
		{
			ClassAdStructAndStatus result =
				service.createJobTemplate(clusterId,
										  jobId,
										  owner,
										  universe,
										  command,
										  args,
										  requirements);

			ClassAd template = null;

			if (result.status.code == StatusCode.SUCCESS)
			{
				template = new ClassAd(result.classAd);
			}

			return template; // null on error
		}
	}
}
