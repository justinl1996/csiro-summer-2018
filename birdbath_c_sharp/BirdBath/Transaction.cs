using System.IO;

using GeneratedBirdBath;
using System;

namespace BirdBath
{
	public class Transaction
	{
		private condorSchedd service;
		private GeneratedBirdBath.Transaction transaction;

		/// <summary>
		/// Create a transaction that will work on the given service.
		/// Transactions should normally be created from
		/// Schedd.createTransaction.
		/// </summary>
		/// <param name="service">A Schedd service instance</param>
		public Transaction(condorSchedd service)
		{
			this.service = service;
		}

		/// <summary>
		/// Begin the transaction. The timeout given is the amount of time
		/// in seconds allowed between operations in the transaction. If
		/// the timeout is reached the remote Schedd will automatically
		/// abort the transaction.
		/// </summary>
		/// <returns>Success of failure</returns>
		/// <param name="timeout">A timeout in seconds</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool begin(int timeout, out Status status)
		{
			TransactionAndStatus result = service.beginTransaction(timeout);
			status = result.status;
			this.transaction = result.transaction;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Commit the transaction.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool commit(out Status status)
		{
			status = service.commitTransaction(transaction);

			return Utilities.isSuccess(status);

			/*
			 * A transaction should not be used after it is committed, but
			 * the schedd should notify the user of this instead of storing
			 * such informatin in these helper classes.
			 service = null;
			 transaction = null;
			 */
		}

		/// <summary>
		/// Abort the transaction.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool abort(out Status status)
		{
			status = service.abortTransaction(transaction);

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Create a new cluster in which to add jobs.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="id">The cluster's id</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool createCluster(out int id, out Status status)
		{
			IntAndStatus result = service.newCluster(transaction);
			status = result.status;
			id = result.integer;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Create a new job in the given cluster. Jobs should only be created
		/// in the most recently created cluster.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">The cluster's id</param>
		/// <param name="id">An job's id</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool createJob(int cluster, out int id, out Status status)
		{
			IntAndStatus result = service.newJob(transaction, cluster);
			status = result.status;
			id = result.integer;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Remove a job from the Schedd. This will also kill a running job.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="reason">A  string</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool removeJob(int cluster,
							  int job,
							  string reason,
							  out Status status)
		{
			status = service.removeJob(transaction,
									   cluster,
									   job,
									   reason,
									   false);

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Submit a job's ClassAd. The arrtibutes parameter allows for addition
		/// of attribute that must be present in the job's ad, such as Out/Err/Log
		/// files. The files parameter specifies all files that will be sent
		/// as part of the job.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="owner">A  string</param>
		/// <param name="universe">An UniverseType</param>
		/// <param name="command">A  string</param>
		/// <param name="arguments">A  string</param>
		/// <param name="requirements">A  string</param>
		/// <param name="attributes">A  ClassAdStructAttr[]</param>
		/// <param name="files">A  System.IO.FileInfo[]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool submit(int cluster,
						   int job,
						   string owner,
						   UniverseType universe,
						   string command,
						   string arguments,
						   string requirements,
						   ClassAdStructAttr[] attributes,
						   System.IO.FileInfo[] files,
						   out Status status)
		{
			if (null == owner)
			{
				owner = Environment.UserName;
			}

			if (null == command)
			{
				throw new ArgumentException("command argument cannot be null.");
			}

			if (null == arguments)
			{
				arguments = "";
			}

			if (null == requirements)
			{
				requirements = "TRUE";
			}

			ClassAdStructAndStatus templateResult =
				service.createJobTemplate(cluster,
										  job,
										  owner,
										  universe,
										  command,
										  arguments,
										  requirements);
			status = templateResult.status;
			if (!Utilities.isSuccess(status))
			{
				return false;
			}

			ClassAdStructAttr[] template = templateResult.classAd;

			if (null != attributes)
			{
				template = Utilities.setAttributes(attributes, template);
			}

			if (null != files)
			{
				for (int index = 0; index < files.Length; index++)
				{
					sendFile(cluster, job, files[index], out status);
				}
			}

			RequirementsAndStatus submitResult = service.submit(transaction,
																cluster,
																job,
																template);
			status = submitResult.status;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Send a specific file to the given job's spool directory.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="file">A  System.IO.FileInfo</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool sendFile(int cluster,
							 int job,
							 System.IO.FileInfo file,
							 out Status status)
		{
			/* NOTES:
			 *  - Size of each chunk is fixed.
			 */

			int CHUNK_SIZE = 512 * 1024; // 512KB

			if (!file.Exists)
			{
				throw new ArgumentException("file '" + file.Name + "' does not exist");
			}

			using (FileStream input =
					   file.Open(FileMode.Open, FileAccess.Read))
			{
				status = service.declareFile(transaction,
											 cluster,
											 job,
											 file.Name,
											 (int) file.Length,
											 HashType.NOHASH,
											 null);
				if (!Utilities.isSuccess(status))
				{
					return false;
				}

				byte[] readBuffer = new byte[CHUNK_SIZE];
				byte[] writeBuffer;
				int length;
				int offset = 0;
				// FileStream.Read returns zero when it reaches the end of the stream.
				while (0 != (length = input.Read(readBuffer, 0, CHUNK_SIZE)))
				{
					/*
					 * Incase the file is not a multiple of the CHUNK_SIZE or
					 * read() does not populate the full buffer, we must create
					 * an output buffer that is exactly the size of the data. This
					 * is only an issue because the base64 encoding encodes the
					 * entire output buffer, not just a portion of it.
					 */
					if (CHUNK_SIZE != length)
					{
						writeBuffer = new byte[length];
						System.Array.Copy(readBuffer, writeBuffer, length);
					}
					else
					{
						writeBuffer = readBuffer;
					}

					status = service.sendFile(transaction,
											  cluster,
											  job,
											  file.Name,
											  offset,
											  writeBuffer);

					if (!Utilities.isSuccess(status))
					{
						input.Close();

						return false;
					}

					offset += length;
				}

				input.Close();
			}

			return true;
		}

		/// <summary>
		/// Get a named file (remoteName) out of the given job's spool directory
		/// and store it locally (localFile).
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="remoteName">A filename</param>
		/// <param name="size">An int</param>
		/// <param name="localFile">A System.IO.FileInfo</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getFile(int cluster,
							int job,
							string remoteName,
							int size,
							System.IO.FileInfo localFile,
							out Status status)
		{
			/* NOTES:
			 *  - Size of each chunk is fixed.
			 */

			int CHUNK_SIZE = 512 * 1024; // 512KB

			status = null;

			using (FileStream output =
					   localFile.Open(FileMode.Create, FileAccess.Write))
			{
				Base64DataAndStatus result;
				int offset = 0;
				while (offset < size)
				{
					result = service.getFile(transaction,
											 cluster,
											 job,
											 remoteName,
											 offset,
											 Math.Min(CHUNK_SIZE, size - offset));
					status = result.status;

					if (!Utilities.isSuccess(status))
					{
						output.Close();

						return false;
					}

					output.Write(result.data, 0, result.data.Length);
					offset += result.data.Length;
				}

				output.Close();
			}

			return true;
		}

		/// <summary>
		/// Get a listing of all the available files in a job's spool directory.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="files">A  GeneratedBirdBath.FileInfo[]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool listSpool(int cluster,
							  int job,
							  out GeneratedBirdBath.FileInfo[] files,
							  out Status status)
		{
			FileInfoArrayAndStatus result = service.listSpool(transaction, cluster, job);
			files = result.info;
			status = result.status;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Close a job's spool directory. This is used to signal the Schedd
		/// that the job's spool directory can be deleted. This must also be
		/// called to completely remove a job from the queue. Using RemoveJob()
		/// may leave the job in an "X" state in the queue until this method
		/// is called.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool closeSpool(int cluster,
							   int job,
							   out Status status)
		{
			status = service.closeSpool(transaction, cluster, job);

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Get the ClassAd that represents the given job.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="cluster">An int</param>
		/// <param name="job">An int</param>
		/// <param name="ad">A  ClassAdStructAttr[]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getJobAd(int cluster,
							 int job,
							 out ClassAdStructAttr[] ad,
							 out Status status)
		{
			ClassAdStructAndStatus result =
				service.getJobAd(transaction, cluster, job);
			ad = result.classAd;
			status = result.status;

			return Utilities.isSuccess(status);
		}

		/// <summary>
		/// Get all job ClassAds matching then given constraint.
		/// </summary>
		/// <returns>Success or failure</returns>
		/// <param name="constraint">A  string</param>
		/// <param name="ads">A  ClassAdStructAttr[][]</param>
		/// <param name="status">Out param that can be checked if returnvalue
		/// is false</param>
		public bool getJobAds(string constraint,
							  out ClassAdStructAttr[][] ads,
							  out Status status)
		{
			ClassAdStructArrayAndStatus result =
				service.getJobAds(transaction, constraint);
			ads = result.classAdArray;
			status = result.status;

			return Utilities.isSuccess(status);
		}
	}
}
