
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;

namespace condor_release
{
	class Condor_Release
	{
		static void Main(string[] args)
		{
			int clusterId = int.Parse(args[1]);
			int jobId = int.Parse(args[2]);
			string reason = args[3];

			Schedd schedd = new Schedd(args[0]);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.
			Status result = schedd.releaseJob(clusterId, jobId, reason);
			//System.FormatException (RESULT WAS NULL)
			//Console.WriteLine("CloseSpool Result: {0}", result.code);
			if (result.code != StatusCode.SUCCESS)
			{
				Console.WriteLine("Relase failed, reason: {0}", result.message);
			}
			else
			{
				Console.WriteLine("Job {0}.{1} released	.", clusterId, jobId);
			}
		}
	}
}
