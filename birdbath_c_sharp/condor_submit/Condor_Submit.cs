
using System;
using System.Text;
using BirdBath;
using GeneratedBirdBath;
using Transaction = BirdBath.Transaction;
using FileInfo = System.IO.FileInfo;

namespace condor_submit
{
	class Condor_Submit
	{
		static void Main(string[] args)
		{
			string scheddEndpoint = args[0];
			string command = args[1];
			string arguments = args[2];
			UniverseType universe = UniverseType.VANILLA;
			string requirements = args[3];

			Schedd schedd = new Schedd(scheddEndpoint);
			//System.UriFormatException (BAD PORT)
			//System.Net.WebException (NameResolutionFailure) (ConnectFailure) (CONNECTION RESET)
			//System.NotSupportedException (BAD PROTOCOL)
			//System.Web.Services.Protocols.SoapException (METHOD NOT IMPLEMENTED)
			//If url is https just get a pegged CPU and nothing returns.

			Status status;
			Transaction transaction = schedd.createTransaction();
			try
			{
				if (!transaction.begin(60, out status)) // timeout after 60 seconds between calls
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				int cluster;
				int job;

				if (!transaction.createCluster(out cluster, out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}
				if (!transaction.createJob(cluster, out job, out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				// The first three files will be log/out/err below...
				FileInfo[] files = new FileInfo[args.Length - 4 + 3];
				for (int i = 4; i < args.Length; i++)
				{
					files[i] = new FileInfo(args[i]);
				}

				ClassAdStructAttr[] extra = new ClassAdStructAttr[3];
				extra[0] = new ClassAdStructAttr();
				extra[0].name = "UserLog";
				extra[0].type = ClassAdAttrType.STRINGATTR;
				extra[0].value = command + ".log";
				extra[1] = new ClassAdStructAttr();
				extra[1].name = "Out";
				extra[1].type = ClassAdAttrType.STRINGATTR;
				extra[1].value = command + ".out";
				extra[2] = new ClassAdStructAttr();
				extra[2].name = "Err";
				extra[2].type = ClassAdAttrType.STRINGATTR;
				extra[2].value = command + ".err";

				// Create empty versions of the log/out/err files so we have
				// something to pretend to send over to the Schedd.
				for (int i = 0; i < 3; i++)
				{
					files[i] = new FileInfo(new FileInfo(extra[i].value).Name);
					// Fix the value, it might have been a full path, if
					// the command was specified with a path!
					extra[i].value = files[i].Name;
					files[i].Create().Close();
				}

				if (!transaction.submit(cluster,
										job,
										null,
										universe,
										command,
										arguments,
										requirements,
										extra,
										files,
										out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				if (!transaction.commit(out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}
			}
			catch (Exception exception)
			{
				if (!transaction.abort(out status))
				{
					Console.WriteLine("Status: {0}; Message: {1}",
									  status.code, status.message);
				}

				throw exception;
			}
		}
	}
}
