// invert_image.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;

std::string remove_extension(std::string filename)
{
	size_t pos = filename.rfind(".");
	if (pos == std::string::npos) {
		return filename;
	}
	return filename.substr(0, pos);
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("inverts an image\n");
		printf("usage: %s %s %s\n", argv[0], "input_image", "some argument");
		return -1;
	}

	for (int i = 2; i < argc; i++) {
		printf("additional arg: %s\n", argv[i]);
	}

	Mat image = imread(argv[1]);
	Mat new_image = Mat::zeros(image.size(), image.type());

	for (int y = 0; y < image.rows; y++) {
		for (int x = 0; x < image.cols; x++) {
			for (int c = 0; c < image.channels(); c++) {
				new_image.at<Vec3b>(y, x)[c] = 255 - image.at<Vec3b>(y, x)[c];
			}
		}
	}
	
	std::string filename = remove_extension(argv[1]) + "_out.jpg";
	bool result = imwrite(filename, new_image);
	if (!result) {
		printf("Error saving image\n");
		return -1;
	}
	/*imshow("Original Image", image);
	imshow("New Image", new_image);
	waitKey();*/



	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
