// partition_image.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

const int GRID_SIZE = 500;


string remove_extension(string filename)
{
	size_t pos = filename.rfind(".");
	if (pos == string::npos) {
		return filename;
	}
	return filename.substr(0, pos);
}

void output_file(string outfile, std::vector<string> filenames, int rows, int cols) 
{
	ofstream out(outfile);
	
	out << rows << "," << cols << endl;
	for (std::vector<string>::const_iterator p = filenames.begin(); p != filenames.end(); p++) {
		out << *p << endl;
	}
	out.close();
}

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		//cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		printf("splits an image into grid cells\n");
		printf("usage: %s %s %s\n", argv[0], "input_image", "outfile");
		return -1;
	}

	Mat image;
	image = imread(argv[1], IMREAD_COLOR); // Read the file


	if (!image.data) // Check for invalid input
	{
		cout << "Could not open or find the image: " << argv[1] << std::endl;
		return -1;
	}

	std::vector<string> filenames;
	int count = 0;

	for (int x = 0; x < image.cols; x += GRID_SIZE) {
		for (int y = 0; y < image.rows; y += GRID_SIZE) {
			int width = x + GRID_SIZE < image.cols ? GRID_SIZE : image.cols - x;
			int height = y + GRID_SIZE < image.rows ? GRID_SIZE : image.rows - y;

			Mat region(image, Rect(x, y, width, height));

			string filename = remove_extension(argv[1]) + to_string(count) + ".jpg";
			filenames.push_back(filename);
			bool result = imwrite(filename, region);
			if (!result) {
				printf("Error slicing image\n");
				return -1;
			}
			count++;
		}
	}

	int rows = image.rows / GRID_SIZE + 1;
	int cols = image.cols / GRID_SIZE + 1;
	
	output_file(argv[2], filenames, rows, cols);
	//namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	//imshow("Display window", region); // Show our image inside it.
	printf("success\n");
	//waitKey(0); // Wait for a keystroke in the window
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
