#include "stdafx.h"
#include "CppUnitTest.h"
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "../shading/region.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestCVConcat)
		{
			cv::Mat top(cv::Size(10, 10), CV_8U, 1);
			cv::Mat bot(cv::Size(11, 10), CV_8U, 1);

			std::vector<cv::Mat> all = { top, bot };
			cv::Mat finish;
			cv::vconcat(all, finish);
			//printf("SDASD\n");
			cv::imshow("cv", finish);
			cv::waitKey(0);
		}


		TEST_METHOD(TestFilterTrueSeams)
		{
			// TODO: Your test code here
			/*std::vector<int> seams;
			std::vector<int> result = filter_true_seams(seams);
			
		
			Assert::AreEqual(0, (int)result.size());

			seams.push_back(1);
			result = filter_true_seams(seams);
			Assert::AreEqual(0, (int)result.size());

			
			seams.push_back(3);
			seams.push_back(4);
			result = filter_true_seams(seams);
			Assert::AreEqual(1, (int)result.size());
			Assert::AreEqual(3, result[0]);

			seams.clear();
			Assert::AreEqual(0, int(seams.size()));

			seams.push_back(1);
			seams.push_back(2);
			seams.push_back(3);
			result = filter_true_seams(seams);
			Assert::AreEqual(0, int(result.size()));

			seams.clear();
			seams.push_back(400);
			seams.push_back(401);
			seams.push_back(300);
			seams.push_back(301);
			result = filter_true_seams(seams);
			Assert::AreEqual(400, result[0]);
			Assert::AreEqual(300, result[1]);

			seams.clear();
			seams.push_back(400);
			seams.push_back(401);
			seams.push_back(402);
			seams.push_back(300);
			seams.push_back(301);
			result = filter_true_seams(seams);
			Assert::AreEqual(300, result[0]);

			seams.push_back(302);
			result = filter_true_seams(seams);
			Assert::AreEqual(0, int(result.size()));

			seams.push_back(501);
			seams.push_back(502);
			seams.push_back(504);
			
			result = filter_true_seams(seams);
			Assert::AreEqual(501, result[0]);
			Assert::AreEqual(1, int(result.size()));*/
		}

	};
}