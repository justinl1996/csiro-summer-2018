// edge_detect.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

void hough_lines(const Mat &src, Mat &dst) {
	//HoughLines(sobelx, lines, 1, CV_PI / 360, 200); // runs the actual detection
	std::vector<Vec2f> lines;
	HoughLines(src, lines, 1, CV_PI / 360, 200);
	// Draw the lines
	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a * rho, y0 = b * rho;
		pt1.x = cvRound(x0 + 2000 * (-b));
		pt1.y = cvRound(y0 + 2000 * (a));
		pt2.x = cvRound(x0 - 2000 * (-b));
		pt2.y = cvRound(y0 - 2000 * (a));

		if (pt1.y == pt2.y || pt1.x == pt2.x) {
			line(dst, pt1, pt2, Scalar(0, 0, 255), 1, LINE_AA);
		}
	}
}

void hough_lines_p(const Mat &src, Mat &dst) {
	std::vector<Vec4i> lines;
	HoughLinesP(src, lines, 1, CV_PI / 180, 1, 2);
	// Draw the lines
	for (size_t i = 0; i < lines.size(); i++)
	{
		int x1 = lines[i][0];
		int y1 = lines[i][1];

		int x2 = lines[i][2];
		int y2 = lines[i][3];

		if ((y1 == y2 || x1 == x2)) {
			line(dst, Point(x1, y1), Point(x2, y2), Scalar(0, 0, 255), 1, LINE_AA);
		}
	}
}

void shading_detection(const Mat &src)
{
	Mat rows, cols;
	reduce(src, cols, 0, CV_REDUCE_AVG);
	reduce(src, rows, 1, CV_REDUCE_AVG);

	for (int i = 0; i < rows.rows; i++) {
		int val = int(rows.at<uchar>(i, 0));
		if (val > 180) {
			std::cout << i << std::endl;
		}
		//std::cout << val << std::endl;
	}

	for (int i = 0; i < cols.cols; i++) {
		int val = int(cols.at<uchar>(0, i));
		if (val < 135) {
			std::cout << i << std::endl;
		}
		//std::cout << val << std::endl;
	}

	//std::cout << rows.size() << std::endl;
	//std::cout << cols.size() << std::endl;
}


int main()
{
	Mat src1;
	//src1 = imread("C:\\Users\\luo029\\Pictures\\shading\\cals_small.tif", IMREAD_GRAYSCALE);
	src1 = imread("C:\\Users\\luo029\\Pictures\\shading\\coals.tif", IMREAD_GRAYSCALE);
	//src1 = imread("C:\\Users\\luo029\\Pictures\\testStitch2.png", IMREAD_GRAYSCALE);
	Mat blur;
	//GaussianBlur(src1, blur, Size(9, 9), 10.0f);

	//namedWindow("Original image");

	//Mat grey;
	//cvtColor(src1, grey, CV_BGR2GRAY);
	//imshow("Original image", src1);
	//waitKey(0);


	
	Mat sobelx, cdst;
	//Sobel(src1, sobelx, CV_8U, 1, 0, 1, 10.0f, 150.0f);
	
	Sobel(src1, sobelx, CV_8U, 1, 0, 1, 10.0f, 150.0f);
	
	//Canny(src1, sobelx, 20.0f, 100.0f, 3);
	cvtColor(src1, cdst, COLOR_GRAY2BGR);
	shading_detection(sobelx);
	//Mat thresh;
	//threshold(sobelx, thresh, 90.0f, 255.0f, THRESH_BINARY);

	imshow("image", sobelx);
	waitKey(0);

	double minVal, maxVal;
	minMaxLoc(sobelx, &minVal, &maxVal); //find minimum and maximum intensities
	cout << "minVal : " << minVal << endl << "maxVal : " << maxVal << endl;

	//hough_lines_p(sobelx, cdst);

	//imshow("LINES", cdst);
	//waitKey(0);

	//Mat draw;
	//sobelx.convertTo(draw, CV_8U, 255.0 / (maxVal - minVal), -minVal * 255.0 / (maxVal - minVal));

	//namedWindow("image", CV_WINDOW_AUTOSIZE);
	//imshow("image", sobelx);

	
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
