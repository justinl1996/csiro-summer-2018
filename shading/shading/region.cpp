
#include "pch.h"
#include <vector>
#include <thread>

#include "region.h"
#include "Timer.h"


/**
Remove false seams (seams that don't have an adjacent one)
*/
std::vector<int> Region::filter_true_seams(const std::vector<int> &seams) 
{
	std::vector<int> result;
	//cannot possibly be a seam
	if (seams.size() == 1 ||  seams.size() == 0) {
		return result;
	}
	//single seam
	else if (seams.size() == 2 && seams[0] + 1 == seams[1]) {
		result.push_back(seams[0]);
		return result;
	}

	//extract only true positive seams (two adjacent to each other)
	for (int i = 0; i < int(seams.size()) - 2; i++) {
		if ((seams[i] + 1 == seams[i + 1])) {
			//third value over is not a seam
			if (seams[i] + 2 != seams[i + 2]) {
				result.push_back(seams[i]);
				i++;
			}
			else {
				//push forward until we find a non-consecutive value 
				do {
					i++;
				} while (i < seams.size() && seams[i] - 1 == seams[i - 1]);
				//since the for loop will increment by one
				i--;
			}
		}
	}

	//last two values
	if (seams[seams.size() - 2] + 1 == seams[seams.size() - 1] && 
		seams[seams.size() -3] + 2 != seams[seams.size() - 2] + 1) {
		result.push_back(seams[seams.size() - 2]);
	}

	return result;
}

/**
Retrieve the seams of the image either for columns or rows. 
Return all pixels columns or row of a possible seam
*/
std::vector<int> Region::get_seams(const cv::Mat &img, ROW_COL_T row_col, int threshold) 
{
	cv::Mat mat;
	std::vector<int> seams;
	//std::vector<std::pair<int,int>> result;
	std::vector<int> result;

	if (row_col == ROW) {
		//compute average for each row
		reduce(img, mat, 1, CV_REDUCE_AVG);
		for (int i = 0; i < mat.rows; i++) {
			int val = int(mat.at<uchar>(i, 0));
			//lighter for horizontal lines (using sobel)
			if (val > threshold) {
				seams.push_back(i);
			}
		}
	}
	else if (row_col == COL) {
		//compute average for each column
		reduce(img, mat, 0, CV_REDUCE_AVG);
		for (int i = 0; i < mat.cols; i++) {
			int val = int(mat.at<uchar>(0, i));
			//darker for vertical lines (using sobel)
			if (val < threshold) {
				seams.push_back(i);
			}
		}
	}
	
	//filter out false positives
	return filter_true_seams(seams);
}

/**
Find all regions defined by the seams.
Return 2D array of each row of images cropped out
*/
std::vector<std::vector<cv::Mat>> Region::find_regions(const cv::Mat &img)
{
	//empirically determined threshold
	const int x_threshold = 120;
	const int y_threshold = 180;

	cv::Mat gray;
	cv::Mat sobely, cdst;
	cv::cvtColor(img, gray, CV_BGR2GRAY);

	//std::vector<int> seams_x;
	std::vector<int> seams_y;

	Timer timer1;
	cv::Sobel(gray, sobely, CV_8U, 0, 1, 1, 10.0f, 150.0f);
	//cv::imshow("sobel", sobely);
	//cv::waitKey(0);

	//filter false positives to get the actual seams
	seams_y = get_seams(sobely, ROW, 180);
	seams_y.push_back(gray.rows);

	std::vector<cv::Mat> sobelx(seams_y.size());
	std::vector<std::vector<int>> seams_x;

	int topy = 0;
	
#if USE_PARALLEL
	std::vector<std::thread> workers;
	//iterate over regions bounded by seams on horizontal axis to find vertical seams
	for (int y = 0; y < seams_y.size(); y++) {
		std::thread th([&](int i) {	
			cv::Mat region = cv::Mat(gray, cv::Rect(0, topy, gray.cols, seams_y[i] - topy));
			cv::Sobel(region, sobelx[i], CV_8U, 1, 0, 1, 10.0f, 150.0f);
		}, y);
		workers.push_back(std::move(th));
		topy = seams_y[y] + 1;
	}
	for (int y = 0; y < workers.size(); y++) {
		workers[y].join();
		std::vector<int> temp = get_seams(sobelx[y], COL, x_threshold);
		seams_x.push_back(temp);
	}
#else
	for (int y = 0; y < seams_y.size(); y++) {
		cv::Mat region = cv::Mat(gray, cv::Rect(0, topy, gray.cols, seams_y[y] - topy));
		cv::Sobel(region, sobelx[y], CV_8U, 1, 0, 1, 10.0f, 150.0f);

		std::vector<int> temp = get_seams(sobelx[y], COL, x_threshold);
		seams_x.push_back(temp);
		topy = seams_y[y] + 1;
	}
#endif

	std::vector<std::vector<cv::Mat>> regions;
	int topleftx, toplefty = 0;
	
	//build Mat objects for each sub region
	for (int i = 0; i < seams_x.size(); i++) {
		std::vector<cv::Mat> row_mat;
		topleftx = 0;
		for (int j = 0; j < seams_x[i].size(); j++) {
			int width = seams_x[i][j] - topleftx;
			int height = seams_y[i] - toplefty;
			
			cv::Mat mat = cv::Mat(img, cv::Rect(topleftx, toplefty, width, height));
			row_mat.push_back(mat);
			//printf("%d,%d,%d,%d\n", topleftx, toplefty, width, height);

			topleftx = seams_x[i][j] + 1;
		}
		toplefty = seams_y[i] + 1;
		regions.push_back(row_mat);
	}

	std::cout << "regions: " << timer1.elapsed() << std::endl;
	return regions;
}

/**
Resize a calibration image to match
Return true on success, false if the calibrate image is too large or small 
(defined by MAX_IMG_DIFF)
*/
bool Region::resize_to_fit(const cv::Mat &img, cv::Mat &cal) 
{
	int x_diff = abs(img.rows - cal.rows);
	int y_diff = abs(img.cols - cal.cols);

	//printf("x_diff: %d, y_diff: %d\n", x_diff, y_diff);
	if (x_diff > MAX_IMG_DIFF || y_diff > MAX_IMG_DIFF) {
		return false;
	}

	cv::resize(cal, cal, img.size());
	return true;
}

/**
Correct the shading for the image using a corrected calibrated image 
and a mean background image.
Returns the fixed image
*/
cv::Mat Region::fix_shading(const cv::Mat &img, cv::Mat cal, const cv::Mat &mean)
{
	//region too large or too small w.r.t calibration image so don't bother 
	if (!Region::resize_to_fit(img, cal)) {
		return img;
	}

	std::vector<cv::Mat> rgb;
	cv::split(cal, rgb);

	std::vector<cv::Mat> gain(rgb.size());
	std::vector<cv::Mat> out;

	//compute the required gain to for the pixel intensities
	for (int c = 0; c < rgb.size(); c++) {
		gain[c] = cv::Mat(rgb[c].size(), CV_32F);
		cv::divide(mean.at<double>(c, 0), rgb[c], gain[c], CV_32F);
	}

	cv::split(img, rgb);

	//convert to float and multiply
	for (int c = 0; c < rgb.size(); c++) {
		cv::Mat temp;
		rgb[c].convertTo(temp, CV_32F);
		out.push_back(temp.mul(gain[c]));
	}

	//threshold and convert back to uchar (0-255)
	for (int i = 0; i < out.size(); i++) {
		cv::threshold(out[i], out[i], 255, 0, cv::THRESH_TRUNC);
		out[i].convertTo(rgb[i], CV_8U);
	}

	cv::Mat output;
	merge(rgb, output);

	return output;
}

/**
Trim cols of images to match smallest
*/
void Region::trim_cols(std::vector<cv::Mat> &regions) 
{
	int min = INT_MAX;
	for (const cv::Mat &region: regions) {
		if (region.cols < min) {
			min = region.cols;
		}
	}

	for (cv::Mat &region : regions) {
		if (region.cols != min) {
			region = region.colRange(0, min);
		}
	}
}