#pragma once

#include "pch.h"
#include <iostream>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


#define USE_PARALLEL 1
#define MAX_IMG_DIFF 150

enum ROW_COL_T {
	ROW,
	COL,
};


class Region {
private:
	static std::vector<int> filter_true_seams(const std::vector<int> &seams);
	static std::vector<int> get_seams(const cv::Mat &img, ROW_COL_T row_col, int threshold);

public:
	static bool resize_to_fit(const cv::Mat &img, cv::Mat &cal);
	static cv::Mat fix_shading(const cv::Mat &img, cv::Mat cal, const cv::Mat &mean);
	static std::vector<std::vector<cv::Mat>> find_regions(const cv::Mat &img);

	static void trim_cols(std::vector<cv::Mat> &regions);
};