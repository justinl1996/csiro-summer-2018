// shading.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <thread>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "region.h"
#include "Timer.h"

int main(int argc, char **argv)
{
	if (argc != 4) {
		fprintf(stderr, "shading cal_img1 cal_img2 cal_correct tofix");
		return 1;
	}
	Timer timer1;

	cv::Mat img = cv::imread("C:\\Users\\luo029\\Pictures\\shading\\coals.tif", 
		cv::IMREAD_COLOR);
	std::vector<std::vector<cv::Mat>> regions = Region::find_regions(img);
	//std::cout << "Regions: " << timer3.elapsed() << std::endl;

	Timer timer3;
	cv::Mat correct = cv::imread(argv[3]);
	cv::Mat mean;
	cv::Mat std;

	//calculate average intensity of background
	cv::meanStdDev(correct, mean, std);

	//flat field image for calibration - top & bottom row
	cv::Mat cal1 = cv::imread(argv[1], cv::IMREAD_COLOR);
	cv::Mat cal2 = cv::imread(argv[2], cv::IMREAD_COLOR);
	//cv::Mat cal3 = cv::imread(argv[3], cv::IMREAD_COLOR);
	std::vector<cv::Mat> cal = { cal1, cal2 };

	//image to fix
	//cv::Mat actual = cv::imread(argv[3], cv::IMREAD_COLOR);

	std::vector<cv::Mat> row_join;

	Timer timer2;
	std::vector<std::thread> workers;
	std::cout << "Cal: " << timer3.elapsed() << std::endl;

	timer3.reset();
	for (int i = 0; i < regions.size(); i++) {
#if USE_PARALLEL
		for (int j = 0; j < regions[i].size(); j++) {
			std::thread th([&](int x, int y) {
				regions[x][y] = Region::fix_shading(regions[x][y], cal[x], mean);
			}, i, j);
			workers.push_back(std::move(th));
		}
		for (std::thread &th : workers) {
			if (th.joinable()) {
				th.join();
			}
		}
#else
		for (int j = 0; j < regions[i].size(); j++) {
			regions[i][j] = Region::fix_shading(regions[i][j], cal[i], mean);
	    }
#endif
		//join the images horizontally 
		cv::Mat row_mat;
		cv::hconcat(regions[i], row_mat);
		row_join.push_back(row_mat);
	}

	//join vertically with entire image
	cv::Mat finish;
	Region::trim_cols(row_join);
	cv::vconcat(row_join, finish);

	std::cout << "Shading: " << timer3.elapsed() << std::endl;

	//std::cout << "Shading: " <<timer2.elapsed() << std::endl;
	std::cout << "Total Time: " << timer1.elapsed() << std::endl;

	/*cv::imshow("image", finish);
	cv::waitKey(0);*/

	imwrite("finish2.tif", finish);

	return 0;
}
